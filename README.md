This is a source controlled version of http://www.7hillsleague.org/.

# How to contribute
* Sign up for an account with Github
* Request a developer rule for this project
* Clone the project locally
* Make the changes
* Commit changes
* Push changes