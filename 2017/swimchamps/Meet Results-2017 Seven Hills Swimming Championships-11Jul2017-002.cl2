A01V3      02Meet Results                  Hy-Tek, Ltd         WMM 6.0Ea Hy-Tek, Ltd         866-456-511107132017                                    MM40    N42
B11        2017 Seven Hills Swimming Cham                                            Cincinnati          OH45140     USA 0711201707122017   0        S 000   N73
C11OH      OHFH  FH Sharks                     FH                                                                                          USA               N82
D01OH      Blocksidge, Abbey                       AUSA0425200215FF  501 19 151807112017   40.08S   38.66S                   1 3 0 0 21             01      NN89
D3                                                                                                                                                           N67
D01OH      Blocksidge, Abbey                       AUSA0425200215FF 1001 49 151807122017 1:31.28S                   1:26.89S 0 0 1 3  0 13       3  01      NN50
G01OH          Blocksidge, Abbey                       1 2  50C   41.32 1:26.89                                                                F             N65
D01OH      Blocksidge, Abbey                       AUSA0425200215FF  503 59 151807122017                              57.89S 0 0 1 4  0 17       2  03      NN39
D01OH      Blocksidge, Avery                       AUSA0212200611FF  501 45 111207112017   44.26S   43.70S                   3 3 0 0 33             01      NN99
D3                                                                                                                                                           N67
D01OH      Blocksidge, Avery                       AUSA0212200611FF  503 55 111207122017 1:03.52S                   1:01.46S 0 0 2 2  0 27       3  03      NN50
D01OH      Blocksidge, Avery                       AUSA0212200611FF 1005 65 111207122017 2:03.46S                   1:57.69S 0 0 1 4  0 15       3  05      NN70
G01OH          Blocksidge, Avery                       1 2  50C   56.19 1:57.69                                                                F             N85
D01OH      Bridges, Will                           AUSA1021199917MM  501 20 151807112017   29.81S   29.68S                   3 1 0 0 15             01      NN68
D3                                                                                                                                                           N67
D01OH      Bridges, Will                           AUSA1021199917MM 1001 50 151807122017 1:08.82S                   1:05.69S 0 0 2 1  0 11  2.   5  0100    NN69
G01OH          Bridges, Will                           1 2  50C   31.48 1:05.69                                                                F             N24
D01OH      Bridges, Will                           AUSA1021199917MM  503 60 151807122017   41.76S                     40.07S 0 0 2 3  0  8  5.   2  0300    NN09
D01OH      Bruenderman, Logan                      AUSA0123200710MM  254 24 091007122017   22.45S                     22.27S 0 0 3 1  0 11  2.   5  0400    NN90
D3                                                                                                                                                           N67
D01OH      Bruenderman, Logan                      AUSA0123200710MM  252 34 091007122017   25.57S                     26.46S 0 0 5 6  0 15       5  02      NN60
D01OH      Bruenderman, Logan                      AUSA0123200710MM  501 44 091007122017   41.27S                     42.23S 0 0 5 6  0  8  5.   5  0100    NN80
D01OH      Bruenderman, Riley                      AUSA03152009 8FF  251 11 UN0807122017   22.15S                     22.03S 0 011 2  0  4 11.   3  0100    NN21
D3                                                                                                                                                           N67
D01OH      Bruenderman, Riley                      AUSA03152009 8FF  254 21 UN0807122017   33.35S                     31.08S 0 0 2 2  0  8  5.   1  0400    NN11
D01OH      Bruenderman, Riley                      AUSA03152009 8FF  252 31 UN0807122017   43.80S                     33.24S 0 0 2 4  0 17       1  02      NN90
G01OH          Bruenderman, Riley                      1 3  25C                   33.24                                                        F             N55
D01OH      Burke, Nolan                            AUSA08222008 8MM  251 12 UN0807122017   18.97S                     19.66S 0 0 9 3  0  2 13.   2  0100    NN09
D3                                                                                                                                                           N67
D01OH      Burke, Nolan                            AUSA08222008 8MM  254 22 UN0807122017   28.57S                     26.01S 0 0 3 2  0  2 13.   2  0400    NN98
D01OH      Burke, Nolan                            AUSA08222008 8MM  501 42 UN0807122017   45.86S                     45.78S 0 0 6 5  0  6  9.   5  0100    NN09
D01OH      Burke, Sophie                           AUSA0904199917FF  501 19 151807112017   35.20S   35.79S                   4 6 0 0 17             01      NN78
D3                                                                                                                                                           N67
D01OH      Burke, Sophie                           AUSA0904199917FF  502 39 151807112017   43.14S   44.19S                   4 6 0 0 17             02      NN78
D01OH      Burke, Sophie                           AUSA0904199917FF  503 59 151807122017   48.72S                        NSS 0 0 3 6  0  0       0  03      NN88
D01OH      Butkovich, Maggie                       AUSA1211200313FF  501 17 131407112017   32.32S   32.16S            31.89S 3 4 2 1  5  5 10.   5  0100    NN21
D3                                                                                                                                                           N67
D01OH      Butkovich, Maggie                       AUSA1211200313FF  502 37 131407112017   40.84S   41.59S            40.49S 2 4 1 4  8  8  5.   2  0200    NN21
D01OH      Butkovich, Maggie                       AUSA1211200313FF 1005 67 131407122017                            1:24.16S 0 0 1 6  0  5 10.   1  0500    NN00
G01OH          Butkovich, Maggie                       1 2  50C   39.45 1:24.16                                                                F             N75
D01OH      Butkovich, Molly                        AUSA0715200412FF  251 15 111207112017   14.31S   14.23S            14.19S 7 4 2 5  4  4 11.   4  0100    NN01
D3                                                                                                                                                           N67
D01OH      Butkovich, Molly                        AUSA0715200412FF  504 25 111207112017   32.56S   33.12S            31.70S 4 3 2 3  1  2 13.   2  0400    NN01
D01OH      Butkovich, Molly                        AUSA0715200412FF 1005 65 111207122017 1:22.38S                   1:18.75S 0 0 3 5  0  3 12.   3  0500    NN90
G01OH          Butkovich, Molly                        1 2  50C   34.89 1:18.75                                                                F             N65
D01OH      Butkovich, Patrick                      AUSA0108199918MM 1001 50 151807122017   56.25S                     54.67S 0 0 3 3  0  1 16.   1  0100    NN31
D3                                                                                                                                                           N67
G01OH          Butkovich, Patrick                      1 2  50C   26.48   54.67                                                                F             N16
D01OH      Butkovich, Patrick                      AUSA0108199918MM  503 60 151807122017   33.16S                     33.54S 0 0 3 4  0  2 13.   2  0300    NN21
D01OH      Butkovich, Patrick                      AUSA0108199918MM 1005 70 151807122017 1:05.08S                   1:01.15S 0 0 4 3  0  1 16.   1  0500    NN71
G01OH          Butkovich, Patrick                      1 2  50C   28.79 1:01.15                                                                F             N36
D01OH      Butkovich, Sean                         AUSA0916200115MM  504 30 151807112017            31.33S            30.98S 3 1 2 6  6  6  9.   6  0400    NN99
D3                                                                                                                                                           N67
D01OH      Butkovich, Sean                         AUSA0916200115MM 1001 50 151807122017 1:04.73S                   1:02.90S 0 0 2 3  0  7  7.   2  0100    NN30
G01OH          Butkovich, Sean                         1 2  50C   30.65 1:02.90                                                                F             N05
D01OH      Butkovich, Sean                         AUSA0916200115MM  503 60 151807122017   37.84S                     36.35S 0 0 3 5  0  3 12.   3  0300    NN99
D01OH      Carroll, Alison                         AUSA0126200017FF  502 39 151807112017   41.57S   40.81S            41.10S 2 2 1 6 12 12  1.   6  0200    NN60
D3                                                                                                                                                           N67
D01OH      Carroll, Alison                         AUSA0126200017FF 1001 49 151807122017 1:16.86S                   1:14.11S 0 0 2 4  0 10  3.   2  0100    NN20
G01OH          Carroll, Alison                         1 2  50C   35.33 1:14.11                                                                F             N05
D01OH      Carroll, Alison                         AUSA0126200017FF 1005 69 151807122017 1:32.43S                   1:30.43S 0 0 2 1  0 13       5  05      NN99
G01OH          Carroll, Alison                         1 2  50C   41.44 1:30.43                                                                F             N05
D01OH      Carroll, MAC                            AUSA0305200215MM  502 40 151807112017   33.32S   33.93S            33.56S 2 4 1 3  7  7  7.   1  0200    NN98
D3                                                                                                                                                           N67
D01OH      Carroll, MAC                            AUSA0305200215MM 1001 50 151807122017 1:06.98S                   1:04.26S 0 0 2 5  0  9  4.   3  0100    NN78
G01OH          Carroll, MAC                            1 2  50C   31.34 1:04.26                                                                F             N43
D01OH      Carroll, MAC                            AUSA0305200215MM 1005 70 151807122017 1:16.45S                   1:14.83S 0 0 3 1  0  9  4.   4  0500    NN78
G01OH          Carroll, MAC                            1 2  50C   34.75 1:14.83                                                                F             N43
D01OH      Colglazier, Charlie                     AUSA05152012 5FF  251 11 UN0807122017 1:02.18S                        NSS 0 0 2 5  0  0       0  01      NN21
D3                                                                                                                                                           N67
D01OH      Colglazier, Charlie                     AUSA05152012 5FF  252 31 UN0807122017 1:08.58S                        NSS 0 0 2 6  0  0       0  02      NN21
G01OH          Colglazier, Charlie                     1 3  25C                                                                                F             N35
D01OH      Colglazier, Charlie                     AUSA05152012 5FF  253 51 UN0807122017                                 NSS 0 0 1 2  0  0       0  03      NN20
D01OH      Colglazier, Kelsey                      AUSA0512200611FF  251 15 111207112017   19.42S   19.53S                   2 2 0 0 34             01      NN40
D3                                                                                                                                                           N67
D01OH      Colglazier, Kelsey                      AUSA0512200611FF  504 25 111207112017            57.75S                   1 4 0 0 20             04      NN69
D01OH      Colglazier, Kelsey                      AUSA0512200611FF  501 45 111207112017   46.67S   50.64S                   3 1 0 0 40             01      NN40
D01OH      Colglazier, Sammy                       AUSA0112200413FF  501 17 131407112017   35.69S   35.19S            34.96S 2 2 1 5 10  9  4.   3  0100    NN41
D3                                                                                                                                                           N67
D01OH      Colglazier, Sammy                       AUSA0112200413FF 1001 47 131407112017 1:26.32S 1:27.26S          1:23.75S 3 5 1 2  9 10  3.   4  0100    NN12
G01OH          Colglazier, Sammy                       1 2  50C   41.33 1:27.26                                                                P             N95
G01OH          Colglazier, Sammy                       1 2  50C   39.70 1:23.75                                                                F             N95
D01OH      Colglazier, Sammy                       AUSA0112200413FF 1005 67 131407122017 1:40.22S                        DQS 0 0 1 2  0  0       0  05      NN30
G01OH          Colglazier, Sammy                       1 2  50C   45.43 1:35.96                                                                F             N95
D01OH      Courtney, Palmer                        AUSA0308200215MM  501 20 151807112017   34.90S      NSS                   2 5 0 0  0             01      NN79
D3                                                                                                                                                           N67
D01OH      Courtney, Palmer                        AUSA0308200215MM  503 60 151807122017   46.28S                        NSS 0 0 1 2  0  0       0  03      NN99
D01OH      Courtney, Palmer                        AUSA0308200215MM 1005 70 151807122017 1:33.43S                        NSS 0 0 1 4  0  0       0  05      NN20
G01OH          Courtney, Palmer                        1 2  50C                                                                                F             N34
D01OH      Crowley, Maggie                         AUSA0510200413FF  504 27 131407112017   38.15S   36.35S            37.46S 1 4 2 5  4  6  9.   6  0400    NN50
D3                                                                                                                                                           N67
D01OH      Crowley, Maggie                         AUSA0510200413FF 1001 47 131407112017 1:11.59S 1:11.37S          1:11.70S 3 4 2 5  4  4 11.   4  0100    NN21
G01OH          Crowley, Maggie                         1 2  50C   34.55 1:11.37                                                                P             N05
G01OH          Crowley, Maggie                         1 2  50C   34.61 1:11.70                                                                F             N05
D01OH      Crowley, Maggie                         AUSA0510200413FF  503 57 131407122017   41.87S                     42.12S 0 0 3 1  0  5 10.   5  0300    NN79
D01OH      Crowley, Mike                           AUSA0618200610MM  252 34 091007122017   20.17S                     20.95S 0 0 6 4  0  2 13.   2  0200    NN19
D3                                                                                                                                                           N67
D01OH      Crowley, Mike                           AUSA0618200610MM  501 44 091007122017   38.60S                        DQS 0 0 5 4  0  0       0  01      NN68
D01OH      Crowley, Mike                           AUSA0618200610MM 1005 64 091007122017 1:32.94S                   1:37.71S 0 0 3 4  0  4 11.   4  0500    NN79
G01OH          Crowley, Mike                           1 2  50C   47.75 1:37.71                                                                F             N44
D01OH      Delahanty, Michael                      AUSA0712200016MM  502 40 151807112017   38.09S   37.50S            38.31S 2 5 1 1 11 11  2.   5  0200    NN71
D3                                                                                                                                                           N67
D01OH      Delahanty, Michael                      AUSA0712200016MM 1001 50 151807122017 1:08.93S                   1:07.22S 0 0 2 6  0 13       6  01      NN01
G01OH          Delahanty, Michael                      1 2  50C   32.62 1:07.22                                                                F             N06
D01OH      Delahanty, Michael                      AUSA0712200016MM 1005 70 151807122017 1:19.92S                   1:18.09S 0 0 3 6  0 11  2.   6  0500    NN41
G01OH          Delahanty, Michael                      1 2  50C   36.28 1:18.09                                                                F             N06
D01OH      Dellecave, Gideon                       AUSA02212010 7MM  251 12 UN0807122017   32.03S                     47.08S 0 0 5 1  0 48       6  01      NN20
D3                                                                                                                                                           N67
D01OH      Dellecave, Gideon                       AUSA02212010 7MM  252 32 UN0807122017   39.98S                     52.16S 0 0 5 6  0 38       6  02      NN40
G01OH          Dellecave, Gideon                       1 3  25C   30.99   41.22   52.16                                                        F             N85
D01OH      Dellecave, Gideon                       AUSA02212010 7MM  253 52 UN0807122017   35.22S                        DQS 0 0 3 2  0  0       0  03      NN10
D01OH      Desjardins, Jeremiah                    AUSA09282008 8MM  251 12 UN0807122017   32.29S                     26.82S 0 0 5 6  0 19       1  01      NN71
D3                                                                                                                                                           N67
D01OH      Desjardins, Jeremiah                    AUSA09282008 8MM  252 32 UN0807122017   32.92S                     37.13S 0 0 6 4  0 18       5  02      NN71
D01OH      Desjardins, Jeremiah                    AUSA09282008 8MM  253 52 UN0807122017                                 DQS 0 0 2 6  0  0       0  03      NN70
D01OH      Disylvestro, Anna                       AUSA03192009 8FF  251 11 UN0807122017   37.25S                     39.77S 0 0 3 4  0 52       3  01      NN70
D3                                                                                                                                                           N67
D01OH      Disylvestro, Anna                       AUSA03192009 8FF  501 41 UN0807122017                                 NSS 0 0 3 6  0  0       0  01      NN89
D01OH      Disylvestro, Anna                       AUSA03192009 8FF  253 51 UN0807122017   39.81S                        NSS 0 0 3 2  0  0       0  03      NN60
D01OH      Donnels, Carter                         AUSA0309200017MM  501 20 151807112017   27.02S   26.84S            25.73S 3 3 2 2  3  2 13.   2  0100    NN60
D3                                                                                                                                                           N67
D01OH      Donnels, Carter                         AUSA0309200017MM  504 30 151807112017   27.64S   28.17S            26.78S 3 3 2 3  1  1 16.   1  0400    NN70
D01OH      Donnels, Carter                         AUSA0309200017MM  502 40 151807112017   31.92S   32.30S            29.86S 1 3 2 2  3  2 13.   2  0200    NN60
D01OH      Dostal, Brayden                         AUSA0822200313MM  502 38 131407112017   49.45S   46.34S                   2 1 0 0 13             02      NN39
D3                                                                                                                                                           N67
D01OH      Dostal, Brayden                         AUSA0822200313MM  503 58 131407122017   50.76S                     50.50S 0 0 2 1  0 12  1.   6  0300    NN89
D01OH      Dostal, Brayden                         AUSA0822200313MM 1005 68 131407122017 1:42.16S                   1:38.78S 0 0 1 2  0  8  5.   2  0500    NN30
G01OH          Dostal, Brayden                         1 2  50C   44.41 1:38.78                                                                F             N05
D01OH      Dostal, Justin                          AUSA0220200512MM  504 26 111207112017   29.98S   29.87S            29.27S 2 3 2 3  1  1 16.   1  0400    NN40
D3                                                                                                                                                           N67
D01OH      Dostal, Justin                          AUSA0220200512MM  501 46 111207112017   29.66S   29.70S            28.72S 4 3 2 3  1  1 16.   1  0100    NN30
D01OH      Dostal, Justin                          AUSA0220200512MM 1005 66 111207122017 1:14.31S                   1:10.37S 0 0 3 3  0  1 16.   1  0500    NN00
G01OH          Dostal, Justin                          1 2  50C   31.84 1:10.37                                                                F             N74
D01OH      Dunn, Jennifer                          AUSA1001199818FF  501 19 151807112017   35.83S   36.15S                   2 3 0 0 18             01      NN98
D3                                                                                                                                                           N67
D01OH      Dunn, Jennifer                          AUSA1001199818FF  502 39 151807112017   41.70S   41.65S                   3 5 0 0 14             02      NN98
D01OH      Dunn, Jennifer                          AUSA1001199818FF 1005 69 151807122017 1:27.99S                   1:26.99S 0 0 2 2  0 11  2.   4  0500    NN10
G01OH          Dunn, Jennifer                          1 2  50C   39.25 1:26.99                                                                F             N74
D01OH      Fiktus, Leah                            AUSA11082007 9FF  251 13 091007122017   23.08S                     22.54S 0 0 3 1  0 23       5  01      NN18
D3                                                                                                                                                           N67
D01OH      Fiktus, Leah                            AUSA11082007 9FF  254 23 091007122017   25.73S                     25.34S 0 0 2 2  0 18       4  04      NN28
D01OH      Fiktus, Leah                            AUSA11082007 9FF  253 53 091007122017   30.50S                     31.69S 0 0 2 5  0 17       4  03      NN28
D01OH      Fitkus, Hailey                          AUSA0621200610FF  251 13 091007122017   36.60S                     30.13S 0 0 1 1  0 34       4  01      NN09
D3                                                                                                                                                           N67
D01OH      Fitkus, Hailey                          AUSA0621200610FF  252 33 091007122017   53.81S                   1:01.75S 0 0 1 4  0 28       4  02      NN39
G01OH          Fitkus, Hailey                          1 4  25C                         1:01.75                                                F             N24
D01OH      Fitkus, Hailey                          AUSA0621200610FF  501 43 091007122017 1:22.92S                   1:26.49S 0 0 1 4  0 31       3  01      NN59
D01OH      Fitkus, Melissa                         AUSA0820200313FF  501 17 131407112017   43.96S   45.60S                   1 2 0 0 21             01      NN39
D3                                                                                                                                                           N67
D01OH      Fitkus, Melissa                         AUSA0820200313FF  502 37 131407112017   59.78S   55.46S                   1 2 0 0 20             02      NN39
D01OH      Fitkus, Melissa                         AUSA0820200313FF  503 57 131407122017 1:06.18S                   1:01.41S 0 0 1 6  0 15       5  03      NN99
D01OH      Gawronski, James                        AUSA0718200511MM  251 16 111207112017   21.42S   21.16S                   1 2 0 0 20             01      NN69
D3                                                                                                                                                           N67
D01OH      Gawronski, James                        AUSA0718200511MM  501 46 111207112017   48.98S   51.24S                   1 5 0 0 21             01      NN79
D01OH      Gawronski, James                        AUSA0718200511MM  503 56 111207122017   59.13S                        DQS 0 0 2 4  0  0       0  03      NN89
D01OH      Gawronski, Sarah                        AUSA01242010 7FF  251 11 UN0807122017   31.72S                     32.63S 0 0 6 5  0 37       6  01      NN10
D3                                                                                                                                                           N67
D01OH      Gawronski, Sarah                        AUSA01242010 7FF  252 31 UN0807122017   35.91S                     35.64S 0 0 4 2  0 25       2  02      NN10
D01OH      Gawronski, Sarah                        AUSA01242010 7FF  253 51 UN0807122017                                 DQS 0 0 1 3  0  0       0  03      NN19
D01OH      Gawronski, Sean                         AUSA07052007 9MM  251 14 091007122017   24.36S                     22.27S 0 0 2 4  0 24       1  01      NN49
D3                                                                                                                                                           N67
D01OH      Gawronski, Sean                         AUSA07052007 9MM  252 34 091007122017   28.90S                     32.43S 0 0 3 2  0 27       5  02      NN59
D01OH      Gawronski, Sean                         AUSA07052007 9MM  253 54 091007122017                                 DQS 0 0 1 2  0  0       0  03      NN68
D01OH      Geiger, Olivia                          AUSA1003200313FF  504 27 131407112017   38.98S   38.09S            36.23S 2 2 1 3  7  7  7.   1  0400    NN00
D3                                                                                                                                                           N67
D01OH      Geiger, Olivia                          AUSA1003200313FF  503 57 131407122017   41.59S                     41.88S 0 0 3 5  0  4 11.   4  0300    NN39
D01OH      Geiger, Olivia                          AUSA1003200313FF 1005 67 131407122017 1:29.37S                   1:25.76S 0 0 2 6  0  6  9.   5  0500    NN89
G01OH          Geiger, Olivia                          1 2  50C         1:25.76                                                                F             N14
D01OH      Geis, Erin                              AUSA0219200215FF  501 19 151807112017   33.18S   32.56S            32.48S 3 5 1 6 12 12  1.   6  0100    NN78
D3                                                                                                                                                           N67
D01OH      Geis, Erin                              AUSA0219200215FF  503 59 151807122017   49.41S                     43.94S 0 0 2 4  0  8  5.   1  0300    NN87
D01OH      Geis, Erin                              AUSA0219200215FF 1005 69 151807122017 1:29.55S                   1:25.27S 0 0 2 5  0 10  3.   3  0500    NN48
G01OH          Geis, Erin                              1 2  50C   40.12 1:25.27                                                                F             N03
D01OH      Geis, Jordan                            AUSA0424200314FF  501 17 131407112017   37.11S   34.61S            34.54S 2 5 1 4  8  8  5.   2  0100    NN29
D3                                                                                                                                                           N67
D01OH      Geis, Jordan                            AUSA0424200314FF  502 37 131407112017   45.47S   45.64S            46.94S 3 5 1 6 12 12  1.   6  0200    NN59
D01OH      Geis, Jordan                            AUSA0424200314FF  503 57 131407122017   45.91S                     46.30S 0 0 2 5  0  9  4.   4  0300    NN58
D01OH      Glover, Sam                             AUSA06092008 8FF  251 11 UN0807122017   31.70S                     28.90S 0 0 6 2  0 26       3  01      NN28
D3                                                                                                                                                           N67
D01OH      Glover, Sam                             AUSA06092008 8FF  252 31 UN0807122017                              38.79S 0 0 1 2  0 30       4  02      NN47
D01OH      Glover, Sam                             AUSA06092008 8FF  253 51 UN0807122017                                 DQS 0 0 2 2  0  0       0  03      NN27
D01OH      Glynn, Emma                             AUSA1012200610FF  254 23 091007122017   21.86S                     23.01S 0 0 3 2  0 13       4  04      NN77
D3                                                                                                                                                           N67
D01OH      Glynn, Emma                             AUSA1012200610FF  501 43 091007122017   43.54S                     42.98S 0 0 4 4  0 15       5  01      NN87
D01OH      Glynn, Emma                             AUSA1012200610FF 1005 63 091007122017                            2:00.80S 0 0 1 2  0 11  2.   2  0500    NN67
G01OH          Glynn, Emma                             1 2  50C   53.44 2:00.80                                                                F             N43
D01OH      Goret, Ellery                           AUSA0523200611FF  251 15 111207112017   19.30S   20.63S                   2 4 0 0 37             01      NN48
D3                                                                                                                                                           N67
D01OH      Goret, Ellery                           AUSA0523200611FF  502 35 111207112017 1:01.85S   59.66S                   1 4 0 0 23             02      NN78
D01OH      Goret, Ellery                           AUSA0523200611FF  501 45 111207112017   45.56S   47.47S                   3 2 0 0 38             01      NN58
D01OH      Grace, Joey                             AUSA0630199917MM  504 30 151807112017               DQS                   1 1 0 0  0             04      NN86
D3                                                                                                                                                           N67
D01OH      Grace, Joey                             AUSA0630199917MM 1001 50 151807122017 1:06.50S                   1:05.02S 0 0 2 2  0 10  3.   4  0100    NN78
G01OH          Grace, Joey                             1 2  50C         1:05.02                                                                F             N82
D01OH      Grace, Joey                             AUSA0630199917MM  501 20 151807112017   27.87S   27.92S            27.22S 5 2 2 6  6  5 10.   5  0100    NN29
D01OH      Grace, Megan                            AUSA1126200016FF 1001 49 151807122017 1:19.41S                   1:18.05S 0 0 2 2  0 11  2.   3  0100    NN88
D3                                                                                                                                                           N67
G01OH          Grace, Megan                            1 2  50C   37.50 1:18.05                                                                F             N63
D01OH      Grace, Megan                            AUSA1126200016FF  503 59 151807122017   46.12S                     44.01S 0 0 3 5  0  9  4.   2  0300    NN38
D01OH      Grace, Megan                            AUSA1126200016FF 1005 69 151807122017                            1:31.33S 0 0 1 2  0 14       3  05      NN67
G01OH          Grace, Megan                            1 2  50C   43.54 1:31.33                                                                F             N63
D01OH      Grandy, Jack                            AUSA0730200313MM  501 18 131407112017   29.84S   29.42S            29.20S 2 3 2 2  3  4 11.   4  0100    NN39
D3                                                                                                                                                           N67
D01OH      Grandy, Jack                            AUSA0730200313MM  503 58 131407122017   36.97S                     36.48S 0 0 3 3  0  2 13.   2  0300    NN68
D01OH      Grandy, Jack                            AUSA0730200313MM 1005 68 131407122017 1:12.92S                   1:12.58S 0 0 2 4  0  2 13.   2  0500    NN19
G01OH          Grandy, Jack                            1 2  50C   34.42 1:12.58                                                                F             N73
D01OH      Grimes, Kelly                           AUSA0130199918FF  501 19 151807112017   30.94S      DQS                   4 3 0 0  0             01      NN58
D3                                                                                                                                                           N67
D01OH      Grimes, Kelly                           AUSA0130199918FF  504 29 151807112017   34.34S   34.78S            34.37S 2 4 1 3  7  7  7.   1  0400    NN00
D01OH      Grimes, Kelly                           AUSA0130199918FF 1005 69 151807122017 1:18.14S                   1:18.10S 0 0 3 5  0  5 10.   4  0500    NN79
G01OH          Grimes, Kelly                           1 2  50C   36.00 1:18.10                                                                F             N24
D01OH      Grimes, Kyle                            AUSA1102200313MM 1001 48 131407112017 1:11.12S 1:13.27S          1:10.43S 1 4 1 3  7  7  7.   1  0100    NN00
D3                                                                                                                                                           N67
G01OH          Grimes, Kyle                            1 2  50C         1:13.27                                                                P             N43
G01OH          Grimes, Kyle                            1 2  50C   33.00 1:10.43                                                                F             N83
D01OH      Grimes, Kyle                            AUSA1102200313MM  503 58 131407122017   44.26S                     43.55S 0 0 2 3  0  7  7.   2  0300    NN68
D01OH      Grimes, Kyle                            AUSA1102200313MM 1005 68 131407122017 1:23.28S                   1:25.95S 0 0 2 6  0  6  9.   6  0500    NN29
G01OH          Grimes, Kyle                            1 2  50C   40.92 1:25.95                                                                F             N93
D01OH      Gutbier, Madeline                       AUSA07242007 9FF  251 13 091007122017   24.13S                     23.52S 0 0 2 3  0 25       1  01      NN99
D3                                                                                                                                                           N67
D01OH      Gutbier, Madeline                       AUSA07242007 9FF  252 33 091007122017                              29.08S 0 0 1 5  0 20       2  02      NN39
G01OH          Gutbier, Madeline                       1 4  25C                           29.08                                                F             N05
D01OH      Gutbier, Madeline                       AUSA07242007 9FF  253 53 091007122017   36.48S                     37.29S 0 0 1 4  0 20       3  03      NN10
D01OH      Heis, Brooke                            AUSA1010200313FF  501 17 131407112017   33.74S   32.68S            33.64S 4 2 2 6  6  6  9.   6  0100    NN39
D3                                                                                                                                                           N67
D01OH      Heis, Brooke                            AUSA1010200313FF  502 37 131407112017   44.55S   39.38S            38.56S 4 5 2 1  5  4 11.   4  0200    NN49
D01OH      Heis, Brooke                            AUSA1010200313FF  503 57 131407122017   41.04S                     41.84S 0 0 3 2  0  3 12.   3  0300    NN48
D01OH      Heis, Forest                            AUSA1212200412MM  502 36 111207112017            46.80S            46.17S 1 5 2 2  3  3 12.   3  0200    NN78
D3                                                                                                                                                           N67
D01OH      Heis, Forest                            AUSA1212200412MM  503 56 111207122017   56.25S                     55.53S 0 0 3 1  0 12  1.   5  0300    NN68
D01OH      Heis, Forest                            AUSA1212200412MM 1005 66 111207122017 1:54.84S                   1:54.12S 0 0 2 5  0 10  3.   3  0500    NN29
G01OH          Heis, Forest                            1 2  50C         1:54.12                                                                F             N43
D01OH      Heis, Max                               AUSA12082007 9MM  251 14 091007122017   18.20S                     17.47S 0 0 5 3  0  5 10.   1  0100    NN47
D3                                                                                                                                                           N67
D01OH      Heis, Max                               AUSA12082007 9MM  253 54 091007122017   23.07S                     23.22S 0 0 5 2  0  3 12.   3  0300    NN47
D01OH      Heis, Max                               AUSA12082007 9MM 1005 64 091007122017 1:58.54S                   1:50.74S 0 0 2 4  0  6  9.   1  0500    NN97
G01OH          Heis, Max                               1 2  50C   51.52 1:50.74                                                                F             N62
D01OH      Heis, Rachel                            AUSA1220200016FF  501 19 151807112017   31.97S   30.89S            31.64S 3 2 2 5  4  5 10.   5  0100    NN29
D3                                                                                                                                                           N67
D01OH      Heis, Rachel                            AUSA1220200016FF 1001 49 151807122017 1:11.14S                   1:09.36S 0 0 3 5  0  3 12.   3  0100    NN98
G01OH          Heis, Rachel                            1 2  50C   32.62 1:09.36                                                                F             N73
D01OH      Heis, Rachel                            AUSA1220200016FF  503 59 151807122017   40.08S                     39.45S 0 0 4 2  0  4 11.   4  0300    NN48
D01OH      Hellmann, Nicholas                      AUSA0316200215MM  501 20 151807112017   28.06S   28.72S                   4 2 0 0 13             01      NN30
D3                                                                                                                                                           N67
D01OH      Hellmann, Nicholas                      AUSA0316200215MM  502 40 151807112017   33.87S   33.38S            33.25S 1 4 2 1  5  5 10.   5  0200    NN71
D01OH      Hellmann, Nicholas                      AUSA0316200215MM 1005 70 151807122017 1:14.33S                   1:10.28S 0 0 3 4  0  6  9.   1  0500    NN31
G01OH          Hellmann, Nicholas                      1 2  50C   32.11 1:10.28                                                                F             N06
D01OH      Isch, Sierra                            AUSA0727200115FF  504 29 151807112017   37.10S      NSS                   2 2 0 0  0             04      NN97
D3                                                                                                                                                           N67
D01OH      Isch, Sierra                            AUSA0727200115FF  502 39 151807112017   41.69S      NSS                   4 5 0 0  0             02      NN08
D01OH      Isch, Sierra                            AUSA0727200115FF  503 59 151807122017   49.90S                        NSS 0 0 2 2  0  0       0  03      NN28
D01OH      Johnson, Alexandra                      AUSA0412200512FF  251 15 111207112017   13.63S   13.83S            13.59S 6 3 2 4  2  2 13.   2  0100    NN61
D3                                                                                                                                                           N67
D01OH      Johnson, Alexandra                      AUSA0412200512FF  502 35 111207112017   35.32S   34.49S            33.05S 4 3 2 3  1  1 16.   1  0200    NN61
D01OH      Johnson, Alexandra                      AUSA0412200512FF 1005 65 111207122017 1:16.76S                   1:15.54S 0 0 3 3  0  1 16.   1  0500    NN41
G01OH          Johnson, Alexandra                      1 2  50C   33.15 1:15.54                                                                F             N26
D01OH      Johnson, Lucia                          AUSA0412200512FF  504 25 111207112017            33.55S            31.44S 2 6 2 4  2  1 16.   1  0400    NN49
D3                                                                                                                                                           N67
D01OH      Johnson, Lucia                          AUSA0412200512FF  501 45 111207112017   33.22S   30.93S            31.12S 8 2 2 5  4  4 11.   4  0100    NN10
D01OH      Johnson, Lucia                          AUSA0412200512FF  503 55 111207122017   45.88S                     42.54S 0 0 6 1  0  2 13.   2  0300    NN49
D01OH      Johnson, McKENNA                        AUSA05282008 9FF  254 23 091007122017   18.72S                     18.74S 0 0 4 2  0  5 10.   4  0400    NN39
D3                                                                                                                                                           N67
D01OH      Johnson, McKENNA                        AUSA05282008 9FF  501 43 091007122017   39.73S                     38.50S 0 0 5 3  0  6  9.   2  0100    NN29
D01OH      Johnson, McKENNA                        AUSA05282008 9FF 1005 63 091007122017 1:37.69S                   1:39.90S 0 0 3 2  0  4 11.   4  0500    NN99
G01OH          Johnson, McKENNA                        1 2  50C   45.28 1:39.90                                                                F             N64
D01OH      Johnson, Owen                           AUSA07072011 5MM  251 12 UN0807122017   34.97S                     36.90S 0 0 4 1  0 44       5  01      NN19
D3                                                                                                                                                           N67
D01OH      Johnson, Owen                           AUSA07072011 5MM  254 22 UN0807122017                                 DQS 0 0 1 1  0  0       0  04      NN18
D01OH      Johnson, Owen                           AUSA07072011 5MM  252 32 UN0807122017   47.31S                     45.61S 0 0 4 5  0 34       5  02      NN19
D01OH      Kenney, Aaron                           AUSA03282010 7MM  251 12 UN0807122017   43.73S                     50.11S 0 0 1 3  0 50       3  01      NN98
D3                                                                                                                                                           N67
D01OH      Kenney, Aaron                           AUSA03282010 7MM  252 32 UN0807122017   39.38S                     44.78S 0 0 5 2  0 33       5  02      NN09
G01OH          Kenney, Aaron                           1 3  25C                   44.78                                                        F             N63
D01OH      Kenney, Aaron                           AUSA03282010 7MM  253 52 UN0807122017   43.74S                        DQS 0 0 2 2  0  0       0  03      NN88
D01OH      Kenney, William                         AUSA07052008 8MM  251 12 UN0807122017   35.50S                     34.51S 0 0 3 2  0 41       5  01      NN89
D3                                                                                                                                                           N67
D01OH      Kenney, William                         AUSA07052008 8MM  252 32 UN0807122017                              41.25S 0 0 2 3  0 25       1  02      NN09
D01OH      Kenney, William                         AUSA07052008 8MM  253 52 UN0807122017   40.75S                     40.96S 0 0 2 4  0 13       2  03      NN89
D01OH      Kimble, Adam                            AUSA0724200115MM  501 20 151807112017   28.60S   28.11S            27.76S 4 5 1 3  7  7  7.   1  0100    NN29
D3                                                                                                                                                           N67
D01OH      Kimble, Adam                            AUSA0724200115MM 1001 50 151807122017 1:04.43S                   1:03.02S 0 0 3 6  0  8  5.   6  0100    NN88
G01OH          Kimble, Adam                            1 2  50C   29.44 1:03.02                                                                F             N63
D01OH      Kimble, Adam                            AUSA0724200115MM  503 60 151807122017   38.56S                     38.14S 0 0 3 1  0  6  9.   5  0300    NN48
D01OH      Kleier, Jenny                           AUSA0129200116FF  501 19 151807112017   33.52S   32.88S                   5 1 0 0 13             01      NN58
D3                                                                                                                                                           N67
D01OH      Kleier, Jenny                           AUSA0129200116FF  504 29 151807112017   39.01S   38.24S            37.17S 1 2 1 1 11 10  3.   4  0400    NN99
D01OH      Kleier, Jenny                           AUSA0129200116FF  503 59 151807122017   43.92S                     42.52S 0 0 4 6  0  6  9.   6  0300    NN09
D01OH      Koenig, Alexis                          AUSA11042010 6FF  251 11 UN0807122017   32.01S                     31.32S 0 0 6 1  0 33       4  01      NN19
D3                                                                                                                                                           N67
D01OH      Koenig, Alexis                          AUSA11042010 6FF  252 31 UN0807122017   39.61S                     38.99S 0 0 3 6  0 31       3  02      NN39
G01OH          Koenig, Alexis                          1 3  25C                   38.99                                                        F             N04
D01OH      Koenig, Alexis                          AUSA11042010 6FF  253 51 UN0807122017                                 DQS 0 0 1 4  0  0       0  03      NN38
D01OH      Koenig, Zackary                         AUSA11032007 9MM  251 14 091007122017   20.65S                     19.22S 0 0 4 2  0 15       3  01      NN49
D3                                                                                                                                                           N67
D01OH      Koenig, Zackary                         AUSA11032007 9MM  252 34 091007122017   26.53S                     26.35S 0 0 4 4  0 14       3  02      NN49
G01OH          Koenig, Zackary                         1 2  25C           26.35                                                                F             N34
D01OH      Koenig, Zackary                         AUSA11032007 9MM  253 54 091007122017   32.78S                     36.05S 0 0 2 4  0 22       3  03      NN49
D01OH      Martin, Chloe                           AUSA10242008 8FF  254 21 UN0807122017   27.84S                     28.84S 0 0 3 6  0  7  7.   6  0400    NN29
D3                                                                                                                                                           N67
D01OH      Martin, Chloe                           AUSA10242008 8FF  501 41 UN0807122017   50.96S                     52.69S 0 0 5 4  0  7  7.   2  0100    NN29
D01OH      Martin, Chloe                           AUSA10242008 8FF  253 51 UN0807122017   35.31S                     40.28S 0 0 4 4  0 20       5  03      NN88
D01OH      Martin, Haley                           AUSA0426200512FF  251 15 111207112017   15.78S   16.19S                   5 2 0 0 14             01      NN48
D3                                                                                                                                                           N67
D01OH      Martin, Haley                           AUSA0426200512FF  503 55 111207122017   51.77S                     47.83S 0 0 4 5  0 10  3.   1  0300    NN09
D01OH      Martin, Haley                           AUSA0426200512FF 1005 65 111207122017 1:35.82S                   1:33.71S 0 0 2 1  0 12  1.   6  0500    NN59
G01OH          Martin, Haley                           1 2  50C         1:33.71                                                                F             N73
D01OH      Mastin, Camden                          AUSA06082010 6MM  251 12 UN0807122017   28.47S                     30.13S 0 0 6 2  0 30       6  01      NN29
D3                                                                                                                                                           N67
D01OH      Mastin, Camden                          AUSA06082010 6MM  252 32 UN0807122017   31.42S                     33.56S 0 0 7 6  0 17       5  02      NN39
D01OH      Mastin, Camden                          AUSA06082010 6MM  501 42 UN0807122017                            1:09.96S 0 0 1 4  0 25       2  01      NN78
D01OH      Mayne, Ally                             AUSA0612200214FF  504 27 131407112017   43.84S   43.28S            44.56S 3 1 1 6 12 12  1.   6  0400    NN19
D3                                                                                                                                                           N67
D01OH      Mayne, Ally                             AUSA0612200214FF  502 37 131407112017   50.86S   48.10S                   1 3 0 0 15             02      NN67
D01OH      Mayne, Ally                             AUSA0612200214FF 1001 47 131407112017 1:33.19S 1:36.86S                   2 5 0 0 15             01      NN28
G01OH          Mayne, Ally                             1 2  50C   45.53 1:36.86                                                                P             N63
D01OH      Mayne, Griffin                          AUSA0511200116MM  504 30 151807112017   34.32S   34.05S            33.89S 3 5 1 5 10 11  2.   5  0400    NN20
D3                                                                                                                                                           N67
D01OH      Mayne, Griffin                          AUSA0511200116MM  502 40 151807112017   40.18S   38.64S            37.44S 3 1 1 6 12 10  3.   4  0200    NN20
D01OH      Mayne, Griffin                          AUSA0511200116MM 1005 70 151807122017 1:21.83S                   1:19.25S 0 0 2 2  0 13       2  05      NN59
G01OH          Mayne, Griffin                          1 2  50C   36.94 1:19.25                                                                F             N64
D01OH      Melton, Floyd                           AUSA0210200710MM  254 24 091007122017   22.02S                     21.50S 0 0 3 2  0  8  5.   2  0400    NN98
D3                                                                                                                                                           N67
D01OH      Melton, Floyd                           AUSA0210200710MM  252 34 091007122017   24.42S                     22.15S 0 0 5 2  0  6  9.   1  0200    NN09
D01OH      Melton, Floyd                           AUSA0210200710MM  501 44 091007122017                              40.37S 0 0 1 2  0  4 11.   1  0100    NN28
D01OH      Melton, Raleigh                         AUSA0827200313FF  501 17 131407112017   38.63S   37.79S            38.73S 3 6 1 6 12 12  1.   6  0100    NN70
D3                                                                                                                                                           N67
D01OH      Melton, Raleigh                         AUSA0827200313FF  502 37 131407112017   49.38S   51.39S                   2 6 0 0 18             02      NN39
D01OH      Melton, Raleigh                         AUSA0827200313FF 1001 47 131407112017 1:24.98S 1:24.51S          1:22.70S 2 2 1 4  8  8  5.   2  0100    NN21
G01OH          Melton, Raleigh                         1 2  50C   41.44 1:24.51                                                                P             N05
G01OH          Melton, Raleigh                         1 2  50C   39.92 1:22.70                                                                F             N05
D01OH      Miller, Grace                           AUSA0303200512FF  504 25 111207112017   38.57S   37.03S            37.26S 2 4 2 6  7  6  9.   6  0400    NN69
D3                                                                                                                                                           N67
D01OH      Miller, Grace                           AUSA0303200512FF  501 45 111207112017   34.55S   35.28S            35.04S 8 5 1 6 13 11  2.   5  0100    NN79
D01OH      Miller, Grace                           AUSA0303200512FF 1005 65 111207122017 1:28.94S                   1:25.24S 0 0 2 2  0  6  9.   1  0500    NN39
G01OH          Miller, Grace                           1 2  50C   39.79 1:25.24                                                                F             N24
D01OH      Miller, Sarah                           AUSA1207200313FF  501 17 131407112017   33.32S   33.35S            33.76S 2 4 1 3  7  7  7.   1  0100    NN69
D3                                                                                                                                                           N67
D01OH      Miller, Sarah                           AUSA1207200313FF  504 27 131407112017   39.78S   41.58S            42.80S 1 2 1 5 10 10  3.   4  0400    NN89
D01OH      Miller, Sarah                           AUSA1207200313FF 1001 47 131407112017 1:19.02S 1:19.49S          1:19.50S 1 4 2 1  5  5 10.   5  0100    NN40
G01OH          Miller, Sarah                           1 2  50C   36.63 1:19.49                                                                P             N34
G01OH          Miller, Sarah                           1 2  50C   38.15 1:19.50                                                                F             N24
D01OH      Moody, Sam                              AUSA1111200115MM  504 30 151807112017               DQS                   1 5 0 0  0             04      NN46
D3                                                                                                                                                           N67
D01OH      Moody, Sam                              AUSA1111200115MM  502 40 151807112017   46.66S   47.14S                   2 6 0 0 17             02      NN47
D01OH      Moody, Sam                              AUSA1111200115MM  503 60 151807122017   47.19S                        NSS 0 0 1 5  0  0       0  03      NN57
D01OH      Nicholson, Ryder                        AUSA09032007 9MM  254 24 091007122017                                 DQS 0 0 1 4  0  0       0  04      NN09
D3                                                                                                                                                           N67
D01OH      Nicholson, Ryder                        AUSA09032007 9MM  252 34 091007122017   27.82S                     24.69S 0 0 4 6  0 10  3.   1  0200    NN30
G01OH          Nicholson, Ryder                        1 2  25C           24.69                                                                F             N84
D01OH      Nicholson, Ryder                        AUSA09032007 9MM  501 44 091007122017                              47.12S 0 0 1 5  0 14       4  01      NN29
D01OH      O'Toole, Maggie                         AUSA0501200611FF  504 25 111207112017   36.32S   35.72S                   4 4 0 0  4             04      NN58
D3                                                                                                                                                           N67
D01OH      O'Toole, Maggie                         AUSA0501200611FF  502 35 111207112017   36.77S   36.57S                   3 3 0 0  2             02      NN58
D01OH      O'Toole, Maggie                         AUSA0501200611FF  501 45 111207112017   31.19S   31.24S                   7 3 0 0  6             01      NN58
D01OH      Pippenger, Emmy                         AUSA0601200512FF  251 15 111207112017   15.61S   16.10S            15.96S 6 2 1 1 11 10  3.   4  0100    NN60
D3                                                                                                                                                           N67
D01OH      Pippenger, Emmy                         AUSA0601200512FF  502 35 111207112017   41.06S   39.34S            39.61S 3 4 2 5  5  4 11.   4  0200    NN60
D01OH      Pippenger, Emmy                         AUSA0601200512FF  501 45 111207112017   35.03S   35.44S                   6 5 0 0 14             01      NN29
D01OH      Pippenger, Jack                         AUSA1214200115MM  501 20 151807112017   28.80S   28.24S            28.09S 3 5 1 5  9  9  4.   3  0100    NN50
D3                                                                                                                                                           N67
D01OH      Pippenger, Jack                         AUSA1214200115MM 1005 70 151807122017 1:15.38S                   1:16.85S 0 0 3 5  0 10  3.   5  0500    NN30
G01OH          Pippenger, Jack                         1 2  50C   36.40 1:16.85                                                                F             N94
D01OH      Pippenger, Jack                         AUSA1214200115MM  504 30 151807112017   31.57S   33.32S            32.52S 3 2 1 2  9  8  5.   2  0400    NN40
D01OH      Preston, Evie                           AUSA09202008 8FF  254 21 UN0807122017   21.68S                     22.16S 0 0 3 4  0  2 13.   2  0400    NN39
D3                                                                                                                                                           N67
D01OH      Preston, Evie                           AUSA09202008 8FF  501 41 UN0807122017   42.16S                     40.57S 0 0 6 4  0  1 16.   1  0100    NN39
D01OH      Preston, Evie                           AUSA09202008 8FF  253 51 UN0807122017   26.75S                     25.85S 0 0 6 3  0  1 16.   1  0300    NN49
D01OH      Ray, Garrett                            AUSA06232009 7MM  254 22 UN0807122017   46.73S                     44.54S 0 0 1 3  0 11  2.   3  0400    NN09
D3                                                                                                                                                           N67
D01OH      Ray, Garrett                            AUSA06232009 7MM  501 42 UN0807122017 1:10.46S                   1:09.77S 0 0 3 3  0 24       4  01      NN19
D01OH      Ray, Garrett                            AUSA06232009 7MM  253 52 UN0807122017   44.09S                        DQS 0 0 2 5  0  0       0  03      NN58
D01OH      Ray, Mia                                AUSA1113200610FF  251 13 091007122017   17.44S                     17.81S 0 0 6 1  0  5 10.   5  0100    NN96
D3                                                                                                                                                           N67
D01OH      Ray, Mia                                AUSA1113200610FF  254 23 091007122017   22.54S                     23.45S 0 0 3 5  0 14       5  04      NN66
D01OH      Ray, Mia                                AUSA1113200610FF  501 43 091007122017   39.20S                     39.96S 0 0 6 1  0  7  7.   5  0100    NN96
D01OH      Rivard, Ava                             AUSA1127200511FF  251 15 111207112017   16.92S   17.03S                   6 6 0 0 20             01      NN57
D3                                                                                                                                                           N67
D01OH      Rivard, Ava                             AUSA1127200511FF  501 45 111207112017            40.43S                   2 1 0 0 27             01      NN86
D01OH      Rivard, Ava                             AUSA1127200511FF  503 55 111207122017   49.58S                        DQS 0 0 5 6  0  0       0  03      NN77
D01OH      Rivard, Christian                       AUSA10022007 9MM  254 24 091007122017   26.21S                     24.68S 0 0 2 5  0 16       3  04      NN20
D3                                                                                                                                                           N67
D01OH      Rivard, Christian                       AUSA10022007 9MM  253 54 091007122017   24.28S                     24.23S 0 0 5 5  0  5 10.   4  0300    NN60
D01OH      Rivard, Christian                       AUSA10022007 9MM 1005 64 091007122017 2:01.00S                   2:01.43S 0 0 2 2  0  9  4.   2  0500    NN90
G01OH          Rivard, Christian                       1 2  50C   57.64 2:01.43                                                                F             N85
D01OH      Rivard, Elle                            AUSA12102009 7FF  251 11 UN0807122017   32.83S                     30.49S 0 0 5 2  0 31       3  01      NN48
D3                                                                                                                                                           N67
D01OH      Rivard, Elle                            AUSA12102009 7FF  252 31 UN0807122017   32.79S                     33.55S 0 0 6 3  0 18       2  02      NN58
D01OH      Rivard, Elle                            AUSA12102009 7FF  253 51 UN0807122017   32.85S                     33.00S 0 0 5 5  0 13       5  03      NN48
D01OH      Roberts, Isabelle                       AUSA0923199917FF  501 19 151807112017   34.63S   34.46S                   3 1 0 0 15             01      NN10
D3                                                                                                                                                           N67
D01OH      Roberts, Isabelle                       AUSA0923199917FF  504 29 151807112017   39.16S   39.08S            37.42S 3 5 1 6 12 11  2.   5  0400    NN61
D01OH      Roberts, Isabelle                       AUSA0923199917FF  502 39 151807112017   40.67S   39.93S            38.39S 4 2 1 1 11  9  4.   3  0200    NN61
D01OH      Root, Brenna                            AUSA0606200610FF  251 13 091007122017   21.38S                     20.61S 0 0 4 4  0 14       1  01      NN28
D3                                                                                                                                                           N67
D01OH      Root, Brenna                            AUSA0606200610FF  501 43 091007122017   50.04S                     48.25S 0 0 3 6  0 23       5  01      NN28
D01OH      Root, Brenna                            AUSA0606200610FF  253 53 091007122017                                 DQS 0 0 1 5  0  0       0  03      NN37
D01OH      Schulok, Joey                           AUSA1118200313MM  502 38 131407112017   39.20S   39.37S            38.44S 2 4 2 1  5  5 10.   5  0200    NN00
D3                                                                                                                                                           N67
D01OH      Schulok, Joey                           AUSA1118200313MM 1001 48 131407112017 1:17.39S 1:17.07S          1:16.80S 2 2 1 5 10 11  2.   5  0100    NN70
G01OH          Schulok, Joey                           1 2  50C   37.52 1:17.07                                                                P             N44
G01OH          Schulok, Joey                           1 2  50C   37.17 1:16.80                                                                F             N34
D01OH      Schulok, Joey                           AUSA1118200313MM  503 58 131407122017   44.20S                     43.77S 0 0 3 6  0  8  5.   5  0300    NN19
D01OH      Schulok, Sydney                         AUSA0214200017FF  501 19 151807112017   30.95S   30.51S            30.15S 3 3 2 4  2  3 12.   3  0100    NN60
D3                                                                                                                                                           N67
D01OH      Schulok, Sydney                         AUSA0214200017FF  502 39 151807112017   36.62S   36.76S            37.66S 4 4 2 6  6  6  9.   6  0200    NN80
D01OH      Schulok, Sydney                         AUSA0214200017FF  503 59 151807122017   41.04S                     40.63S 0 0 4 1  0  5 10.   5  0300    NN99
D01OH      Sells, Bennett                          AUSA0222200413MM  501 18 131407112017   35.12S   33.24S            33.70S 3 1 1 1 11 11  2.   5  0100    NN20
D3                                                                                                                                                           N67
D01OH      Sells, Bennett                          AUSA0222200413MM  502 38 131407112017   44.07S   44.87S            45.10S 1 2 1 6 12 12  1.   6  0200    NN30
D01OH      Sells, Bennett                          AUSA0222200413MM 1001 48 131407112017 1:23.50S 1:17.44S          1:17.36S 3 1 1 6 12 12  1.   6  0100    NN01
G01OH          Sells, Bennett                          1 2  50C   36.24 1:17.44                                                                P             N74
G01OH          Sells, Bennett                          1 2  50C   36.95 1:17.36                                                                F             N74
D01OH      Shingleton, Spencer                     AUSA0706200214MM 1001 48 131407112017 1:21.20S 1:15.04S          1:12.96S 2 5 1 4  8  9  4.   3  0100    NN92
D3                                                                                                                                                           N67
G01OH          Shingleton, Spencer                     1 2  50C   35.41 1:15.04                                                                P             N76
G01OH          Shingleton, Spencer                     1 2  50C   34.62 1:12.96                                                                F             N76
D01OH      Shingleton, Spencer                     AUSA0706200214MM  503 58 131407122017   49.41S                     45.44S 0 0 2 5  0 10  3.   4  0300    NN51
D01OH      Shingleton, Spencer                     AUSA0706200214MM 1005 68 131407122017 1:30.77S                   1:27.99S 0 0 1 3  0  7  7.   1  0500    NN02
G01OH          Shingleton, Spencer                     1 2  50C   42.28 1:27.99                                                                F             N76
D01OH      Sievers, Anna                           AUSA0521200611FF  251 15 111207112017   23.59S   23.79S                   1 4 0 0 39             01      NN48
D3                                                                                                                                                           N67
D01OH      Sievers, Anna                           AUSA0521200611FF  502 35 111207112017 1:07.07S 1:10.85S                   1 2 0 0 24             02      NN88
D01OH      Sievers, Anna                           AUSA0521200611FF  501 45 111207112017   55.49S 1:01.31S                   2 2 0 0 43             01      NN68
D01OH      Teter, Jon                              AUSA0918200313MM  501 18 131407112017   30.31S   28.17S            28.20S 3 4 2 4  2  2 13.   2  0100    NN78
D3                                                                                                                                                           N67
D01OH      Teter, Jon                              AUSA0918200313MM 1001 48 131407112017 1:02.63S 1:02.48S          1:01.17S 3 3 2 3  1  1 16.   1  0100    NN49
G01OH          Teter, Jon                              1 2  50C         1:02.48                                                                P             N72
G01OH          Teter, Jon                              1 2  50C   29.75 1:01.17                                                                F             N13
D01OH      Teter, Jon                              AUSA0918200313MM  502 38 131407112017   37.91S   35.53S            38.08S 3 4 2 2  3  4 11.   4  0200    NN88
D01OH      Turchiano, Emma                         AUSA0714200412FF  251 15 111207112017   16.92S   15.76S            16.11S 7 6 1 2  9 11  2.   5  0100    NN50
D3                                                                                                                                                           N67
D01OH      Turchiano, Emma                         AUSA0714200412FF  501 45 111207112017   43.01S   37.83S                   4 5 0 0 20             01      NN19
D01OH      Turchiano, Emma                         AUSA0714200412FF  503 55 111207122017   53.82S                     54.55S 0 0 4 6  0 21       6  03      NN49
D01OH      Turner, Aaron                           AUSA07022009 7MM  254 22 UN0807122017   33.15S                     33.72S 0 0 2 2  0  8  5.   2  0400    NN39
D3                                                                                                                                                           N67
D01OH      Turner, Aaron                           AUSA07022009 7MM  501 42 UN0807122017 1:10.74S                     54.98S 0 0 3 4  0 12  1.   1  0100    NN69
D01OH      Turner, Aaron                           AUSA07022009 7MM  253 52 UN0807122017   38.82S                     40.65S 0 0 3 6  0 12  1.   4  0300    NN59
D01OH      Turner, Aiden                           AUSA07022009 7MM  251 12 UN0807122017   35.05S                     31.44S 0 0 4 6  0 34       4  01      NN09
D3                                                                                                                                                           N67
D01OH      Turner, Aiden                           AUSA07022009 7MM  501 42 UN0807122017 1:16.73S                   1:45.36S 0 0 3 6  0 31       6  01      NN59
D01OH      Turner, Aiden                           AUSA07022009 7MM  253 52 UN0807122017                                 DQS 0 0 1 1  0  0       0  03      NN08
D01OH      Turner, Daniel                          AUSA0817200214MM  501 18 131407112017   31.54S   31.15S            31.14S 3 2 1 3  7  7  7.   1  0100    NN10
D3                                                                                                                                                           N67
D01OH      Turner, Daniel                          AUSA0817200214MM  504 28 131407112017   34.70S   34.39S            34.16S 2 2 2 1  5  5 10.   5  0400    NN30
D01OH      Turner, Daniel                          AUSA0817200214MM 1001 48 131407112017 1:10.87S 1:09.13S          1:09.30S 2 4 2 5  4  4 11.   4  0100    NN01
G01OH          Turner, Daniel                          1 2  50C   33.04 1:09.13                                                                P             N74
G01OH          Turner, Daniel                          1 2  50C   33.52 1:09.30                                                                F             N64
D01OH      Waligura, Amy                           AUSA1219200610FF  251 13 091007122017   21.19S                     21.09S 0 0 4 3  0 18       4  01      NN68
D3                                                                                                                                                           N67
D01OH      Waligura, Amy                           AUSA1219200610FF  252 33 091007122017                              26.63S 0 0 1 2  0 14       1  02      NN97
G01OH          Waligura, Amy                           1 4  25C   15.68   26.22   51.05   26.63                                                F             N05
D01OH      Waligura, Amy                           AUSA1219200610FF  253 53 091007122017   25.27S                     25.29S 0 0 3 3  0 10  3.   4  0300    NN09
D01OH      Wiethorn, Craig                         AUSA08212007 9MM  251 14 091007122017   28.56S                        NSS 0 0 1 4  0  0       0  01      NN39
D3                                                                                                                                                           N67
D01OH      Wiethorn, Craig                         AUSA08212007 9MM  501 44 091007122017 1:09.22S                        NSS 0 0 2 6  0  0       0  01      NN59
D01OH      Wiethorn, Craig                         AUSA08212007 9MM  253 54 091007122017                                 NSS 0 0 1 3  0  0       0  03      NN68
D01OH      Williams, Glynn                         AUSA0907200115MM  501 20 151807112017   32.33S   31.12S                   2 4 0 0 19             01      NN49
D3                                                                                                                                                           N67
D01OH      Williams, Glynn                         AUSA0907200115MM  502 40 151807112017   40.97S   39.72S                   1 1 0 0 13             02      NN49
D01OH      Williams, Glynn                         AUSA0907200115MM 1001 50 151807122017 1:13.80S                   1:09.66S 0 0 1 2  0 15       3  01      NN10
G01OH          Williams, Glynn                         1 2  50C   33.07 1:09.66                                                                F             N25
D01OH      Withers, Allison                        AUSA1028200511FF  251 15 111207112017   19.12S   19.95S                   2 3 0 0 36             01      NN79
D3                                                                                                                                                           N67
D01OH      Withers, Allison                        AUSA1028200511FF  502 35 111207112017   55.92S   53.55S                   2 6 0 0 19             02      NN89
D01OH      Withers, Allison                        AUSA1028200511FF  501 45 111207112017   43.19S   47.60S                   4 1 0 0 39             01      NN79
D01OH      Withers, Ava                            AUSA06022010 6FF  251 11 UN0807122017   31.25S                     32.25S 0 0 6 3  0 36       5  01      NN58
D3                                                                                                                                                           N67
D01OH      Withers, Ava                            AUSA06022010 6FF  252 31 UN0807122017   33.95S                     33.09S 0 0 5 2  0 16       3  02      NN58
D01OH      Withers, Ava                            AUSA06022010 6FF  501 41 UN0807122017                            1:10.27S 0 0 1 3  0 22       2  01      NN97
E01OH      AOHFH    F 1007  1 UN08  007122017 1:54.43S                   1:41.68S     1 4     2 26.                                            2N   0500X    N05
F01OH          OHFH  AMartin, Chloe                           USA102420088 F  1                                                                              N25
G01OH          Martin, Chloe                           1 4  25C                                                                                F             N03
F01OH          OHFH  ARivard, Elle                            USA121020097 F  2                                                                              N74
G01OH          Rivard, Elle                            1 4  25C   58.48                                                                        F             N13
F01OH          OHFH  APreston, Evie                           USA092020088 F  3                                                                              N35
G01OH          Preston, Evie                           1 4  25C                                                                                F             N23
F01OH          OHFH  ABruenderman, Riley                      USA031520098 F  4                                                                              N27
G01OH          Bruenderman, Riley                      1 4  25C 1:41.68                                                                        F             N85
E01OH      AOHFH    M 1007  2 UN08  007122017 2:11.46S                   2:13.25S     1 1     3 24.                                            3N   0500X    N05
F01OH          OHFH  AMastin, Camden                          USA060820106 M  1                                                                              N55
G01OH          Mastin, Camden                          1 4  25C                                                                                F             N33
F01OH          OHFH  ADellecave, Gideon                       USA022120107 M  2                                                                              N56
G01OH          Dellecave, Gideon                       1 4  25C 1:26.44                                                                        F             N15
F01OH          OHFH  ABurke, Nolan                            USA082220088 M  3                                                                              N94
G01OH          Burke, Nolan                            1 4  25C                                                                                F             N62
F01OH          OHFH  ATurner, Aaron                           USA070220097 M  4                                                                              N45
G01OH          Turner, Aaron                           1 4  25C 2:13.25                                                                        F             N83
E01OH      AOHFH    F 1007  3 0910  007122017 1:28.18S                   1:25.30S     1 2     3 24.                                            3N   0500X    N64
F01OH          OHFH  AGlynn, Emma                             USA1012200610F  1                                                                              N44
G01OH          Glynn, Emma                             1 4  25C                                                                                F             N32
F01OH          OHFH  AWaligura, Amy                           USA1219200610F  2                                                                              N35
G01OH          Waligura, Amy                           1 4  25C   48.98                                                                        F             N63
F01OH          OHFH  AJohnson, McKENNA                        USA052820089 F  3                                                                              N65
G01OH          Johnson, McKENNA                        1 4  25C                                                                                F             N43
F01OH          OHFH  ARay, Mia                                USA1113200610F  4                                                                              N23
G01OH          Ray, Mia                                1 4  25C 1:25.30                                                                        F             N71
E01OH      AOHFH    M 1007  4 0910  007122017 1:25.20S                   1:24.96S     1 4     4 22.                                            4N   0500X    N74
F01OH          OHFH  ACrowley, Mike                           USA0618200610M  1                                                                              N45
G01OH          Crowley, Mike                           1 4  25C                                                                                F             N13
F01OH          OHFH  AHeis, Max                               USA120820079 M  2                                                                              N73
G01OH          Heis, Max                               1 4  25C   45.33                                                                        F             N91
F01OH          OHFH  AMelton, Floyd                           USA0210200710M  3                                                                              N35
G01OH          Melton, Floyd                           1 4  25C                                                                                F             N13
F01OH          OHFH  ABruenderman, Logan                      USA0123200710M  4                                                                              N27
G01OH          Bruenderman, Logan                      1 4  25C 1:24.96                                                                        F             N75
E01OH      AOHFH    F 2007  5 1112  007122017 2:17.69S                   2:20.57S     1 3     1 32.                                            1N   0500X    N74
F01OH          OHFH  AJohnson, Alexandra                      USA0412200512F  1                                                                              N27
G01OH          Johnson, Alexandra                      1 4  50C   33.60                                                                        F             N45
F01OH          OHFH  AJohnson, Lucia                          USA0412200512F  2                                                                              N75
G01OH          Johnson, Lucia                          1 4  50C 1:15.46                                                                        F             N24
F01OH          OHFH  AButkovich, Molly                        USA0715200412F  3                                                                              N66
G01OH          Butkovich, Molly                        1 4  50C 1:46.82                                                                        F             N15
F01OH          OHFH  AMiller, Grace                           USA0303200512F  4                                                                              N15
G01OH          Miller, Grace                           1 4  50C 2:20.57                                                                        F             N63
E01OH      AOHFH    M 2007  6 1112  007122017 3:13.42S                   2:58.57S     1 1     4 22.                                            4N   0500X    N74
F01OH          OHFH  AHeis, Forest                            USA1212200412M  1                                                                              N94
G01OH          Heis, Forest                            1 4  50C   48.24                                                                        F             N23
F01OH          OHFH  ARivard, Christian                       USA100220079 M  2                                                                              N86
G01OH          Rivard, Christian                       1 4  50C 1:42.51                                                                        F             N35
F01OH          OHFH  ADostal, Justin                          USA0220200512M  3                                                                              N85
G01OH          Dostal, Justin                          1 4  50C 2:11.21                                                                        F             N24
F01OH          OHFH  AGawronski, James                        USA0718200511M  4                                                                              N56
G01OH          Gawronski, James                        1 4  50C 2:58.57                                                                        F             N05
E01OH      AOHFH    F 2007  7 1314  007122017 2:26.89S                   2:25.01S     1 4     2 26.                                            2N   0500X    N74
F01OH          OHFH  AHeis, Brooke                            USA1010200313F  1                                                                              N84
G01OH          Heis, Brooke                            1 4  50C   37.74                                                                        F             N13
F01OH          OHFH  AGeiger, Olivia                          USA1003200313F  2                                                                              N55
G01OH          Geiger, Olivia                          1 4  50C 1:17.58                                                                        F             N14
F01OH          OHFH  ACrowley, Maggie                         USA0510200413F  3                                                                              N06
G01OH          Crowley, Maggie                         1 4  50C 1:54.17                                                                        F             N54
F01OH          OHFH  AButkovich, Maggie                       USA1211200313F  4                                                                              N76
G01OH          Butkovich, Maggie                       1 4  50C 2:25.01                                                                        F             N25
E01OH      AOHFH    M 2007  8 1314  007122017 2:18.57S                   2:14.52S     1 3     1 32.                                            1N   0500X    N74
F01OH          OHFH  ATeter, Jon                              USA0918200313M  1                                                                              N24
G01OH          Teter, Jon                              1 4  50C   34.35                                                                        F             N42
F01OH          OHFH  AGrandy, Jack                            USA0730200313M  2                                                                              N84
G01OH          Grandy, Jack                            1 4  50C 1:10.42                                                                        F             N23
F01OH          OHFH  ATurner, Daniel                          USA0817200214M  3                                                                              N75
G01OH          Turner, Daniel                          1 4  50C 1:43.55                                                                        F             N24
F01OH          OHFH  AGrimes, Kyle                            USA1102200313M  4                                                                              N94
G01OH          Grimes, Kyle                            1 4  50C 2:14.52                                                                        F             N43
E01OH      AOHFH    F 2007  9 1518  007122017 2:24.53S                   2:22.11S     1 2     3 24.                                            3N   0500X    N74
F01OH          OHFH  ASchulok, Sydney                         USA0214200017F  1                                                                              N26
G01OH          Schulok, Sydney                         1 4  50C   36.91                                                                        F             N54
F01OH          OHFH  AHeis, Rachel                            USA1220200016F  2                                                                              N74
G01OH          Heis, Rachel                            1 4  50C 1:16.18                                                                        F             N23
F01OH          OHFH  AGrimes, Kelly                           USA0130199918F  3                                                                              N45
G01OH          Grimes, Kelly                           1 4  50C 1:49.89                                                                        F             N93
F01OH          OHFH  AGeis, Erin                              USA0219200215F  4                                                                              N14
G01OH          Geis, Erin                              1 4  50C 2:22.11                                                                        F             N52
E01OH      AOHFH    M 2007 10 1518  007122017 2:03.58S                   1:55.90S     1 3     2 26.                                            2N   0500X    N84
F01OH          OHFH  APippenger, Jack                         USA1214200115M  1                                                                              N06
G01OH          Pippenger, Jack                         1 4  50C   31.15                                                                        F             N24
F01OH          OHFH  AButkovich, Patrick                      USA0108199918M  2                                                                              N47
G01OH          Butkovich, Patrick                      1 4  50C 1:02.41                                                                        F             N75
F01OH          OHFH  ADonnels, Carter                         USA0309200017M  3                                                                              N16
G01OH          Donnels, Carter                         1 4  50C 1:29.23                                                                        F             N54
F01OH          OHFH  AGrace, Joey                             USA0630199917M  4                                                                              N64
G01OH          Grace, Joey                             1 4  50C 1:55.90                                                                        F             N92
E01OH      AOHFH    F 1006 61 UN08  007122017 1:41.80S                   1:42.62S     1 2     5 20.                                            5N   0100X    N15
F01OH          OHFH  ABruenderman, Riley                      USA031520098 F  1                                                                              N27
G01OH          Bruenderman, Riley                      1 4  25C                                                                                F             N05
F01OH          OHFH  AWithers, Ava                            USA060220106 F  2                                                                              N84
G01OH          Withers, Ava                            1 4  25C 1:00.59                                                                        F             N43
F01OH          OHFH  AMartin, Chloe                           USA102420088 F  3                                                                              N25
G01OH          Martin, Chloe                           1 4  25C                                                                                F             N03
F01OH          OHFH  APreston, Evie                           USA092020088 F  4                                                                              N35
G01OH          Preston, Evie                           1 4  25C 1:42.62                                                                        F             N93
E01OH      AOHFH    M 1006 62 UN08  007122017 1:47.76S                   1:48.42S     1 1     5 20.                                            5N   0100X    N25
F01OH          OHFH  ATurner, Aaron                           USA070220097 M  1                                                                              N35
G01OH          Turner, Aaron                           1 4  25C                                                                                F             N13
F01OH          OHFH  ARay, Garrett                            USA062320097 M  2                                                                              N05
G01OH          Ray, Garrett                            1 4  25C   58.48                                                                        F             N33
F01OH          OHFH  AMastin, Camden                          USA060820106 M  3                                                                              N55
G01OH          Mastin, Camden                          1 4  25C                                                                                F             N33
F01OH          OHFH  ABurke, Nolan                            USA082220088 M  4                                                                              N94
G01OH          Burke, Nolan                            1 4  25C 1:48.42                                                                        F             N43
E01OH      AOHFH    F 1006 71 0910  007122017 1:18.53S                   1:18.15S     1 2     5 20.                                            5N   0100X    N74
F01OH          OHFH  AJohnson, McKENNA                        USA052820089 F  1                                                                              N65
G01OH          Johnson, McKENNA                        1 4  25C                                                                                F             N43
F01OH          OHFH  AGlynn, Emma                             USA1012200610F  2                                                                              N44
G01OH          Glynn, Emma                             1 4  25C   37.82                                                                        F             N82
F01OH          OHFH  AWaligura, Amy                           USA1219200610F  3                                                                              N35
G01OH          Waligura, Amy                           1 4  25C                                                                                F             N13
F01OH          OHFH  ARay, Mia                                USA1113200610F  4                                                                              N23
G01OH          Ray, Mia                                1 4  25C 1:18.15                                                                        F             N81
E01OH      AOHFH    M 1006 72 0910  007122017 1:15.82S                   1:13.09S     1 2     3 24.                                            3N   0100X    N84
F01OH          OHFH  AHeis, Max                               USA120820079 M  1                                                                              N73
G01OH          Heis, Max                               1 4  25C                                                                                F             N41
F01OH          OHFH  ABruenderman, Logan                      USA0123200710M  2                                                                              N27
G01OH          Bruenderman, Logan                      1 4  25C   36.58                                                                        F             N55
F01OH          OHFH  AMelton, Floyd                           USA0210200710M  3                                                                              N35
G01OH          Melton, Floyd                           1 4  25C                                                                                F             N13
F01OH          OHFH  ACrowley, Mike                           USA0618200610M  4                                                                              N45
G01OH          Crowley, Mike                           1 4  25C 1:13.09                                                                        F             N83
E01OH      AOHFH    F 2006 73 1112  007122017 2:07.31S                   2:06.81S     1 3     1 32.                                            1N   0100X    N74
F01OH          OHFH  AButkovich, Molly                        USA0715200412F  1                                                                              N66
G01OH          Butkovich, Molly                        1 4  50C   32.03                                                                        F             N84
F01OH          OHFH  AJohnson, Lucia                          USA0412200512F  2                                                                              N75
G01OH          Johnson, Lucia                          1 4  50C 1:02.92                                                                        F             N24
F01OH          OHFH  AMiller, Grace                           USA0303200512F  3                                                                              N15
G01OH          Miller, Grace                           1 4  50C 1:37.18                                                                        F             N63
F01OH          OHFH  AJohnson, Alexandra                      USA0412200512F  4                                                                              N27
G01OH          Johnson, Alexandra                      1 4  50C 2:06.81                                                                        F             N75
E01OH      AOHFH    M 2006 74 1112  007122017 2:47.96S                   2:49.07S     1 6     5 20.                                            5N   0100X    N94
F01OH          OHFH  AHeis, Forest                            USA1212200412M  1                                                                              N94
G01OH          Heis, Forest                            1 4  50C   42.19                                                                        F             N23
F01OH          OHFH  AGawronski, James                        USA0718200511M  2                                                                              N56
G01OH          Gawronski, James                        1 4  50C 1:31.86                                                                        F             N05
F01OH          OHFH  AKoenig, Zackary                         USA110320079 M  3                                                                              N06
G01OH          Koenig, Zackary                         1 4  50C 2:22.16                                                                        F             N54
F01OH          OHFH  ADostal, Justin                          USA0220200512M  4                                                                              N85
G01OH          Dostal, Justin                          1 4  50C 2:49.07                                                                        F             N34
E01OH      AOHFH    F 2006 75 1314  007122017 2:16.63S                   2:08.73S     1 5     2 26.                                            2N   0100X    N84
F01OH          OHFH  AButkovich, Maggie                       USA1211200313F  1                                                                              N76
G01OH          Butkovich, Maggie                       1 4  50C   31.85                                                                        F             N05
F01OH          OHFH  AGeiger, Olivia                          USA1003200313F  2                                                                              N55
G01OH          Geiger, Olivia                          1 4  50C 1:05.19                                                                        F             N04
F01OH          OHFH  AHeis, Brooke                            USA1010200313F  3                                                                              N84
G01OH          Heis, Brooke                            1 4  50C 1:37.14                                                                        F             N33
F01OH          OHFH  ACrowley, Maggie                         USA0510200413F  4                                                                              N06
G01OH          Crowley, Maggie                         1 4  50C 2:08.73                                                                        F             N54
E01OH      AOHFH    M 2006 76 1314  007122017 2:03.82S                   1:58.05S     1 3     1 32.                                            1N   0100X    N84
F01OH          OHFH  AGrandy, Jack                            USA0730200313M  1                                                                              N84
G01OH          Grandy, Jack                            1 4  50C   29.81                                                                        F             N03
F01OH          OHFH  ATurner, Daniel                          USA0817200214M  2                                                                              N75
G01OH          Turner, Daniel                          1 4  50C   59.41                                                                        F             N04
F01OH          OHFH  AGrimes, Kyle                            USA1102200313M  3                                                                              N94
G01OH          Grimes, Kyle                            1 4  50C 1:10.99                                                                        F             N43
F01OH          OHFH  ATeter, Jon                              USA0918200313M  4                                                                              N24
G01OH          Teter, Jon                              1 4  50C 1:58.05                                                                        F             N72
E01OH      AOHFH    F 2006 77 1518  007122017 2:07.28S                   2:04.39S     1 4     2 26.                                            2N   0100X    N84
F01OH          OHFH  AHeis, Rachel                            USA1220200016F  1                                                                              N74
G01OH          Heis, Rachel                            1 4  50C   36.33                                                                        F             N03
F01OH          OHFH  AGeis, Erin                              USA0219200215F  2                                                                              N04
G01OH          Geis, Erin                              1 4  50C 1:03.69                                                                        F             N52
F01OH          OHFH  ASchulok, Sydney                         USA0214200017F  3                                                                              N26
G01OH          Schulok, Sydney                         1 4  50C 1:34.16                                                                        F             N74
F01OH          OHFH  AGrimes, Kelly                           USA0130199918F  4                                                                              N45
G01OH          Grimes, Kelly                           1 4  50C 2:04.39                                                                        F             N83
E01OH      AOHFH    M 2006 78 1518  007122017 1:53.45S                   1:44.51S     1 3     2 26.                                            2N   0100X    N94
F01OH          OHFH  ADonnels, Carter                         USA0309200017M  1                                                                              N16
G01OH          Donnels, Carter                         1 4  50C   25.66                                                                        F             N34
F01OH          OHFH  AKimble, Adam                            USA0724200115M  2                                                                              N74
G01OH          Kimble, Adam                            1 4  50C   53.75                                                                        F             N92
F01OH          OHFH  AGrace, Joey                             USA0630199917M  3                                                                              N64
G01OH          Grace, Joey                             1 4  50C 1:20.23                                                                        F             N82
F01OH          OHFH  AButkovich, Patrick                      USA0108199918M  4                                                                              N57
G01OH          Butkovich, Patrick                      1 4  50C 1:44.51                                                                        F             N85
C11OH      OHIH  Indian Hill Club              IH                                                                                          USA               N15
D01OH      Bennet, Duncan                          AUSA11202009 7MM  251 12 UN0807122017   29.51S                     28.61S 0 0 5 3  0 24       2  01      NN29
D3                                                                                                                                                           N67
D01OH      Bennet, Duncan                          AUSA11202009 7MM  252 32 UN0807122017                              44.16S 0 0 2 1  0 31       3  02      NN48
D01OH      Bennet, Duncan                          AUSA11202009 7MM  501 42 UN0807122017 1:04.41S                   1:08.29S 0 0 4 2  0 21       5  01      NN69
D01OH      Bey, Leo                                AUSA01152010 7MM  251 12 UN0807122017   27.54S                     28.21S 0 0 6 4  0 23       5  01      NN96
D3                                                                                                                                                           N67
D01OH      Bey, Leo                                AUSA01152010 7MM  252 32 UN0807122017   28.26S                     32.43S 0 0 8 6  0 15       6  02      NN07
D01OH      Bey, Leo                                AUSA01152010 7MM  253 52 UN0807122017   38.23S                        DQS 0 0 3 1  0  0       0  03      NN76
D01OH      Bey, Sarah                              AUSA0124200710FF  251 13 091007122017   19.31S                     19.73S 0 0 5 2  0 11  2.   4  0100    NN77
D3                                                                                                                                                           N67
D01OH      Bey, Sarah                              AUSA0124200710FF  252 33 091007122017   24.25S                     24.51S 0 0 5 1  0  8  5.   5  0200    NN67
D01OH      Bey, Sarah                              AUSA0124200710FF  501 43 091007122017   45.95S                     45.54S 0 0 3 4  0 17       1  01      NN47
D01OH      Cass, Dylan                             AUSA03082010 7MM  251 12 UN0807122017   36.29S                     35.31S 0 0 3 6  0 42       6  01      NN18
D3                                                                                                                                                           N67
D01OH      Cass, Dylan                             AUSA03082010 7MM  252 32 UN0807122017   51.74S                     54.05S 0 0 3 4  0 39       5  02      NN28
G01OH          Cass, Dylan                             1 2  25C           54.05                                                                F             N72
D01OH      Cass, Dylan                             AUSA03082010 7MM  501 42 UN0807122017                            1:21.72S 0 0 2 6  0 29       4  01      NN67
D01OH      Cass, Garrett                           AUSA06282008 8MM  251 12 UN0807122017   25.65S                     24.07S 0 0 7 4  0 15       3  01      NN09
D3                                                                                                                                                           N67
D01OH      Cass, Garrett                           AUSA06282008 8MM  252 32 UN0807122017   33.19S                     28.95S 0 0 6 2  0  6  9.   1  0200    NN49
D01OH      Cass, Garrett                           AUSA06282008 8MM  501 42 UN0807122017 1:00.60S                     54.60S 0 0 5 1  0 11  2.   4  0100    NN59
D01OH      Diwan, Aiden                            A            9MM  251 14 091007122017                              21.28S 0 0 1 5  0 21       3  01      NN85
D3                                                                                                                                                           N67
D01OH      Diwan, Aiden                            A            9MM  252 34 091007122017                              29.01S 0 0 2 1  0 22       1  02      NN85
G01OH          Diwan, Aiden                            1 4  25C   15.25   26.33           29.01                                                F             N93
D01OH      Diwan, Aiden                            A            9MM  253 54 091007122017                              26.54S 0 0 1 4  0 12  1.   2  0300    NN26
D01OH      Diwan, Henry                            A            8MM  251 12 UN0807122017                              47.60S 0 0 1 1  0 49       2  01      NN36
D3                                                                                                                                                           N67
D01OH      Diwan, Henry                            A            8MM  252 32 UN0807122017                              44.04S 0 0 2 4  0 30       2  02      NN36
D01OH      Diwan, Rowan                            A            8MM  251 12 UN0807122017                              36.79S 0 0 1 5  0 43       1  01      NN46
D3                                                                                                                                                           N67
D01OH      Diwan, Rowan                            A            8MM  253 52 UN0807122017                                 DQS 0 0 1 3  0  0       0  03      NN26
D01OH      Diwan, Tobias                           A           11MM  251 16 111207112017            22.90S                   1 5 0 0 21             01      NN16
D3                                                                                                                                                           N67
D01OH      Diwan, Tobias                           A           11MM  502 36 111207112017          1:02.54S            59.14S 1 6 1 1 11 11  2.   5  0200    NN87
D01OH      Diwan, Tobias                           A           11MM  503 56 111207122017                                 DQS 0 0 1 5  0  0       0  03      NN26
D01OH      Dowling, Joe                            AUSA0301200017MM  501 20 151807112017   26.97S   26.90S            26.25S 4 3 2 1  5  4 11.   4  0100    NN49
D3                                                                                                                                                           N67
D01OH      Dowling, Joe                            AUSA0301200017MM  504 30 151807112017   29.26S   28.90S            28.55S 1 3 2 5  4  3 12.   3  0400    NN59
D01OH      Dowling, Joe                            AUSA0301200017MM  502 40 151807112017   30.05S   29.92S            28.91S 3 3 2 3  1  1 16.   1  0200    NN49
D01OH      Economon, Anna                          AUSA0427200215FF  501 19 151807112017   31.01S   30.90S            29.91S 5 4 2 1  5  2 13.   2  0100    NN10
D3                                                                                                                                                           N67
D01OH      Economon, Anna                          AUSA0427200215FF  502 39 151807112017   35.32S   36.43S            35.33S 3 3 2 1  5  4 11.   4  0200    NN10
D01OH      Economon, Anna                          AUSA0427200215FF  503 59 151807122017   40.58S                     38.46S 0 0 4 5  0  3 12.   3  0300    NN49
D01OH      Economon, Athina                        AUSA1020200412FF  251 15 111207112017   13.57S   13.79S            13.31S 7 3 2 3  1  1 16.   1  0100    NN80
D3                                                                                                                                                           N67
D01OH      Economon, Athina                        AUSA1020200412FF  502 35 111207112017   36.87S   37.36S            35.70S 2 3 2 4  3  2 13.   2  0200    NN80
D01OH      Economon, Athina                        AUSA1020200412FF  501 45 111207112017   30.83S   30.76S            29.46S 8 3 2 2  3  1 16.   1  0100    NN80
D01OH      Eisenhauer, Ellie                       AUSA0723200412FF  251 15 111207112017   13.90S   14.10S            14.04S 5 3 2 2  3  3 12.   3  0100    NN11
D3                                                                                                                                                           N67
D01OH      Eisenhauer, Ellie                       AUSA0723200412FF  501 45 111207112017   32.68S   30.31S            30.35S 6 4 2 3  1  2 13.   2  0100    NN11
D01OH      Eisenhauer, Ellie                       AUSA0723200412FF  503 55 111207122017   43.99S                     42.81S 0 0 6 4  0  3 12.   3  0300    NN50
D01OH      Eisenhauer, Emma                        AUSA0406200611FF  251 15 111207112017   18.46S   16.98S                   3 3 0 0 19             01      NN59
D3                                                                                                                                                           N67
D01OH      Eisenhauer, Emma                        AUSA0406200611FF  501 45 111207112017   41.05S   40.39S                   5 1 0 0 26             01      NN49
D01OH      Eisenhauer, Emma                        AUSA0406200611FF  503 55 111207122017   49.24S                     49.12S 0 0 5 1  0 12  1.   4  0300    NN00
D01OH      Farrell, Finley                         AUSA08272008 8FF  251 11 UN0807122017                              23.10S 0 0 2 1  0  5 10.   1  0100    NN29
D3                                                                                                                                                           N67
D01OH      Farrell, Finley                         AUSA08272008 8FF  254 21 UN0807122017   25.71S                     23.17S 0 0 3 5  0  3 12.   3  0400    NN00
D01OH      Farrell, Finley                         AUSA08272008 8FF  252 31 UN0807122017   25.16S                     27.58S 0 0 8 4  0  4 11.   2  0200    NN10
D01OH      Franz, Novella                          AUSA0515200611FF  251 15 111207112017   17.02S   16.24S                   4 3 0 0 16             01      NN88
D3                                                                                                                                                           N67
D01OH      Franz, Novella                          AUSA0515200611FF  502 35 111207112017   45.48S   45.12S            43.25S 2 5 1 1 12 10  3.   4  0200    NN20
D01OH      Franz, Novella                          AUSA0515200611FF  501 45 111207112017   37.54S   36.49S                   7 6 0 0 16             01      NN98
D01OH      Groh, Ilkin                             AUSA0907200412MM  251 16 111207112017   16.34S   16.92S            17.28S 2 3 2 1  5  5 10.   5  0100    NN09
D3                                                                                                                                                           N67
D01OH      Groh, Ilkin                             AUSA0907200412MM  503 56 111207122017   51.32S                     48.36S 0 0 3 3  0  5 10.   1  0300    NN28
D01OH      Groh, Ilkin                             AUSA0907200412MM 1005 66 111207122017                            1:48.04S 0 0 1 2  0  9  4.   1  0500    NN77
G01OH          Groh, Ilkin                             1 2  50C   55.07 1:48.04                                                                F             N43
D01OH      Groh, Osher                             AUSA10022007 9MM  251 14 091007122017   23.12S                     24.30S 0 0 3 1  0 28       4  01      NN87
D3                                                                                                                                                           N67
D01OH      Groh, Osher                             AUSA10022007 9MM  252 34 091007122017   27.70S                     31.46S 0 0 4 1  0 24       6  02      NN97
G01OH          Groh, Osher                             1 2  25C   27.72   31.46                                                                F             N33
D01OH      Groh, Osher                             AUSA10022007 9MM  253 54 091007122017   34.03S                        DQS 0 0 2 5  0  0       0  03      NN77
D01OH      Groh, Uziah                             AUSA07232009 7MM  251 12 UN0807122017   37.97S                     32.85S 0 0 2 4  0 37       2  01      NN38
D3                                                                                                                                                           N67
D01OH      Groh, Uziah                             AUSA07232009 7MM  252 32 UN0807122017 1:02.60S                     46.44S 0 0 3 1  0 35       4  02      NN48
G01OH          Groh, Uziah                             1 2  25C           46.44                                                                F             N82
D01OH      Groh, Uziah                             AUSA07232009 7MM  501 42 UN0807122017 1:35.40S                   1:23.72S 0 0 2 3  0 30       5  01      NN68
D01OH      Groh, Yaqar                             AUSA01252011 6FF  251 11 UN0807122017   30.08S                     33.89S 0 0 7 4  0 42       6  01      NN18
D3                                                                                                                                                           N67
G01OH          Groh, Yaqar                             1 4  25C                           33.89                                                F             N82
D01OH      Groh, Yaqar                             AUSA01252011 6FF  252 31 UN0807122017   37.85S                     40.75S 0 0 4 6  0 33       4  02      NN28
D01OH      Groh, Yaqar                             AUSA01252011 6FF  501 41 UN0807122017                            1:16.93S 0 0 2 2  0 25       4  01      NN67
D01OH      Hagopian, Mina                          AUSA04242009 8FF  251 11 UN0807122017   25.02S                     24.63S 0 010 3  0 10  3.   2  0100    NN59
D3                                                                                                                                                           N67
D01OH      Hagopian, Mina                          AUSA04242009 8FF  254 21 UN0807122017   33.01S                     32.56S 0 0 2 4  0  9  4.   2  0400    NN49
D01OH      Hagopian, Mina                          AUSA04242009 8FF  253 51 UN0807122017   33.03S                        DQS 0 0 5 6  0  0       0  03      NN09
D01OH      Hardek, Claire                          AUSA0518200413FF  504 27 131407112017   42.21S   44.33S                   3 5 0 0 13             04      NN68
D3                                                                                                                                                           N67
D01OH      Hardek, Claire                          AUSA0518200413FF  502 37 131407112017   43.75S   43.66S            42.51S 2 2 1 1 11 10  3.   4  0200    NN00
D01OH      Hardek, Claire                          AUSA0518200413FF 1001 47 131407112017 1:22.31S 1:23.49S          1:19.58S 3 2 1 3  7  7  7.   1  0100    NN60
G01OH          Hardek, Claire                          1 2  50C   39.30 1:23.49                                                                P             N54
G01OH          Hardek, Claire                          1 2  50C   37.51 1:19.58                                                                F             N44
D01OH      Hardek, Faye                            AUSA0501200314FF  504 27 131407112017   38.19S   37.85S            36.75S 3 2 2 6  6  5 10.   5  0400    NN39
D3                                                                                                                                                           N67
D01OH      Hardek, Faye                            AUSA0501200314FF  502 37 131407112017   40.19S   39.71S            39.27S 3 4 2 6  6  5 10.   5  0200    NN29
D01OH      Hardek, Faye                            AUSA0501200314FF  503 57 131407122017   44.80S                     42.14S 0 0 2 3  0  6  9.   1  0300    NN38
D01OH      Hardek, Mallory                         AUSA05292008 9FF  254 23 091007122017   23.67S                     24.14S 0 0 3 6  0 16       6  04      NN49
D3                                                                                                                                                           N67
D01OH      Hardek, Mallory                         AUSA05292008 9FF  252 33 091007122017   25.91S                     26.36S 0 0 4 5  0 12  1.   4  0200    NN79
G01OH          Hardek, Mallory                         1 2  25C           26.36                                                                F             N34
D01OH      Hardek, Mallory                         AUSA05292008 9FF  253 53 091007122017   25.99S                     25.20S 0 0 3 5  0  9  4.   3  0300    NN79
D01OH      Helms, Aibhilin                         AUSA07302010 6FF  251 11 UN0807122017   36.31S                     34.99S 0 0 4 6  0 46       2  01      NN59
D3                                                                                                                                                           N67
D01OH      Helms, Aibhilin                         AUSA07302010 6FF  252 31 UN0807122017   37.22S                     35.11S 0 0 4 1  0 22       1  02      NN49
D01OH      Helms, Aibhilin                         AUSA07302010 6FF  501 41 UN0807122017                            1:22.95S 0 0 1 4  0 28       3  01      NN09
D01OH      Joseph, Anna                            AUSA1123200610FF  254 23 091007122017   21.73S                     20.40S 0 0 3 4  0  7  7.   1  0400    NN48
D3                                                                                                                                                           N67
D01OH      Joseph, Anna                            AUSA1123200610FF  501 43 091007122017   43.33S                     42.23S 0 0 4 3  0 13       3  01      NN18
D01OH      Joseph, Anna                            AUSA1123200610FF  253 53 091007122017   24.64S                     22.59S 0 0 4 1  0  4 11.   4  0300    NN58
D01OH      Joseph, Jameson                         AUSA08212008 8MM  251 12 UN0807122017   21.28S                     19.68S 0 0 9 5  0  3 12.   3  0100    NN20
D3                                                                                                                                                           N67
D01OH      Joseph, Jameson                         AUSA08212008 8MM  252 32 UN0807122017   24.43S                     25.24S 0 0 8 2  0  2 13.   2  0200    NN10
D01OH      Joseph, Jameson                         AUSA08212008 8MM  501 42 UN0807122017   48.18S                     45.89S 0 0 6 6  0  7  7.   6  0100    NN20
D01OH      Kassar, Camilla                         AUSA01172011 6FF  251 11 UN0807122017   25.67S                     27.14S 0 010 2  0 17       4  01      NN69
D3                                                                                                                                                           N67
D01OH      Kassar, Camilla                         AUSA01172011 6FF  252 31 UN0807122017   32.34S                     30.72S 0 0 7 1  0 12  1.   3  0200    NN89
G01OH          Kassar, Camilla                         1 3  25C                   30.72                                                        F             N24
D01OH      Kassar, Camilla                         AUSA01172011 6FF  253 51 UN0807122017   32.98S                     28.12S 0 0 5 1  0  6  9.   2  0300    NN89
D01OH      Kassar, Claire                          AUSA04292009 8FF  251 11 UN0807122017   21.28S                     21.53S 0 011 3  0  3 12.   2  0100    NN69
D3                                                                                                                                                           N67
D01OH      Kassar, Claire                          AUSA04292009 8FF  501 41 UN0807122017   49.34S                     47.51S 0 0 6 1  0  4 11.   4  0100    NN69
D01OH      Kassar, Claire                          AUSA04292009 8FF  253 51 UN0807122017   27.42S                     26.69S 0 0 6 4  0  3 12.   3  0300    NN69
D01OH      Kennebeck, Maddy                        AUSA0605200214FF  501 17 131407112017   47.00S      NSS                   1 5 0 0  0             01      NN29
D3                                                                                                                                                           N67
D01OH      Kennebeck, Maddy                        AUSA0605200214FF 1001 47 131407112017 1:55.06S      NSS                   1 1 0 0  0             01      NN69
G01OH          Kennebeck, Maddy                        1 2  50C                                                                                P             N04
D01OH      Kennebeck, Maddy                        AUSA0605200214FF  503 57 131407122017   56.97S                        NSS 0 0 1 5  0  0       0  03      NN59
D01OH      Kennebeck, Ryan                         AUSA0224200611MM  251 16 111207112017   18.79S   17.41S            17.64S 4 1 1 3  7  7  7.   1  0100    NN50
D3                                                                                                                                                           N67
D01OH      Kennebeck, Ryan                         AUSA0224200611MM  502 36 111207112017            49.63S               DQS 1 1 2 1  5  0       0  02      NN29
D01OH      Kennebeck, Ryan                         AUSA0224200611MM 1005 66 111207122017 1:47.85S                   1:44.27S 0 0 2 2  0  8  5.   2  0500    NN20
G01OH          Kennebeck, Ryan                         1 2  50C   47.73 1:44.27                                                                F             N05
D01OH      Leder, Abby                             AUSA0426200512FF  251 15 111207112017   17.06S   16.20S                   4 4 0 0 15             01      NN47
D3                                                                                                                                                           N67
D01OH      Leder, Abby                             AUSA0426200512FF  504 25 111207112017            47.26S                   1 3 0 0 18             04      NN76
D01OH      Leder, Abby                             AUSA0426200512FF  502 35 111207112017   46.71S   47.58S                   3 1 0 0 15             02      NN57
D01OH      Leder, Eddie                            AUSA01252011 6MM  251 12 UN0807122017   36.20S                     32.17S 0 0 3 1  0 35       2  01      NN28
D3                                                                                                                                                           N67
D01OH      Leder, Eddie                            AUSA01252011 6MM  252 32 UN0807122017   40.00S                     41.09S 0 0 4 3  0 24       3  02      NN28
D01OH      Leder, Eddie                            AUSA01252011 6MM  501 42 UN0807122017                                 NSS 0 0 1 3  0  0       0  01      NN47
D01OH      Leder, Gus                              AUSA04292009 8MM  251 12 UN0807122017   22.98S                     23.06S 0 0 8 2  0 13       5  01      NN87
D3                                                                                                                                                           N67
D01OH      Leder, Gus                              AUSA04292009 8MM  254 22 UN0807122017   32.34S                     38.65S 0 0 2 3  0 10  3.   3  0400    NN28
D01OH      Leder, Gus                              AUSA04292009 8MM  252 32 UN0807122017   30.07S                     30.21S 0 0 7 5  0 10  3.   2  0200    NN18
D01OH      Leder, Merrie                           AUSA0117200710FF  254 23 091007122017   25.63S                     24.01S 0 0 2 4  0 15       2  04      NN58
D3                                                                                                                                                           N67
D01OH      Leder, Merrie                           AUSA0117200710FF  252 33 091007122017   25.59S                     24.16S 0 0 4 4  0  7  7.   2  0200    NN88
G01OH          Leder, Merrie                           1 2  25C           24.16                                                                F             N43
D01OH      Leder, Merrie                           AUSA0117200710FF  501 43 091007122017   45.42S                     47.63S 0 0 4 6  0 19       6  01      NN68
D01OH      Leder, Will                             AUSA0918200313MM  501 18 131407112017   33.78S   32.68S            33.84S 1 5 1 2  9 12  1.   6  0100    NN19
D3                                                                                                                                                           N67
D01OH      Leder, Will                             AUSA0918200313MM  502 38 131407112017   42.41S   41.21S            42.27S 1 4 2 6  6  6  9.   6  0200    NN09
D01OH      Leder, Will                             AUSA0918200313MM 1001 48 131407112017 1:19.72S 1:21.35S                   3 5 0 0 14             01      NN28
G01OH          Leder, Will                             1 2  50C         1:21.35                                                                P             N03
D01OH      Longbottom, Hayden                      AUSA0817200511MM  251 16 111207112017   18.13S   17.76S            18.20S 2 2 1 5 10 11  2.   5  0100    NN91
D3                                                                                                                                                           N67
D01OH      Longbottom, Hayden                      AUSA0817200511MM  502 36 111207112017   53.67S   50.39S            51.80S 1 4 1 3  7  8  5.   2  0200    NN81
D01OH      Longbottom, Hayden                      AUSA0817200511MM  501 46 111207112017   40.82S   40.64S            40.06S 2 2 1 1 11 10  3.   4  0100    NN81
D01OH      Lount, Matthew                          AUSA0102200611MM  251 16 111207112017   18.44S   19.59S                   2 5 0 0 17             01      NN19
D3                                                                                                                                                           N67
D01OH      Lount, Matthew                          AUSA0102200611MM  502 36 111207112017   50.68S   51.22S            51.04S 1 3 1 4  8  7  7.   1  0200    NN20
D01OH      Lount, Matthew                          AUSA0102200611MM  503 56 111207122017   52.19S                     50.73S 0 0 3 2  0  8  5.   2  0300    NN59
D01OH      Lount, Thomas                           AUSA05202008 9MM  254 24 091007122017   23.57S                     23.69S 0 0 2 3  0 15       2  04      NN88
D3                                                                                                                                                           N67
D01OH      Lount, Thomas                           AUSA05202008 9MM  253 54 091007122017   28.63S                     27.42S 0 0 3 3  0 16       2  03      NN88
D01OH      Lount, Thomas                           AUSA05202008 9MM 1005 64 091007122017 2:06.86S                   1:55.39S 0 0 1 3  0  8  5.   1  0500    NN79
G01OH          Lount, Thomas                           1 2  50C   59.43 1:55.39                                                                F             N54
D01OH      Magenheim, Hailey                       AUSA1203199818FF  501 19 151807112017   31.75S   30.98S            31.82S 4 2 2 6  6  6  9.   6  0100    NN31
D3                                                                                                                                                           N67
D01OH      Magenheim, Hailey                       AUSA1203199818FF 1001 49 151807122017 1:13.06S                   1:11.53S 0 0 3 1  0  6  9.   5  0100    NN90
G01OH          Magenheim, Hailey                       1 2  50C   34.67 1:11.53                                                                F             N65
D01OH      Magenheim, Hailey                       AUSA1203199818FF 1005 69 151807122017                            1:29.57S 0 0 1 4  0 12  1.   2  0500    NN10
G01OH          Magenheim, Hailey                       1 2  50C   40.08 1:29.57                                                                F             N65
D01OH      Mangano, Christian                      AUSA10242007 9MM  251 14 091007122017   21.19S                     20.76S 0 0 4 5  0 19       5  01      NN50
D3                                                                                                                                                           N67
D01OH      Mangano, Christian                      AUSA10242007 9MM  252 34 091007122017   25.63S                     25.45S 0 0 4 3  0 13       2  02      NN50
G01OH          Mangano, Christian                      1 2  25C           25.45                                                                F             N45
D01OH      Mangano, Christian                      AUSA10242007 9MM  253 54 091007122017   29.93S                     27.57S 0 0 3 2  0 17       3  03      NN60
D01OH      Mangano, Gabriella                      AUSA0930200412FF  502 35 111207112017            37.73S            37.58S 1 1 2 2  4  3 12.   3  0200    NN70
D3                                                                                                                                                           N67
D01OH      Mangano, Gabriella                      AUSA0930200412FF  503 55 111207122017   44.49S                     42.90S 0 0 6 2  0  4 11.   4  0300    NN70
D01OH      Mangano, Gabriella                      AUSA0930200412FF 1005 65 111207122017 1:23.43S                   1:23.65S 0 0 3 6  0  5 10.   5  0500    NN21
G01OH          Mangano, Gabriella                      1 2  50C         1:23.65                                                                F             N55
D01OH      Mangano, Gigi                           AUSA01222011 6FF  251 11 UN0807122017   27.78S                     28.22S 0 0 9 6  0 22       4  01      NN78
D3                                                                                                                                                           N67
D01OH      Mangano, Gigi                           AUSA01222011 6FF  252 31 UN0807122017   34.25S                     34.13S 0 0 5 1  0 21       4  02      NN68
D01OH      Mangano, Gigi                           AUSA01222011 6FF  501 41 UN0807122017                            1:04.70S 0 0 1 2  0 17       1  01      NN18
D01OH      Mangano, Luke                           AUSA0328200611MM  504 26 111207112017   35.17S   35.04S            34.76S 2 4 2 2  3  3 12.   3  0400    NN89
D3                                                                                                                                                           N67
D01OH      Mangano, Luke                           AUSA0328200611MM  501 46 111207112017   31.65S   32.56S            32.23S 3 3 2 2  3  3 12.   3  0100    NN79
D01OH      Mangano, Luke                           AUSA0328200611MM 1005 66 111207122017 1:28.63S                   1:25.84S 0 0 3 1  0  5 10.   5  0500    NN59
G01OH          Mangano, Luke                           1 2  50C   39.37 1:25.84                                                                F             N24
D01OH      Manning, Griffin                        AUSA0730200016MM  501 20 151807112017   26.67S   26.87S            25.52S 5 3 2 5  4  1 16.   1  0100    NN01
D3                                                                                                                                                           N67
D01OH      Manning, Griffin                        AUSA0730200016MM  503 60 151807122017   31.30S                     29.82S 0 0 3 3  0  1 16.   1  0300    NN10
D01OH      Manning, Griffin                        AUSA0730200016MM 1005 70 151807122017 1:06.87S                   1:04.49S 0 0 4 4  0  2 13.   2  0500    NN70
G01OH          Manning, Griffin                        1 2  50C   31.02 1:04.49                                                                F             N35
D01OH      Manning, Reagan                         AUSA0214200413FF  501 17 131407112017   31.45S   31.63S            31.53S 3 3 2 5  4  4 11.   4  0100    NN30
D3                                                                                                                                                           N67
D01OH      Manning, Reagan                         AUSA0214200413FF  504 27 131407112017   35.90S   36.80S            35.84S 3 4 2 1  5  4 11.   4  0400    NN40
D01OH      Manning, Reagan                         AUSA0214200413FF  503 57 131407122017   40.57S                     40.48S 0 0 3 4  0  2 13.   2  0300    NN69
D01OH      Miller, Libby                           AUSA0618200115FF  504 29 151807112017   32.63S   31.91S            30.69S 1 3 2 2  3  3 12.   3  0400    NN89
D3                                                                                                                                                           N67
D01OH      Miller, Libby                           AUSA0618200115FF  503 59 151807122017   37.30S                     35.58S 0 0 4 3  0  1 16.   1  0300    NN09
D01OH      Miller, Libby                           AUSA0618200115FF 1005 69 151807122017 1:14.48S                   1:11.81S 0 0 3 2  0  3 12.   3  0500    NN59
G01OH          Miller, Libby                           1 2  50C   34.73 1:11.81                                                                F             N24
D01OH      Miller, Logan                           AUSA1003200610MM  251 14 091007122017   19.18S                     17.73S 0 0 5 1  0  6  9.   2  0100    NN98
D3                                                                                                                                                           N67
D01OH      Miller, Logan                           AUSA1003200610MM  253 54 091007122017   29.04S                     27.26S 0 0 3 4  0 14       1  03      NN68
D01OH      Miller, Logan                           AUSA1003200610MM 1005 64 091007122017 1:54.90S                   1:51.15S 0 0 3 6  0  7  7.   6  0500    NN59
G01OH          Miller, Logan                           1 2  50C   52.72 1:51.15                                                                F             N24
D01OH      Moldrem, Charlotte                      AUSA1115200511FF  251 15 111207112017   18.55S   17.91S                   3 4 0 0 25             01      NN40
D3                                                                                                                                                           N67
D01OH      Moldrem, Charlotte                      AUSA1115200511FF  502 35 111207112017   51.86S   53.99S                   3 6 0 0 20             02      NN40
D01OH      Moldrem, Charlotte                      AUSA1115200511FF  501 45 111207112017   42.57S   41.62S                   4 2 0 0 29             01      NN40
D01OH      Moldrem, Laurel                         AUSA05072008 9FF  251 13 091007122017   21.45S                     20.67S 0 0 4 5  0 15       2  01      NN39
D3                                                                                                                                                           N67
D01OH      Moldrem, Laurel                         AUSA05072008 9FF  254 23 091007122017   25.12S                     24.24S 0 0 2 3  0 17       3  04      NN39
D01OH      Moldrem, Laurel                         AUSA05072008 9FF  252 33 091007122017   25.88S                     25.53S 0 0 4 2  0  9  4.   3  0200    NN79
G01OH          Moldrem, Laurel                         1 2  25C           25.53                                                                F             N34
D01OH      Moldrem, Maddie                         AUSA0926200313FF  502 37 131407112017   49.35S   48.17S                   3 6 0 0 16             02      NN19
D3                                                                                                                                                           N67
D01OH      Moldrem, Maddie                         AUSA0926200313FF 1001 47 131407112017 1:35.93S 1:36.82S                   3 1 0 0 14             01      NN69
G01OH          Moldrem, Maddie                         1 2  50C   45.22 1:36.82                                                                P             N94
D01OH      Moldrem, Maddie                         AUSA0926200313FF 1005 67 131407122017 1:46.47S                   1:41.06S 0 0 1 1  0  9  4.   4  0500    NN10
G01OH          Moldrem, Maddie                         1 2  50C         1:41.06                                                                F             N34
D01OH      Norwell, Owen                           AUSA0727200115MM  502 40 151807112017   34.30S   33.76S            33.33S 3 2 2 6  6  6  9.   6  0200    NN00
D3                                                                                                                                                           N67
D01OH      Norwell, Owen                           AUSA0727200115MM 1001 50 151807122017 1:03.00S                   1:00.26S 0 0 3 1  0  4 11.   4  0100    NN69
G01OH          Norwell, Owen                           1 2  50C   28.26 1:00.26                                                                F             N44
D01OH      Norwell, Owen                           AUSA0727200115MM 1005 70 151807122017 1:12.86S                   1:09.91S 0 0 4 6  0  5 10.   5  0500    NN89
G01OH          Norwell, Owen                           1 2  50C   32.91 1:09.91                                                                F             N44
D01OH      Okum, Anna                              AUSA1217200214FF  501 17 131407112017            31.14S            31.13S 1 1 2 2  3  3 12.   3  0100    NN77
D3                                                                                                                                                           N67
D01OH      Okum, Anna                              AUSA1217200214FF 1001 47 131407112017 1:07.65S 1:09.50S          1:09.10S 2 3 2 2  3  3 12.   3  0100    NN29
G01OH          Okum, Anna                              1 2  50C   33.01 1:09.50                                                                P             N03
G01OH          Okum, Anna                              1 2  50C   32.39 1:09.10                                                                F             N03
D01OH      Okum, Anna                              AUSA1217200214FF 1005 67 131407122017 1:18.32S                   1:18.42S 0 0 2 4  0  3 12.   3  0500    NN38
G01OH          Okum, Anna                              1 2  50C   36.46 1:18.42                                                                F             N03
D01OH      Okum, Sam                               AUSA0307200017MM  504 30 151807112017   30.54S   28.81S            28.64S 2 4 2 4  2  4 11.   4  0400    NN38
D3                                                                                                                                                           N67
D01OH      Okum, Sam                               AUSA0307200017MM 1001 50 151807122017 1:02.13S                     58.92S 0 0 3 2  0  3 12.   3  0100    NN87
G01OH          Okum, Sam                               1 2  50C   27.96   58.92                                                                F             N62
D01OH      Okum, Sam                               AUSA0307200017MM 1005 70 151807122017 1:09.38S                   1:07.85S 0 0 4 2  0  4 11.   4  0500    NN18
G01OH          Okum, Sam                               1 2  50C   31.86 1:07.85                                                                F             N72
D01OH      Pearson, Jake                           AUSA1115199917MM  501 20 151807112017   28.95S   28.43S            28.43S 5 1 1 1 11 11  2.   5  0100    NN00
D3                                                                                                                                                           N67
D01OH      Pearson, Jake                           AUSA1115199917MM  502 40 151807112017   35.95S   36.90S            35.37S 2 2 1 5 10  8  5.   2  0200    NN00
D01OH      Pearson, Jake                           AUSA1115199917MM 1001 50 151807122017 1:05.32S                   1:02.64S 0 0 2 4  0  6  9.   1  0100    NN59
G01OH          Pearson, Jake                           1 2  50C   31.08 1:02.64                                                                F             N14
D01OH      Pearson, Josh                           AUSA0330200215MM  504 30 151807112017   32.69S   31.95S            31.16S 2 2 1 3  7  7  7.   1  0400    NN89
D3                                                                                                                                                           N67
D01OH      Pearson, Josh                           AUSA0330200215MM  503 60 151807122017   37.58S                     37.25S 0 0 3 2  0  5 10.   4  0300    NN19
D01OH      Pearson, Josh                           AUSA0330200215MM 1005 70 151807122017 1:15.31S                   1:12.65S 0 0 3 2  0  8  5.   3  0500    NN59
G01OH          Pearson, Josh                           1 2  50C   34.69 1:12.65                                                                F             N34
D01OH      Perry, Grace                            AUSA10202008 8FF  251 11 UN0807122017   36.64S                     34.26S 0 0 3 3  0 43       1  01      NN58
D3                                                                                                                                                           N67
D01OH      Perry, Grace                            AUSA10202008 8FF  252 31 UN0807122017   38.98S                     34.05S 0 0 3 3  0 20       2  02      NN58
G01OH          Perry, Grace                            1 3  25C                   34.05                                                        F             N13
D01OH      Perry, Grace                            AUSA10202008 8FF  253 51 UN0807122017   37.03S                        DQS 0 0 4 2  0  0       0  03      NN38
D01OH      Recker, Sophia                          AUSA0615200610FF  254 23 091007122017   21.65S                     21.06S 0 0 3 3  0  9  4.   2  0400    NN29
D3                                                                                                                                                           N67
D01OH      Recker, Sophia                          AUSA0615200610FF  501 43 091007122017   41.72S                     41.03S 0 0 5 5  0  9  4.   4  0100    NN29
D01OH      Recker, Sophia                          AUSA0615200610FF 1005 63 091007122017 1:52.46S                        DQS 0 0 2 4  0  0       0  05      NN19
G01OH          Recker, Sophia                          1 2  50C   50.88 1:45.68                                                                F             N64
D01OH      Replogle, Abby                          AUSA0511200611FF  251 15 111207112017   16.44S   15.88S            15.93S 7 1 1 5 10  9  4.   3  0100    NN10
D3                                                                                                                                                           N67
D01OH      Replogle, Abby                          AUSA0511200611FF  503 55 111207122017   48.95S                     46.11S 0 0 5 5  0  5 10.   1  0300    NN39
D01OH      Replogle, Abby                          AUSA0511200611FF 1005 65 111207122017 1:36.11S                   1:31.89S 0 0 2 6  0 11  2.   5  0500    NN89
G01OH          Replogle, Abby                          1 2  50C   43.08 1:31.89                                                                F             N64
D01OH      Replogle, CJ J                          AUSA1112200313MM  501 18 131407112017   30.93S   30.80S            30.96S 1 4 2 6  6  6  9.   6  0100    NN39
D3                                                                                                                                                           N67
D01OH      Replogle, CJ J                          AUSA1112200313MM  504 28 131407112017   34.95S   34.26S            32.99S 1 2 2 5  4  3 12.   3  0400    NN59
D01OH      Replogle, CJ J                          AUSA1112200313MM  503 58 131407122017   46.09S                     44.16S 0 0 2 4  0  9  4.   3  0300    NN68
D01OH      Ringer, Nick                            AUSA0721200214MM  501 18 131407112017   30.07S   29.94S            29.96S 1 3 2 1  5  5 10.   5  0100    NN49
D3                                                                                                                                                           N67
D01OH      Ringer, Nick                            AUSA0721200214MM  504 28 131407112017   34.09S   32.63S            33.10S 1 4 2 2  3  4 11.   4  0400    NN39
D01OH      Ringer, Nick                            AUSA0721200214MM  502 38 131407112017   37.17S   38.36S            37.25S 1 3 2 5  4  3 12.   3  0200    NN49
D01OH      Robbins, Jack                           AUSA09182007 9MM  251 14 091007122017   22.94S                     21.93S 0 0 3 5  0 23       3  01      NN68
D3                                                                                                                                                           N67
D01OH      Robbins, Jack                           AUSA09182007 9MM  252 34 091007122017   29.30S                     27.02S 0 0 3 1  0 18       1  02      NN58
D01OH      Robbins, Jack                           AUSA09182007 9MM  501 44 091007122017   51.70S                     55.21S 0 0 3 6  0 24       6  01      NN68
D01OH      Robbins, Katie                          AUSA06012010 7FF  251 11 UN0807122017   32.77S                     33.35S 0 0 5 3  0 41       4  01      NN29
D3                                                                                                                                                           N67
D01OH      Robbins, Katie                          AUSA06012010 7FF  252 31 UN0807122017   36.14S                     37.98S 0 0 4 5  0 29       3  02      NN39
D01OH      Robbins, Katie                          AUSA06012010 7FF  501 41 UN0807122017 1:21.98S                   1:20.24S 0 0 3 5  0 26       3  01      NN69
D01OH      Rodenberg, Asher                        AUSA02142009 8MM  251 12 UN0807122017   41.30S                     39.34S 0 0 2 6  0 45       4  01      NN00
D3                                                                                                                                                           N67
D01OH      Rodenberg, Asher                        AUSA02142009 8MM  252 32 UN0807122017   55.38S                     40.10S 0 0 3 2  0 22       2  02      NN00
G01OH          Rodenberg, Asher                        1 2  25C   20.30   40.10                                                                F             N05
D01OH      Schecter, Alexander                     AUSA07152011 5MM  251 12 UN0807122017   29.68S                     29.15S 0 0 5 2  0 26       3  01      NN21
D3                                                                                                                                                           N67
D01OH      Schecter, Alexander                     AUSA07152011 5MM  252 32 UN0807122017   47.38S                     42.78S 0 0 4 1  0 28       4  02      NN31
D01OH      Schecter, Alexander                     AUSA07152011 5MM  501 42 UN0807122017                            1:08.87S 0 0 1 2  0 23       1  01      NN60
D01OH      Schecter, William                       AUSA08072007 9MM  501 44 091007122017   46.91S                     49.07S 0 0 3 4  0 20       5  01      NN20
D3                                                                                                                                                           N67
D01OH      Schecter, William                       AUSA08072007 9MM  253 54 091007122017   26.17S                     25.01S 0 0 4 2  0  8  5.   3  0300    NN50
D01OH      Schecter, William                       AUSA08072007 9MM 1005 64 091007122017 2:04.97S                   2:07.71S 0 0 2 1  0 11  2.   4  0500    NN11
G01OH          Schecter, William                       1 2  50C 1:07.20 2:07.71                                                                F             N06
D01OH      Seinen, Ava                             AUSA1114200610FF  251 13 091007122017   26.49S                     28.30S 0 0 1 4  0 32       3  01      NN77
D3                                                                                                                                                           N67
D01OH      Seinen, Ava                             AUSA1114200610FF  252 33 091007122017   31.77S                     34.08S 0 0 2 3  0 24       5  02      NN77
G01OH          Seinen, Ava                             1 2  25C   21.35   34.08                                                                F             N13
D01OH      Seinen, Ava                             AUSA1114200610FF  253 53 091007122017   43.53S                     42.89S 0 0 1 2  0 21       4  03      NN87
D01OH      Seinen, Drew                            AUSA08142008 8MM  251 12 UN0807122017   25.81S                     30.42S 0 0 7 5  0 32       6  01      NN68
D3                                                                                                                                                           N67
D01OH      Seinen, Drew                            AUSA08142008 8MM  252 32 UN0807122017   32.86S                     31.85S 0 0 6 3  0 12  1.   3  0200    NN09
D01OH      Seinen, Drew                            AUSA08142008 8MM  501 42 UN0807122017 1:03.09S                   1:04.02S 0 0 4 4  0 17       3  01      NN09
D01OH      Setzekorn, Jackson                      AUSA02022012 5MM  251 12 UN0807122017   38.76S                     40.54S 0 0 2 2  0 46       5  01      NN01
D3                                                                                                                                                           N67
D01OH      Setzekorn, Kathryn                      AUSA03232010 7FF  251 11 UN0807122017   29.90S                     30.79S 0 0 7 3  0 32       3  01      NN11
D3                                                                                                                                                           N67
G01OH          Setzekorn, Kathryn                      1 4  25C                           30.79                                                F             N85
D01OH      Setzekorn, Kathryn                      AUSA03232010 7FF  501 41 UN0807122017 1:17.42S                   1:10.38S 0 0 3 2  0 24       2  01      NN51
D01OH      Setzekorn, Kathryn                      AUSA03232010 7FF  253 51 UN0807122017   46.91S                        DQS 0 0 3 6  0  0       0  03      NN01
D01OH      Sharif, Amaya                           AUSA01152011 6FF  501 41 UN0807122017                            1:04.52S 0 0 2 5  0 16       3  01      NN18
D3                                                                                                                                                           N67
D01OH      Sharif, Amaya                           AUSA01152011 6FF  251 11 UN0807122017   32.75S                     28.58S 0 0 6 6  0 23       2  01      NN88
D01OH      Sharif, Amaya                           AUSA01152011 6FF  252 31 UN0807122017   34.74S                        DQS 0 0 4 4  0  0       0  02      NN68
D01OH      Sharif, Xavier                          AUSA01012009 8MM  251 12 UN0807122017   29.38S                     24.24S 0 0 6 1  0 16       1  01      NN39
D3                                                                                                                                                           N67
D01OH      Sharif, Xavier                          AUSA01012009 8MM  254 22 UN0807122017   36.31S                        DQS 0 0 2 5  0  0       0  04      NN29
D01OH      Sharif, Xavier                          AUSA01012009 8MM  252 32 UN0807122017   35.21S                     30.07S 0 0 6 6  0  9  4.   2  0200    NN69
D01OH      Starkey, Ben                            AUSA0310200314MM  502 38 131407112017   35.15S   33.69S            33.70S 3 3 2 3  1  1 16.   1  0200    NN49
D3                                                                                                                                                           N67
D01OH      Starkey, Ben                            AUSA0310200314MM 1001 48 131407112017 1:09.54S 1:10.46S          1:09.36S 3 4 2 1  5  5 10.   5  0100    NN20
G01OH          Starkey, Ben                            1 2  50C   33.75 1:10.46                                                                P             N93
G01OH          Starkey, Ben                            1 2  50C   33.42 1:09.36                                                                F             N93
D01OH      Starkey, Ben                            AUSA0310200314MM 1005 68 131407122017 1:20.44S                   1:20.13S 0 0 2 1  0  4 11.   4  0500    NN19
G01OH          Starkey, Ben                            1 2  50C   36.45 1:20.13                                                                F             N83
D01OH      Starkey, James                          AUSA0114200116MM  502 40 151807112017   36.38S   36.78S            38.93S 1 2 1 2  9 12  1.   6  0200    NN40
D3                                                                                                                                                           N67
D01OH      Starkey, James                          AUSA0114200116MM  503 60 151807122017   41.66S                     42.31S 0 0 3 6  0 14       6  03      NN19
D01OH      Starkey, James                          AUSA0114200116MM 1005 70 151807122017 1:21.39S                        DQS 0 0 2 4  0  0       0  05      NN39
G01OH          Starkey, James                          1 2  50C   39.82 1:25.51                                                                F             N74
D01OH      Starkey, Scarlett                       AUSA1219200610FF  251 13 091007122017   19.26S                     18.79S 0 0 5 4  0 10  3.   3  0100    NN70
D3                                                                                                                                                           N67
D01OH      Starkey, Scarlett                       AUSA1219200610FF  252 33 091007122017   21.30S                     20.06S 0 0 5 4  0  1 16.   1  0200    NN60
D01OH      Starkey, Scarlett                       AUSA1219200610FF  501 43 091007122017   42.53S                     43.93S 0 0 5 1  0 16       6  01      NN40
D01OH      Steltenpohl, Lauren                     AUSA1209200511FF  251 15 111207112017   18.00S   18.00S                   4 6 0 0 26             01      NN80
D3                                                                                                                                                           N67
D01OH      Steltenpohl, Lauren                     AUSA1209200511FF  504 25 111207112017   45.65S   47.04S                   4 1 0 0 16             04      NN90
D01OH      Steltenpohl, Lauren                     AUSA1209200511FF  501 45 111207112017   42.04S   43.08S                   4 3 0 0 31             01      NN90
D01OH      Steltenpohl, Luke                       AUSA11132007 9MM  251 14 091007122017   28.31S                        NSS 0 0 1 3  0  0       0  01      NN20
D3                                                                                                                                                           N67
D01OH      Steltenpohl, Luke                       AUSA11132007 9MM  252 34 091007122017   36.27S                        NSS 0 0 2 5  0  0       0  02      NN20
G01OH          Steltenpohl, Luke                       1 4  25C                                                                                F             N84
D01OH      Steltenpohl, Luke                       AUSA11132007 9MM  253 54 091007122017   33.78S                        NSS 0 0 2 2  0  0       0  03      NN30
D01OH      Steltonpohl, Lindsey                    AUSA02142012 5FF  251 11 UN0807122017   52.01S                        NSS 0 0 2 4  0  0       0  01      NN61
D3                                                                                                                                                           N67
D01OH      Taylor, Miles                           AUSA1202200511MM  251 16 111207112017   19.02S   18.61S                   2 1 0 0 13             01      NN58
D3                                                                                                                                                           N67
D01OH      Taylor, Miles                           AUSA1202200511MM  502 36 111207112017   52.84S   53.22S            53.17S 2 4 1 5 10  9  4.   3  0200    NN99
D01OH      Taylor, Miles                           AUSA1202200511MM  501 46 111207112017   46.24S   43.08S            44.39S 2 6 1 6 12 12  1.   6  0100    NN00
D01OH      Tramontana, Max                         AUSA0815200313MM  501 18 131407112017   38.65S   37.77S                   3 6 0 0 17             01      NN49
D3                                                                                                                                                           N67
D01OH      Tramontana, Max                         AUSA0815200313MM  502 38 131407112017   50.61S   49.60S                   1 1 0 0 14             02      NN39
D01OH      Tramontana, Max                         AUSA0815200313MM  503 58 131407122017 1:02.17S                        NSS 0 0 1 4  0  0       0  03      NN69
D01OH      Tramontana, Mia                         AUSA1002200115FF  501 19 151807112017   38.63S   38.54S                   2 4 0 0 20             01      NN29
D3                                                                                                                                                           N67
D01OH      Tramontana, Mia                         AUSA1002200115FF  502 39 151807112017   49.03S   47.64S                   2 6 0 0 19             02      NN29
D01OH      Tramontana, Mia                         AUSA1002200115FF  503 59 151807122017   49.34S                     49.09S 0 0 2 3  0 14       2  03      NN49
D01OH      Wiley, Cole                             AUSA0629200610MM  251 14 091007122017   21.51S                     20.20S 0 0 4 1  0 18       4  01      NN97
D3                                                                                                                                                           N67
D01OH      Wiley, Cole                             AUSA0629200610MM  252 34 091007122017   27.50S                     28.41S 0 0 4 5  0 21       5  02      NN08
G01OH          Wiley, Cole                             1 2  25C           28.41                                                                F             N82
D01OH      Wiley, Cole                             AUSA0629200610MM  501 44 091007122017   49.71S                     48.89S 0 0 3 5  0 19       4  01      NN18
D01OH      Wiley, Emma                             AUSA0629200610FF  254 23 091007122017   26.39S                     25.59S 0 0 2 1  0 19       5  04      NN97
D3                                                                                                                                                           N67
D01OH      Wiley, Emma                             AUSA0629200610FF  501 43 091007122017   45.65S                     46.67S 0 0 3 3  0 18       2  01      NN97
D01OH      Wiley, Emma                             AUSA0629200610FF  253 53 091007122017   28.11S                     29.50S 0 0 2 3  0 13       1  03      NN87
D01OH      Wilson, AJ J                            AUSA11012009 7MM  251 12 UN0807122017   30.57S                     32.82S 0 0 5 5  0 36       5  01      NN97
D3                                                                                                                                                           N67
D01OH      Wilson, AJ J                            AUSA11012009 7MM  254 22 UN0807122017   42.91S                     46.29S 0 0 2 6  0 12  1.   4  0400    NN28
D01OH      Wilson, AJ J                            AUSA11012009 7MM  253 52 UN0807122017                                 DQS 0 0 1 5  0  0       0  03      NN96
D01OH      Zahn, Freddie                           AUSA0218200116MM  501 20 151807112017   32.00S   31.92S                   3 6 0 0 21             01      NN38
D3                                                                                                                                                           N67
D01OH      Zahn, Freddie                           AUSA0218200116MM  502 40 151807112017   40.22S   40.42S                   2 1 0 0 15             02      NN38
D01OH      Zahn, Freddie                           AUSA0218200116MM  503 60 151807122017   43.11S                     41.05S 0 0 2 1  0 11  2.   4  0300    NN88
D01OH      Zahn, Martin                            AUSA08302008 8MM  252 32 UN0807122017   34.92S                     51.79S 0 0 6 1  0 37       6  02      NN78
D3                                                                                                                                                           N67
D01OH      Zahn, Martin                            AUSA08302008 8MM  501 42 UN0807122017 1:16.21S                   1:07.72S 0 0 3 1  0 20       2  01      NN09
D01OH      Zahn, Martin                            AUSA08302008 8MM  253 52 UN0807122017   39.99S                     37.19S 0 0 2 3  0 11  2.   1  0300    NN19
D01OH      Zahn, Warren                            AUSA0824200313MM 1001 48 131407112017          1:17.23S          1:16.77S 2 1 1 1 11 10  3.   4  0100    NN39
D3                                                                                                                                                           N67
G01OH          Zahn, Warren                            1 2  50C   37.46 1:17.23                                                                P             N04
G01OH          Zahn, Warren                            1 2  50C   36.55 1:16.77                                                                F             N04
D01OH      Zahn, Warren                            AUSA0824200313MM  503 58 131407122017   52.14S                     49.87S 0 0 2 6  0 11  2.   5  0300    NN88
D01OH      Zahn, Warren                            AUSA0824200313MM 1005 68 131407122017 1:41.36S                        DQS 0 0 1 4  0  0       0  05      NN58
G01OH          Zahn, Warren                            1 2  50C   44.32 1:32.38                                                                F             N93
E01OH      AOHIH    F 1007  1 UN08  007122017 2:03.43S                   1:45.01S     1 1     3 24.                                            3N   0500X    N94
F01OH          OHIH  AFarrell, Finley                         USA082720088 F  1                                                                              N06
G01OH          Farrell, Finley                         1 4  25C                                                                                F             N83
F01OH          OHIH  AKassar, Camilla                         USA011720116 F  2                                                                              N85
G01OH          Kassar, Camilla                         1 4  25C   59.54                                                                        F             N24
F01OH          OHIH  AKassar, Claire                          USA042920098 F  3                                                                              N65
G01OH          Kassar, Claire                          1 4  25C                                                                                F             N33
F01OH          OHIH  AHagopian, Mina                          USA042420098 F  4                                                                              N55
G01OH          Hagopian, Mina                          1 4  25C 1:45.01                                                                        F             N04
E01OH      AOHIH    M 1007  2 UN08  007122017 2:08.32S                        DQS     1 5     0                                                0N   05  X    N34
F01OH          OHIH  AJoseph, Jameson                         USA082120088 M  1                                                                              N16
G01OH          Joseph, Jameson                         1 4  25C                                                                                F             N83
F01OH          OHIH  ABey, Leo                                USA011520107 M  2                                                                              N23
G01OH          Bey, Leo                                1 4  25C 1:15.21                                                                        F             N71
F01OH          OHIH  ACass, Garrett                           USA062820088 M  3                                                                              N35
G01OH          Cass, Garrett                           1 4  25C                                                                                F             N13
F01OH          OHIH  ALeder, Gus                              USA042920098 M  4                                                                              N14
G01OH          Leder, Gus                              1 4  25C 2:01.92                                                                        F             N62
E01OH      AOHIH    F 1007  3 0910  007122017 1:29.46S                   1:26.85S     1 1     5 20.                                            5N   0500X    N74
F01OH          OHIH  AStarkey, Scarlett                       USA1219200610F  1                                                                              N07
G01OH          Starkey, Scarlett                       1 4  25C                                                                                F             N84
F01OH          OHIH  AJoseph, Anna                            USA1123200610F  2                                                                              N84
G01OH          Joseph, Anna                            1 4  25C   45.56                                                                        F             N13
F01OH          OHIH  ARecker, Sophia                          USA0615200610F  3                                                                              N65
G01OH          Recker, Sophia                          1 4  25C                                                                                F             N43
F01OH          OHIH  ABey, Sarah                              USA0124200710F  4                                                                              N04
G01OH          Bey, Sarah                              1 4  25C 1:26.85                                                                        F             N52
E01OH      AOHIH    M 1007  4 0910  007122017 1:40.44S                   1:37.01S     1 6     5 20.                                            5N   0500X    N74
F01OH          OHIH  AMangano, Christian                      USA102420079 M  1                                                                              N17
G01OH          Mangano, Christian                      1 4  25C                                                                                F             N94
F01OH          OHIH  ASchecter, William                       USA080720079 M  2                                                                              N86
G01OH          Schecter, William                       1 4  25C   53.42                                                                        F             N15
F01OH          OHIH  ALount, Thomas                           USA052020089 M  3                                                                              N45
G01OH          Lount, Thomas                           1 4  25C                                                                                F             N23
F01OH          OHIH  AMiller, Logan                           USA1003200610M  4                                                                              N25
G01OH          Miller, Logan                           1 4  25C 1:37.01                                                                        F             N73
E01OH      AOHIH    F 2007  5 1112  007122017 2:38.45S                   2:25.55S     1 5     2 26.                                            2N   0500X    N74
F01OH          OHIH  AMangano, Gabriella                      USA0930200412F  1                                                                              N07
G01OH          Mangano, Gabriella                      1 4  50C   36.80                                                                        F             N25
F01OH          OHIH  AEisenhauer, Ellie                       USA0723200412F  2                                                                              N76
G01OH          Eisenhauer, Ellie                       1 4  50C 1:18.38                                                                        F             N25
F01OH          OHIH  AEconomon, Athina                        USA1020200412F  3                                                                              N46
G01OH          Economon, Athina                        1 4  50C 1:51.35                                                                        F             N94
F01OH          OHIH  AReplogle, Abby                          USA0511200611F  4                                                                              N65
G01OH          Replogle, Abby                          1 4  50C 2:25.55                                                                        F             N14
E01OH      AOHIH    M 2007  6 1112  007122017 2:55.33S                   2:50.76S     1 2     3 24.                                            3N   0500X    N74
F01OH          OHIH  ALount, Matthew                          USA0102200611M  1                                                                              N85
G01OH          Lount, Matthew                          1 4  50C   49.00                                                                        F             N14
F01OH          OHIH  AGroh, Ilkin                             USA0907200412M  2                                                                              N54
G01OH          Groh, Ilkin                             1 4  50C 1:36.79                                                                        F             N03
F01OH          OHIH  AMangano, Luke                           USA0328200611M  3                                                                              N35
G01OH          Mangano, Luke                           1 4  50C 1:47.42                                                                        F             N73
F01OH          OHIH  ALongbottom, Hayden                      USA0817200511M  4                                                                              N47
G01OH          Longbottom, Hayden                      1 4  50C 2:50.76                                                                        F             N85
E01OH      AOHIH    F 2007  7 1314  007122017 2:34.63S                   2:28.15S     1 5     3 24.                                            3N   0500X    N74
F01OH          OHIH  AHardek, Faye                            USA0501200314F  1                                                                              N74
G01OH          Hardek, Faye                            1 4  50C   39.29                                                                        F             N03
F01OH          OHIH  AManning, Reagan                         USA0214200413F  2                                                                              N95
G01OH          Manning, Reagan                         1 4  50C 1:19.37                                                                        F             N44
F01OH          OHIH  AOkum, Anna                              USA1217200214F  3                                                                              N14
G01OH          Okum, Anna                              1 4  50C 1:53.89                                                                        F             N62
F01OH          OHIH  AHardek, Claire                          USA0518200413F  4                                                                              N55
G01OH          Hardek, Claire                          1 4  50C 2:28.15                                                                        F             N93
E01OH      AOHIH    M 2007  8 1314  007122017 2:35.50S                   2:20.43S     1 2     3 24.                                            3N   0500X    N74
F01OH          OHIH  AStarkey, Ben                            USA0310200314M  1                                                                              N94
G01OH          Starkey, Ben                            1 4  50C   34.03                                                                        F             N13
F01OH          OHIH  AReplogle, CJ                            USA1112200313M  2                                                                              N74
G01OH          Replogle, CJ                            1 4  50C 1:17.57                                                                        F             N13
F01OH          OHIH  ARinger, Nick                            USA0721200214M  3                                                                              N94
G01OH          Ringer, Nick                            1 4  50C 1:48.78                                                                        F             N43
F01OH          OHIH  ALeder, Will                             USA0918200313M  4                                                                              N54
G01OH          Leder, Will                             1 4  50C 2:20.43                                                                        F             N92
E01OH      AOHIH    F 2007  9 1518  007122017 2:59.51S                   2:25.05S     1 6     4 22.                                            4N   0500X    N84
F01OH          OHIH  AEconomon, Anna                          USA0427200215F  1                                                                              N65
G01OH          Economon, Anna                          1 4  50C   40.45                                                                        F             N83
F01OH          OHIH  AMiller, Libby                           USA0618200115F  2                                                                              N35
G01OH          Miller, Libby                           1 4  50C 1:10.28                                                                        F             N73
F01OH          OHIH  AMagenheim, Hailey                       USA1203199818F  3                                                                              N86
G01OH          Magenheim, Hailey                       1 4  50C 1:45.85                                                                        F             N25
F01OH          OHIH  ATramontana, Mia                         USA1002200115F  4                                                                              N06
G01OH          Tramontana, Mia                         1 4  50C 2:25.05                                                                        F             N54
E01OH      AOHIH    M 2007 10 1518  007122017 2:04.14S                   1:52.82S     1 4     1 32.                                            1N   0500X    N84
F01OH          OHIH  ADowling, Joe                            USA0301200017M  1                                                                              N94
G01OH          Dowling, Joe                            1 4  50C   28.72                                                                        F             N13
F01OH          OHIH  AManning, Griffin                        USA0730200016M  2                                                                              N46
G01OH          Manning, Griffin                        1 4  50C   57.61                                                                        F             N64
F01OH          OHIH  AOkum, Sam                               USA0307200017M  3                                                                              N83
G01OH          Okum, Sam                               1 4  50C 1:25.84                                                                        F             N22
F01OH          OHIH  ANorwell, Owen                           USA0727200115M  4                                                                              N55
G01OH          Norwell, Owen                           1 4  50C 1:52.82                                                                        F             N93
E01OH      AOHIH    F 1006 61 UN08  007122017 1:46.35S                   1:36.26S     1 1     2 26.                                            2N   0100X    N15
F01OH          OHIH  AKassar, Claire                          USA042920098 F  1                                                                              N55
G01OH          Kassar, Claire                          1 4  25C                                                                                F             N33
F01OH          OHIH  AKassar, Camilla                         USA011720116 F  2                                                                              N85
G01OH          Kassar, Camilla                         1 4  25C   51.48                                                                        F             N24
F01OH          OHIH  AFarrell, Finley                         USA082720088 F  3                                                                              N06
G01OH          Farrell, Finley                         1 4  25C                                                                                F             N83
F01OH          OHIH  AHagopian, Mina                          USA042420098 F  4                                                                              N55
G01OH          Hagopian, Mina                          1 4  25C 1:36.26                                                                        F             N04
E01OH      AOHIH    M 1006 62 UN08  007122017 1:46.86S                   1:35.20S     1 5     3 24.                                            3N   0100X    N25
F01OH          OHIH  ALeder, Gus                              USA042920098 M  1                                                                              N14
G01OH          Leder, Gus                              1 4  25C                                                                                F             N81
F01OH          OHIH  ACass, Garrett                           USA062820088 M  2                                                                              N35
G01OH          Cass, Garrett                           1 4  25C   48.72                                                                        F             N63
F01OH          OHIH  ASeinen, Drew                            USA081420088 M  3                                                                              N94
G01OH          Seinen, Drew                            1 4  25C                                                                                F             N72
F01OH          OHIH  AJoseph, Jameson                         USA082120088 M  4                                                                              N16
G01OH          Joseph, Jameson                         1 4  25C 1:35.20                                                                        F             N54
E01OH      AOHIH    F 1006 71 0910  007122017 1:19.21S                   1:15.62S     1 1     4 22.                                            4N   0100X    N74
F01OH          OHIH  ARecker, Sophia                          USA0615200610F  1                                                                              N65
G01OH          Recker, Sophia                          1 4  25C                                                                                F             N43
F01OH          OHIH  AStarkey, Scarlett                       USA1219200610F  2                                                                              N07
G01OH          Starkey, Scarlett                       1 4  25C   37.93                                                                        F             N35
F01OH          OHIH  ABey, Sarah                              USA0124200710F  3                                                                              N04
G01OH          Bey, Sarah                              1 4  25C                                                                                F             N81
F01OH          OHIH  AJoseph, Anna                            USA1123200610F  4                                                                              N84
G01OH          Joseph, Anna                            1 4  25C 1:15.62                                                                        F             N33
E01OH      AOHIH    M 1006 72 0910  007122017 1:26.57S                   1:22.51S     1 6     6 18.                                            6N   0100X    N94
F01OH          OHIH  AMiller, Logan                           USA1003200610M  1                                                                              N25
G01OH          Miller, Logan                           1 4  25C                                                                                F             N03
F01OH          OHIH  ADiwan, Aiden                                       9 M  2                                                                              N23
G01OH          Diwan, Aiden                            1 4  25C   42.99                                                                        F             N03
F01OH          OHIH  ASchecter, William                       USA080720079 M  3                                                                              N86
G01OH          Schecter, William                       1 4  25C                                                                                F             N64
F01OH          OHIH  AMangano, Christian                      USA102420079 M  4                                                                              N27
G01OH          Mangano, Christian                      1 4  25C 1:22.51                                                                        F             N65
E01OH      AOHIH    F 2006 73 1112  007122017 2:18.55S                   2:08.04S     1 5     3 24.                                            3N   0100X    N84
F01OH          OHIH  AEisenhauer, Ellie                       USA0723200412F  1                                                                              N76
G01OH          Eisenhauer, Ellie                       1 4  50C   31.09                                                                        F             N05
F01OH          OHIH  AMangano, Gabriella                      USA0930200412F  2                                                                              N07
G01OH          Mangano, Gabriella                      1 4  50C 1:03.39                                                                        F             N55
F01OH          OHIH  AReplogle, Abby                          USA0511200611F  3                                                                              N65
G01OH          Replogle, Abby                          1 4  50C 1:38.39                                                                        F             N14
F01OH          OHIH  AEconomon, Athina                        USA1020200412F  4                                                                              N46
G01OH          Economon, Athina                        1 4  50C 2:08.04                                                                        F             N94
E01OH      AOHIH    M 2006 74 1112  007122017 2:39.26S                   2:34.32S     1 5     3 24.                                            3N   0100X    N84
F01OH          OHIH  AMangano, Luke                           USA0328200611M  1                                                                              N35
G01OH          Mangano, Luke                           1 4  50C   32.56                                                                        F             N53
F01OH          OHIH  AKennebeck, Ryan                         USA0224200611M  2                                                                              N06
G01OH          Kennebeck, Ryan                         1 4  50C 1:14.16                                                                        F             N44
F01OH          OHIH  ALongbottom, Hayden                      USA0817200511M  3                                                                              N47
G01OH          Longbottom, Hayden                      1 4  50C 1:59.21                                                                        F             N85
F01OH          OHIH  AGroh, Ilkin                             USA0907200412M  4                                                                              N64
G01OH          Groh, Ilkin                             1 4  50C 2:34.32                                                                        F             N92
E01OH      AOHIH    F 2006 75 1314  007122017 2:16.03S                   2:10.22S     1 2     3 24.                                            3N   0100X    N74
F01OH          OHIH  AManning, Reagan                         USA0214200413F  1                                                                              N95
G01OH          Manning, Reagan                         1 4  50C   31.55                                                                        F             N14
F01OH          OHIH  AHardek, Faye                            USA0501200314F  2                                                                              N74
G01OH          Hardek, Faye                            1 4  50C 1:04.39                                                                        F             N23
F01OH          OHIH  AHardek, Claire                          USA0518200413F  3                                                                              N55
G01OH          Hardek, Claire                          1 4  50C 1:39.54                                                                        F             N04
F01OH          OHIH  AOkum, Anna                              USA1217200214F  4                                                                              N14
G01OH          Okum, Anna                              1 4  50C 2:10.22                                                                        F             N52
E01OH      AOHIH    M 2006 76 1314  007122017 2:14.18S                   2:04.44S     1 2     3 24.                                            3N   0100X    N84
F01OH          OHIH  AReplogle, CJ                            USA1112200313M  1                                                                              N64
G01OH          Replogle, CJ                            1 4  50C   31.49                                                                        F             N92
F01OH          OHIH  ALeder, Will                             USA0918200313M  2                                                                              N54
G01OH          Leder, Will                             1 4  50C 1:04.28                                                                        F             N92
F01OH          OHIH  AStarkey, Ben                            USA0310200314M  3                                                                              N94
G01OH          Starkey, Ben                            1 4  50C 1:35.12                                                                        F             N43
F01OH          OHIH  ARinger, Nick                            USA0721200214M  4                                                                              N94
G01OH          Ringer, Nick                            1 4  50C 2:04.44                                                                        F             N33
E01OH      AOHIH    F 2006 77 1518  007122017 2:12.63S                   2:10.01S     1 2     4 22.                                            4N   0100X    N74
F01OH          OHIH  AEconomon, Anna                          USA0427200215F  1                                                                              N65
G01OH          Economon, Anna                          1 4  50C   30.65                                                                        F             N83
F01OH          OHIH  AMagenheim, Hailey                       USA1203199818F  2                                                                              N86
G01OH          Magenheim, Hailey                       1 4  50C 1:03.21                                                                        F             N15
F01OH          OHIH  ATramontana, Mia                         USA1002200115F  3                                                                              N06
G01OH          Tramontana, Mia                         1 4  50C 1:40.75                                                                        F             N54
F01OH          OHIH  AMiller, Libby                           USA0618200115F  4                                                                              N35
G01OH          Miller, Libby                           1 4  50C 2:10.01                                                                        F             N63
E01OH      AOHIH    M 2006 78 1518  007122017 1:54.84S                   1:44.43S     1 4     1 32.                                            1N   0100X    N94
F01OH          OHIH  AOkum, Sam                               USA0307200017M  1                                                                              N83
G01OH          Okum, Sam                               1 4  50C   27.04                                                                        F             N02
F01OH          OHIH  ANorwell, Owen                           USA0727200115M  2                                                                              N55
G01OH          Norwell, Owen                           1 4  50C   53.84                                                                        F             N73
F01OH          OHIH  ADowling, Joe                            USA0301200017M  3                                                                              N94
G01OH          Dowling, Joe                            1 4  50C 1:19.21                                                                        F             N33
F01OH          OHIH  AManning, Griffin                        USA0730200016M  4                                                                              N46
G01OH          Manning, Griffin                        1 4  50C 1:44.43                                                                        F             N84
C11OH      OHMH  Miami Hills                   MH                                                                                          USA               N83
D01OH      Barclay, Cameron                        AUSA0501200710MM  251 14 091007122017                              18.37S 0 0 1 6  0 10  3.   1  0100    NN29
D3                                                                                                                                                           N67
D01OH      Barclay, Cameron                        AUSA0501200710MM  254 24 091007122017   22.58S                     23.30S 0 0 3 6  0 14       6  04      NN79
D01OH      Barclay, Cameron                        AUSA0501200710MM  252 34 091007122017   23.57S                     22.45S 0 0 5 3  0  7  7.   2  0200    NN00
D01OH      Barclay, Carson                         AUSA0920200511MM  502 36 111207112017 1:02.77S 1:07.01S                   3 5 0 0 13             02      NN69
D3                                                                                                                                                           N67
D01OH      Barclay, Carson                         AUSA0920200511MM  501 46 111207112017   49.06S      DQS                   1 1 0 0  0             01      NN09
D01OH      Barclay, Carson                         AUSA0920200511MM  503 56 111207122017 1:02.25S                   1:04.94S 0 0 1 3  0 20       4  03      NN89
D01OH      Barclay, Katie                          AUSA01272010 7FF  251 11 UN0807122017   35.59S                     38.16S 0 0 4 5  0 50       5  01      NN19
D3                                                                                                                                                           N67
D01OH      Barclay, Katie                          AUSA01272010 7FF  252 31 UN0807122017   40.84S                     39.72S 0 0 2 3  0 32       2  02      NN19
G01OH          Barclay, Katie                          1 3  25C   13.54           39.72                                                        F             N34
D01OH      Bradley, Will                           AUSA1105200115MM  501 20 151807112017   31.64S   30.69S                   4 6 0 0 18             01      NN58
D3                                                                                                                                                           N67
D01OH      Bradley, Will                           AUSA1105200115MM 1001 50 151807122017 1:10.62S                   1:08.70S 0 0 1 3  0 14       2  01      NN19
G01OH          Bradley, Will                           1 2  50C   32.41 1:08.70                                                                F             N24
D01OH      Bradley, Will                           AUSA1105200115MM 1005 70 151807122017 1:26.60S                   1:28.51S 0 0 1 3  0 17       1  05      NN29
G01OH          Bradley, Will                           1 2  50C   40.88 1:28.51                                                                F             N24
D01OH      Bruce, Kristen                          AUSA08202008 8FF  251 11 UN0807122017   23.04S                     23.72S 0 011 1  0  7  7.   4  0100    NN69
D3                                                                                                                                                           N67
D01OH      Bruce, Kristen                          AUSA08202008 8FF  252 31 UN0807122017   31.05S                     30.44S 0 0 7 4  0 11  2.   2  0200    NN69
G01OH          Bruce, Kristen                          1 3  25C   14.23   29.41   30.44                                                        F             N94
D01OH      Bruce, Kristen                          AUSA08202008 8FF  501 41 UN0807122017   54.43S                     53.34S 0 0 5 2  0  9  4.   4  0100    NN69
D01OH      Bruce, Lindsay                          AUSA09152010 6FF  252 31 UN0807122017   33.26S                        DQS 0 0 6 5  0  0       0  02      NN19
D3                                                                                                                                                           N67
D01OH      Chitwood, Madison                       AUSA1109200016FF  501 19 151807112017   39.38S   39.42S                   2 5 0 0 23             01      NN10
D3                                                                                                                                                           N67
D01OH      Chitwood, Madison                       AUSA1109200016FF  502 39 151807112017   59.88S   54.19S                   1 3 0 0 20             02      NN10
D01OH      Chitwood, Madison                       AUSA1109200016FF  503 59 151807122017   51.07S                     50.24S 0 0 2 1  0 15       3  03      NN20
D01OH      Chumley, Samantha                       AUSA0404200215FF  504 29 151807112017   44.10S   44.55S                   1 5 0 0 15             04      NN00
D3                                                                                                                                                           N67
D01OH      Chumley, Samantha                       AUSA0404200215FF  502 39 151807112017   42.50S   43.70S                   3 1 0 0 16             02      NN00
D01OH      Chumley, Samantha                       AUSA0404200215FF 1001 49 151807122017 1:23.41S                   1:19.21S 0 0 2 5  0 12  1.   4  0100    NN01
G01OH          Chumley, Samantha                       1 2  50C   36.16 1:19.21                                                                F             N85
D01OH      Clark, Lauren                           AUSA0110200215FF  502 39 151807112017            35.23S            34.97S 1 5 2 2  3  2 13.   2  0200    NN98
D3                                                                                                                                                           N67
D01OH      Clark, Lauren                           AUSA0110200215FF 1001 49 151807122017 1:05.64S                   1:06.37S 0 0 3 4  0  2 13.   2  0100    NN49
G01OH          Clark, Lauren                           1 2  50C   32.06 1:06.37                                                                F             N24
D01OH      Clark, Lauren                           AUSA0110200215FF 1005 69 151807122017 1:19.87S                   1:18.44S 0 0 3 1  0  6  9.   5  0500    NN59
G01OH          Clark, Lauren                           1 2  50C   35.84 1:18.44                                                                F             N24
D01OH      Clark, Mason                            AUSA0801200313MM  501 18 131407112017            34.80S                   1 6 0 0 15             01      NN37
D3                                                                                                                                                           N67
D01OH      Clark, Mason                            AUSA0801200313MM  504 28 131407112017   41.29S   40.02S            41.09S 1 5 1 4  8  9  4.   3  0400    NN39
D01OH      Clark, Mason                            AUSA0801200313MM  502 38 131407112017   43.34S   44.66S            43.01S 3 2 1 5 10 10  3.   4  0200    NN49
D01OH      Clark, Morgan                           AUSA0808200511FF  504 25 111207112017   41.94S   40.19S            40.22S 4 5 1 2 10 10  3.   4  0400    NN79
D3                                                                                                                                                           N67
D01OH      Clark, Morgan                           AUSA0808200511FF  501 45 111207112017   36.21S   36.30S                   6 1 0 0 15             01      NN38
D01OH      Clark, Morgan                           AUSA0808200511FF 1005 65 111207122017 1:31.29S                   1:31.15S 0 0 2 5  0 10  3.   4  0500    NN49
G01OH          Clark, Morgan                           1 2  50C   42.45 1:31.15                                                                F             N14
D01OH      Craycraft, Molly                        AUSA1216200412FF  504 25 111207112017   39.58S   37.81S            37.44S 3 2 1 3  8  7  7.   1  0400    NN01
D3                                                                                                                                                           N67
D01OH      Craycraft, Molly                        AUSA1216200412FF  502 35 111207112017   40.75S   39.75S            41.21S 4 4 2 1  6  5 10.   5  0200    NN01
D01OH      Craycraft, Molly                        AUSA1216200412FF  501 45 111207112017   34.19S   34.74S            34.58S 6 2 1 5 11 10  3.   4  0100    NN11
D01OH      Dickerson, A.J.                         AUSA1031200016MM  502 40 151807112017   36.44S   35.88S            35.59S 3 5 1 4  8  9  4.   3  0200    NN79
D3                                                                                                                                                           N67
D01OH      Dickerson, A.J.                         AUSA1031200016MM  503 60 151807122017   44.89S                     40.19S 0 0 1 3  0 10  3.   2  0300    NN98
D01OH      Dickerson, A.J.                         AUSA1031200016MM 1005 70 151807122017 1:21.85S                   1:18.82S 0 0 2 5  0 12  1.   1  0500    NN49
G01OH          Dickerson, A.J.                         1 2  50C   35.56 1:18.82                                                                F             N24
D01OH      Dickerson, Eli                          AUSA0109200314MM  501 18 131407112017   32.98S   32.05S            32.21S 3 5 1 4  8  8  5.   2  0100    NN10
D3                                                                                                                                                           N67
D01OH      Dickerson, Eli                          AUSA0109200314MM  502 38 131407112017   43.70S   44.66S            43.67S 2 2 1 1 10 11  2.   5  0200    NN20
D01OH      Dickerson, Eli                          AUSA0109200314MM 1001 48 131407112017 1:16.87S 1:16.50S          1:12.51S 3 2 1 2  9  8  5.   2  0100    NN80
G01OH          Dickerson, Eli                          1 2  50C   35.29 1:16.50                                                                P             N64
G01OH          Dickerson, Eli                          1 2  50C   33.78 1:12.51                                                                F             N64
D01OH      Dickerson, Walt                         AUSA0209200512MM  504 26 111207112017   38.83S   36.10S               DQS 2 2 2 5  4  0       0  04      NN10
D3                                                                                                                                                           N67
D01OH      Dickerson, Walt                         AUSA0209200512MM  501 46 111207112017   32.07S   31.86S            31.36S 2 3 2 4  2  2 13.   2  0100    NN60
D01OH      Dickerson, Walt                         AUSA0209200512MM 1005 66 111207122017 1:26.80S                   1:24.59S 0 0 3 5  0  3 12.   3  0500    NN40
G01OH          Dickerson, Walt                         1 2  50C   39.91 1:24.59                                                                F             N15
D01OH      Engelman, Jared                         AUSA0419200116MM  501 20 151807112017   28.23S   28.59S            28.55S 3 2 1 6 12 12  1.   6  0100    NN60
D3                                                                                                                                                           N67
D01OH      Engelman, Jared                         AUSA0419200116MM  503 60 151807122017   41.89S                     40.02S 0 0 2 4  0  7  7.   1  0300    NN69
D01OH      Engelman, Jared                         AUSA0419200116MM 1005 70 151807122017 1:25.64S                   1:24.48S 0 0 2 6  0 16       5  05      NN99
G01OH          Engelman, Jared                         1 2  50C   38.11 1:24.48                                                                F             N84
D01OH      Fentress, Emmett                        AUSA10142007 9MM  254 24 091007122017   17.74S                     18.71S 0 0 4 2  0  5 10.   5  0400    NN30
D3                                                                                                                                                           N67
D01OH      Fentress, Emmett                        AUSA10142007 9MM  252 34 091007122017   21.24S                     21.82S 0 0 6 2  0  5 10.   5  0200    NN20
D01OH      Fentress, Emmett                        AUSA10142007 9MM  501 44 091007122017   39.63S                     38.43S 0 0 5 5  0  2 13.   2  0100    NN30
D01OH      Fentress, Erin                          AUSA0711200511FF  504 25 111207112017   34.98S   36.10S            35.95S 3 3 2 5  5  4 11.   4  0400    NN30
D3                                                                                                                                                           N67
D01OH      Fentress, Erin                          AUSA0711200511FF  501 45 111207112017   31.63S   30.67S               DQS 6 3 2 4  2  0       0  01      NN79
D01OH      Fentress, Erin                          AUSA0711200511FF 1005 65 111207122017 1:22.46S                   1:25.66S 0 0 3 1  0  7  7.   6  0500    NN99
G01OH          Fentress, Erin                          1 2  50C   39.16 1:25.66                                                                F             N74
D01OH      Fentress, Evan                          AUSA1227200313MM  504 28 131407112017   31.76S   32.23S            32.19S 1 3 2 4  2  2 13.   2  0400    NN20
D3                                                                                                                                                           N67
D01OH      Fentress, Evan                          AUSA1227200313MM 1001 48 131407112017 1:07.20S 1:06.54S          1:06.75S 2 3 2 4  2  3 12.   3  0100    NN01
G01OH          Fentress, Evan                          1 2  50C   31.46 1:06.54                                                                P             N74
G01OH          Fentress, Evan                          1 2  50C   31.50 1:06.75                                                                F             N74
D01OH      Fentress, Evan                          AUSA1227200313MM 1005 68 131407122017 1:19.76S                   1:22.16S 0 0 2 5  0  5 10.   5  0500    NN10
G01OH          Fentress, Evan                          1 2  50C   36.03 1:22.16                                                                F             N64
D01OH      Francis, Sarah                          AUSA0402200116FF  504 29 151807112017   41.89S   40.23S                   2 5 0 0 14             04      NN78
D3                                                                                                                                                           N67
D01OH      Francis, Sarah                          AUSA0402200116FF  503 59 151807122017   45.28S                     44.30S 0 0 3 3  0 10  3.   3  0300    NN39
D01OH      Francis, Sarah                          AUSA0402200116FF 1005 69 151807122017 1:36.53S                   1:34.93S 0 0 2 6  0 15       6  05      NN59
G01OH          Francis, Sarah                          1 2  50C         1:34.93                                                                F             N04
D01OH      Greenwell, Tim                          AUSA0801200214MM  501 18 131407112017   28.74S   28.05S            28.00S 3 3 2 3  1  1 16.   1  0100    NN20
D3                                                                                                                                                           N67
D01OH      Greenwell, Tim                          AUSA0801200214MM  504 28 131407112017   30.97S   30.51S            30.54S 2 3 2 3  1  1 16.   1  0400    NN20
D01OH      Greenwell, Tim                          AUSA0801200214MM 1005 68 131407122017 1:10.15S                   1:08.73S 0 0 2 3  0  1 16.   1  0500    NN00
G01OH          Greenwell, Tim                          1 2  50C   32.31 1:08.73                                                                F             N64
D01OH      Hall, Auri                              AUSA03182008 9FF  254 23 091007122017   19.48S                     19.85S 0 0 4 1  0  6  9.   5  0400    NN77
D3                                                                                                                                                           N67
D01OH      Hall, Auri                              AUSA03182008 9FF  252 33 091007122017   22.93S                     23.44S 0 0 5 5  0  4 11.   4  0200    NN77
D01OH      Hall, Auri                              AUSA03182008 9FF 1005 63 091007122017 1:57.80S                   1:56.98S 0 0 2 5  0 10  3.   3  0500    NN38
G01OH          Hall, Auri                              1 2  50C   49.52 1:56.98                                                                F             N13
D01OH      Hanna, Sydney                           AUSA0125199918FF  501 19 151807112017   31.58S   31.40S            31.52S 5 2 1 5 10  9  4.   3  0100    NN99
D3                                                                                                                                                           N67
D01OH      Hanna, Sydney                           AUSA0125199918FF  502 39 151807112017   38.15S   37.74S            37.42S 3 4 1 3  7  7  7.   1  0200    NN99
D01OH      Hanna, Sydney                           AUSA0125199918FF 1001 49 151807122017 1:13.50S                   1:12.68S 0 0 3 6  0  8  5.   6  0100    NN69
G01OH          Hanna, Sydney                           1 2  50C   34.31 1:12.68                                                                F             N24
D01OH      Hardoerfer, Grayson                     AUSA07132009 7MM  252 32 UN0807122017   36.73S                     42.81S 0 0 5 3  0 29       3  02      NN41
D3                                                                                                                                                           N67
G01OH          Hardoerfer, Grayson                     1 3  25C                   42.81                                                        F             N95
D01OH      Hardoerfer, Grayson                     AUSA07132009 7MM  253 52 UN0807122017   34.86S                     35.88S 0 0 3 3  0 10  3.   3  0300    NN81
D01OH      Hardoerfer, Grayson                     AUSA07132009 7MM  251 12 UN0807122017   29.43S                     27.78S 0 0 6 6  0 22       4  01      NN41
D01OH      Harper, Ava                             AUSA09232008 8FF  252 31 UN0807122017   29.83S                     31.94S 0 0 8 6  0 14       6  02      NN18
D3                                                                                                                                                           N67
D01OH      Harper, Ava                             AUSA09232008 8FF  501 41 UN0807122017 1:04.91S                     58.62S 0 0 4 3  0 12  1.   1  0100    NN68
D01OH      Harper, Ava                             AUSA09232008 8FF  253 51 UN0807122017                                 DQS 0 0 2 5  0  0       0  03      NN17
D01OH      Harper, Sophia                          AUSA01282010 7FF  251 11 UN0807122017   39.89S                     43.26S 0 0 3 6  0 53       4  01      NN39
D3                                                                                                                                                           N67
D01OH      Herriott, Lilly                         AUSA1003200412FF  501 45 111207112017   32.27S   31.99S            31.88S 8 4 2 6  7  5 10.   5  0100    NN80
D3                                                                                                                                                           N67
D01OH      Herriott, Lilly                         AUSA1003200412FF  503 55 111207122017   42.92S                     42.04S 0 0 6 3  0  1 16.   1  0300    NN99
D01OH      Herriott, Lilly                         AUSA1003200412FF 1005 65 111207122017 1:22.15S                   1:22.22S 0 0 3 2  0  4 11.   4  0500    NN30
G01OH          Herriott, Lilly                         1 2  50C   39.12 1:22.22                                                                F             N25
D01OH      Hirsch, Austin                          AUSA08072010 6MM  251 12 UN0807122017 1:01.58S                   1:11.66S 0 0 1 4  0 52       5  01      NN89
D3                                                                                                                                                           N67
D01OH      Huxell, Callahan                        AUSA0221200611MM  504 26 111207112017               DQS                   2 6 0 0  0             04      NN68
D3                                                                                                                                                           N67
D01OH      Huxell, Callahan                        AUSA0221200611MM  501 46 111207112017   48.72S   48.11S                   1 2 0 0 20             01      NN59
D01OH      Huxell, Callahan                        AUSA0221200611MM  503 56 111207122017 1:01.47S                     57.57S 0 0 2 1  0 15       4  03      NN99
D01OH      Huxell, Finnegan                        AUSA0815200115MM  501 20 151807112017   33.25S   33.83S                   2 2 0 0 22             01      NN69
D3                                                                                                                                                           N67
D01OH      Huxell, Finnegan                        AUSA0815200115MM  503 60 151807122017   45.68S                        NSS 0 0 1 4  0  0       0  03      NN89
D01OH      Huxell, Finnegan                        AUSA0815200115MM 1005 70 151807122017 1:43.68S                        NSS 0 0 1 2  0  0       0  05      NN10
G01OH          Huxell, Finnegan                        1 2  50C                                                                                F             N24
D01OH      King, Emily                             AUSA0915200412FF  251 15 111207112017   14.62S   14.63S            15.08S 6 4 2 1  5  5 10.   5  0100    NN98
D3                                                                                                                                                           N67
D01OH      King, Emily                             AUSA0915200412FF  504 25 111207112017   41.90S   40.39S            39.73S 2 2 1 5 11  9  4.   3  0400    NN09
D01OH      King, Emily                             AUSA0915200412FF  501 45 111207112017   33.37S   33.61S            33.17S 7 2 1 2 10  9  4.   3  0100    NN98
D01OH      Krebs, Kari                             AUSA1030200214FF  501 17 131407112017   37.31S   38.88S                   4 1 0 0 16             01      NN67
D3                                                                                                                                                           N67
D01OH      Krebs, Kari                             AUSA1030200214FF  502 37 131407112017   49.00S   48.97S                   4 6 0 0 17             02      NN67
D01OH      Lane, Sarah                             AUSA06192008 8FF  251 11 UN0807122017                              27.71S 0 0 1 3  0 20       2  01      NN27
D3                                                                                                                                                           N67
D01OH      Lane, Sarah                             AUSA06192008 8FF  254 21 UN0807122017   35.54S                     36.79S 0 0 2 5  0 12  1.   4  0400    NN48
D01OH      Lane, Sarah                             AUSA06192008 8FF  253 51 UN0807122017   35.18S                     39.97S 0 0 4 3  0 19       4  03      NN18
D01OH      Lieving, Laney                          AUSA0329200413FF  501 17 131407112017   38.62S   39.26S                   4 6 0 0 17             01      NN98
D3                                                                                                                                                           N67
D01OH      Lieving, Laney                          AUSA0329200413FF 1001 47 131407112017 1:35.06S 1:31.78S          1:33.79S 1 5 1 6 12 12  1.   6  0100    NN01
G01OH          Lieving, Laney                          1 2  50C   43.05 1:31.78                                                                P             N74
G01OH          Lieving, Laney                          1 2  50C   42.93 1:33.79                                                                F             N74
D01OH      Lieving, Laney                          AUSA0329200413FF  503 57 131407122017   55.46S                     57.97S 0 0 1 2  0 14       4  03      NN19
D01OH      Lieving, Lindsey                        AUSA0629200610FF  501 43 091007122017   39.98S                     37.28S 0 0 5 2  0  4 11.   1  0100    NN30
D3                                                                                                                                                           N67
D01OH      Lieving, Lindsey                        AUSA0629200610FF  253 53 091007122017   24.72S                     24.01S 0 0 4 6  0  6  9.   6  0300    NN20
D01OH      Lieving, Lindsey                        AUSA0629200610FF 1005 63 091007122017 1:37.92S                   1:37.86S 0 0 3 5  0  3 12.   3  0500    NN80
G01OH          Lieving, Lindsey                        1 2  50C   48.01 1:37.86                                                                F             N55
D01OH      Lindquist, Elin                         AUSA05092010 7FF  251 11 UN0807122017   42.43S                     38.58S 0 0 2 3  0 51       2  01      NN89
D3                                                                                                                                                           N67
D01OH      Lindquist, Elin                         AUSA05092010 7FF  252 31 UN0807122017   48.79S                     54.13S 0 0 2 1  0 36       3  02      NN89
G01OH          Lindquist, Elin                         1 3  25C   21.86   33.05   54.13                                                        F             N45
D01OH      Lykins, Emma                            AUSA1128200412FF  251 15 111207112017   17.33S      NSS                   4 5 0 0  0             01      NN97
D3                                                                                                                                                           N67
D01OH      Lykins, Emma                            AUSA1128200412FF  501 45 111207112017   38.58S      NSS                   5 3 0 0  0             01      NN08
D01OH      Lykins, Emma                            AUSA1128200412FF  503 55 111207122017   45.42S                        NSS 0 0 6 5  0  0       0  03      NN28
D01OH      Magato, Isa                             AUSA1228200313FF 1001 47 131407112017          1:30.16S          1:28.22S 2 6 1 5 10 11  2.   5  0100    NN78
D3                                                                                                                                                           N67
G01OH          Magato, Isa                             1 2  50C   43.16 1:30.16                                                                P             N43
G01OH          Magato, Isa                             1 2  50C   41.55 1:28.22                                                                F             N33
D01OH      Magato, Isa                             AUSA1228200313FF  503 57 131407122017   48.14S                        DQS 0 0 2 6  0  0       0  03      NN67
D01OH      Magato, Isa                             AUSA1228200313FF 1005 67 131407122017 1:46.01S                   1:43.11S 0 0 1 5  0 10  3.   5  0500    NN68
G01OH          Magato, Isa                             1 2  50C   51.34 1:43.11                                                                F             N33
D01OH      Marks, Carolyn                          AUSA1020200412FF  251 15 111207112017   17.21S   17.62S                   4 2 0 0 23             01      NN88
D3                                                                                                                                                           N67
D01OH      Marks, Carolyn                          AUSA1020200412FF  501 45 111207112017   42.21S   40.38S                   4 4 0 0 25             01      NN88
D01OH      Marks, Carolyn                          AUSA1020200412FF  503 55 111207122017   54.11S                     53.11S 0 0 3 3  0 19       2  03      NN09
D01OH      Marks, Matthew                          AUSA0830200214MM  501 18 131407112017   32.40S   33.00S            32.57S 2 2 1 5 10  9  4.   3  0100    NN20
D3                                                                                                                                                           N67
D01OH      Marks, Matthew                          AUSA0830200214MM  504 28 131407112017   42.26S   42.58S            39.72S 2 1 1 2  9  8  5.   2  0400    NN30
D01OH      Marks, Matthew                          AUSA0830200214MM  503 58 131407122017   39.07S                        DQS 0 0 3 2  0  0       0  03      NN09
D01OH      McKenney, Jack                          AUSA1008200214MM  501 18 131407112017   30.53S   29.52S            28.90S 2 4 2 5  4  3 12.   3  0100    NN99
D3                                                                                                                                                           N67
D01OH      McKenney, Jack                          AUSA1008200214MM 1001 48 131407112017 1:21.42S 1:10.97S          1:14.84S 1 5 2 6  6  6  9.   6  0100    NN60
G01OH          McKenney, Jack                          1 2  50C   31.03 1:10.97                                                                P             N44
G01OH          McKenney, Jack                          1 2  50C         1:14.84                                                                F             N93
D01OH      McKenney, Jack                          AUSA1008200214MM  503 58 131407122017   42.22S                     41.49S 0 0 3 1  0  5 10.   4  0300    NN19
D01OH      McKenney, Kelsey                        AUSA0724200610FF  254 23 091007122017                              18.73S 0 0 1 2  0  4 11.   1  0400    NN29
D3                                                                                                                                                           N67
D01OH      McKenney, Kelsey                        AUSA0724200610FF  501 43 091007122017   39.86S                     40.39S 0 0 5 4  0  8  5.   3  0100    NN00
D01OH      McKenney, Kelsey                        AUSA0724200610FF  253 53 091007122017   23.55S                     21.53S 0 0 4 2  0  3 12.   3  0300    NN00
D01OH      Meshew, Alex                            AUSA05282008 9MM  251 14 091007122017   26.56S                     27.57S 0 0 2 1  0 31       4  01      NN38
D3                                                                                                                                                           N67
D01OH      Meshew, Alex                            AUSA05282008 9MM  252 34 091007122017   35.30S                     34.61S 0 0 2 2  0 29       4  02      NN38
G01OH          Meshew, Alex                            1 4  25C   16.21   21.50   25.88   34.61                                                F             N64
D01OH      Meshew, Alex                            AUSA05282008 9MM  253 54 091007122017   32.66S                     34.28S 0 0 2 3  0 20       2  03      NN38
D01OH      Meshew, Cali                            AUSA0726200610FF  251 13 091007122017   28.30S                     30.91S 0 0 1 5  0 35       5  01      NN18
D3                                                                                                                                                           N67
D01OH      Meshew, Cali                            AUSA0726200610FF  252 33 091007122017   37.76S                     41.02S 0 0 1 3  0 27       3  02      NN18
G01OH          Meshew, Cali                            1 4  25C                           41.02                                                F             N03
D01OH      Munz, Kai                               AUSA0720200511MM  251 16 111207112017   19.71S   19.93S                   3 6 0 0 18             01      NN07
D3                                                                                                                                                           N67
D01OH      Munz, Kai                               AUSA0720200511MM  501 46 111207112017   46.70S   46.75S                   1 4 0 0 19             01      NN07
D01OH      Munz, Kai                               AUSA0720200511MM  503 56 111207122017 1:00.87S                   1:00.66S 0 0 2 5  0 18       5  03      NN67
D01OH      Munz, Ryder                             AUSA08122007 9MM  251 14 091007122017   17.78S                     19.47S 0 0 6 5  0 16       6  01      NN28
D3                                                                                                                                                           N67
D01OH      Munz, Ryder                             AUSA08122007 9MM  253 54 091007122017   32.39S                        DQS 0 0 3 6  0  0       0  03      NN97
D01OH      Osburne, Kinley                         AUSA10262010 6FF  251 11 UN0807122017   37.67S                     35.86S 0 0 3 2  0 47       2  01      NN89
D3                                                                                                                                                           N67
D01OH      Osburne, Kinley                         AUSA10262010 6FF  252 31 UN0807122017   39.20S                     45.11S 0 0 3 2  0 35       4  02      NN79
G01OH          Osburne, Kinley                         1 3  25C   23.69           45.11                                                        F             N94
D01OH      Pattison, Olivia                        AUSA0823200214FF  501 17 131407112017   32.20S   31.01S            30.96S 4 4 2 4  2  2 13.   2  0100    NN90
D3                                                                                                                                                           N67
D01OH      Pattison, Olivia                        AUSA0823200214FF  502 37 131407112017   37.53S      DQS                   2 3 0 0  0             02      NN69
D01OH      Pattison, Olivia                        AUSA0823200214FF 1005 67 131407122017 1:23.05S                   1:20.76S 0 0 2 1  0  4 11.   4  0500    NN80
G01OH          Pattison, Olivia                        1 2  50C   37.73 1:20.76                                                                F             N65
D01OH      Sager, Anna                             AUSA1027200214FF  501 17 131407112017   38.41S   38.77S                   2 1 0 0 14             01      NN57
D3                                                                                                                                                           N67
D01OH      Sager, Anna                             AUSA1027200214FF  503 57 131407122017   50.59S                     49.82S 0 0 1 3  0 11  2.   1  0300    NN18
D01OH      Sager, Lila                             AUSA10152009 7FF  251 11 UN0807122017   28.84S                     28.96S 0 0 8 4  0 27       3  01      NN18
D3                                                                                                                                                           N67
G01OH          Sager, Lila                             1 2  25C           28.96                                                                F             N72
D01OH      Sager, Lila                             AUSA10152009 7FF  253 51 UN0807122017   30.90S                     30.23S 0 0 6 6  0  8  5.   6  0300    NN38
D01OH      Schaeffer, Katelyn                      AUSA1130199818FF  501 19 151807112017   34.55S   34.73S                   4 1 0 0 16             01      NN40
D3                                                                                                                                                           N67
D01OH      Schaeffer, Katelyn                      AUSA1130199818FF  504 29 151807112017            39.49S                   3 1 0 0 13             04      NN79
D01OH      Schaeffer, Katelyn                      AUSA1130199818FF  502 39 151807112017   40.97S   41.21S                   3 2 0 0 13             02      NN40
D01OH      Schulte, Matthew                        AUSA0426200710MM  252 34 091007122017   22.99S                     24.18S 0 0 6 6  0  8  5.   6  0200    NN30
D3                                                                                                                                                           N67
D01OH      Schulte, Matthew                        AUSA0426200710MM  501 44 091007122017   41.97S                     40.70S 0 0 4 4  0  5 10.   1  0100    NN30
D01OH      Schulte, Matthew                        AUSA0426200710MM  253 54 091007122017   24.73S                     25.70S 0 0 5 1  0 10  3.   6  0300    NN30
D01OH      Schulte, Rachel                         AUSA1025200412FF  251 15 111207112017   15.84S   15.56S            15.56S 7 5 1 4  8  8  5.   2  0100    NN40
D3                                                                                                                                                           N67
D01OH      Schulte, Rachel                         AUSA1025200412FF  502 35 111207112017   42.16S   43.09S            43.19S 3 2 1 2 10  9  4.   3  0200    NN40
D01OH      Schulte, Rachel                         AUSA1025200412FF  501 45 111207112017   35.51S   35.19S            35.85S 8 1 1 1 12 12  1.   6  0100    NN50
D01OH      Serger, Alexandra                       AUSA1104200511FF  251 15 111207112017            19.66S                   1 5 0 0 35             01      NN19
D3                                                                                                                                                           N67
D01OH      Serger, Alexandra                       AUSA1104200511FF  501 45 111207112017   46.31S   45.59S                   3 5 0 0 35             01      NN99
D01OH      Serger, Alexandra                       AUSA1104200511FF  503 55 111207122017   56.98S                     54.65S 0 0 3 1  0 22       4  03      NN10
D01OH      Serger, Amanda                          AUSA1016200313FF  504 27 131407112017   35.10S   34.19S            34.16S 1 3 2 2  3  3 12.   3  0400    NN99
D3                                                                                                                                                           N67
D01OH      Serger, Amanda                          AUSA1016200313FF  502 37 131407112017   35.92S   35.44S            34.64S 4 3 2 4  2  1 16.   1  0200    NN00
D01OH      Serger, Amanda                          AUSA1016200313FF 1001 47 131407112017 1:08.39S 1:09.11S          1:06.25S 1 3 2 4  2  2 13.   2  0100    NN70
G01OH          Serger, Amanda                          1 2  50C   32.25 1:09.11                                                                P             N54
G01OH          Serger, Amanda                          1 2  50C   31.26 1:06.25                                                                F             N44
D01OH      Serger, Josie                           AUSA04222009 8FF  254 21 UN0807122017   37.42S                        DQS 0 0 2 1  0  0       0  04      NN78
D3                                                                                                                                                           N67
D01OH      Serger, Josie                           AUSA04222009 8FF  501 41 UN0807122017 1:08.98S                   1:10.31S 0 0 4 1  0 23       6  01      NN39
D01OH      Storer, Kaitlyn                         AUSA05222010 7FF  251 11 UN0807122017   30.93S                     31.61S 0 0 7 6  0 35       4  01      NN89
D3                                                                                                                                                           N67
G01OH          Storer, Kaitlyn                         1 4  25C   24.42   36.30           31.61                                                F             N55
D01OH      Storer, Kaitlyn                         AUSA05222010 7FF  252 31 UN0807122017   33.77S                     35.39S 0 0 5 3  0 23       5  02      NN99
D01OH      Storer, Patrick                         AUSA01022009 8MM  251 12 UN0807122017   25.23S                     25.53S 0 0 7 3  0 18       5  01      NN99
D3                                                                                                                                                           N67
D01OH      Storer, Patrick                         AUSA01022009 8MM  254 22 UN0807122017                              30.68S 0 0 1 5  0  6  9.   1  0400    NN49
D01OH      Storer, Patrick                         AUSA01022009 8MM  253 52 UN0807122017   33.04S                     33.63S 0 0 4 5  0  8  5.   5  0300    NN20
D01OH      Underwood, Jack                         AUSA01152009 8MM  251 12 UN0807122017   23.23S                     22.22S 0 0 8 5  0  8  5.   1  0100    NN99
D3                                                                                                                                                           N67
D01OH      Underwood, Jack                         AUSA01152009 8MM  252 32 UN0807122017   23.80S                     24.13S 0 0 8 3  0  1 16.   1  0200    NN00
D01OH      Underwood, Jack                         AUSA01152009 8MM  501 42 UN0807122017   50.52S                     52.81S 0 0 5 4  0 10  3.   3  0100    NN00
D01OH      Waterman, Samantha                      AUSA1204200313FF  502 37 131407112017   54.86S   53.83S                   1 4 0 0 19             02      NN40
D3                                                                                                                                                           N67
D01OH      Waterman, Samantha                      AUSA1204200313FF 1001 47 131407112017 1:51.55S 1:50.00S                   2 1 0 0 16             01      NN80
G01OH          Waterman, Samantha                      1 2  50C   51.22 1:50.00                                                                P             N16
D01OH      Waterman, Samantha                      AUSA1204200313FF  503 57 131407122017   57.90S                     56.57S 0 0 1 1  0 13       3  03      NN60
D01OH      Waugh, Kristina                         AUSA0609200313FF  501 17 131407112017   39.28S   38.17S                   1 3 0 0 13             01      NN39
D3                                                                                                                                                           N67
D01OH      Waugh, Kristina                         AUSA0609200313FF  504 27 131407112017   54.19S   54.03S                   3 6 0 0 15             04      NN39
D01OH      Waugh, Kristina                         AUSA0609200313FF  502 37 131407112017   47.40S   46.58S                   4 1 0 0 13             02      NN39
D01OH      Westerkamp, Luke                        AUSA0331200512MM  504 26 111207112017   35.06S   34.63S            33.77S 1 3 2 4  2  2 13.   2  0400    NN11
D3                                                                                                                                                           N67
D01OH      Westerkamp, Luke                        AUSA0331200512MM  503 56 111207122017   41.62S                     40.06S 0 0 4 3  0  1 16.   1  0300    NN20
D01OH      Westerkamp, Luke                        AUSA0331200512MM 1005 66 111207122017 1:19.17S                   1:19.14S 0 0 3 4  0  2 13.   2  0500    NN80
G01OH          Westerkamp, Luke                        1 2  50C   37.39 1:19.14                                                                F             N65
D01OH      Williams, Nathan                        AUSA03192008 9MM  254 24 091007122017   23.99S                     22.42S 0 0 2 4  0 12  1.   1  0400    NN20
D3                                                                                                                                                           N67
D01OH      Williams, Nathan                        AUSA03192008 9MM  501 44 091007122017   46.87S                     46.45S 0 0 3 3  0 13       1  01      NN99
D01OH      Williams, Nathan                        AUSA03192008 9MM  253 54 091007122017   24.81S                     24.68S 0 0 5 6  0  6  9.   5  0300    NN20
D01OH      Wright, Nicholas                        AUSA0922200016MM  504 30 151807112017   34.43S   34.49S            33.82S 2 5 1 1 11 10  3.   4  0400    NN11
D3                                                                                                                                                           N67
D01OH      Wright, Nicholas                        AUSA0922200016MM  503 60 151807122017   42.97S                     42.36S 0 0 2 5  0 15       6  03      NN00
D01OH      Wright, Nicholas                        AUSA0922200016MM 1005 70 151807122017 1:20.47S                   1:20.19S 0 0 2 3  0 14       3  05      NN40
G01OH          Wright, Nicholas                        1 2  50C   35.68 1:20.19                                                                F             N55
E01OH      AOHMH    F 1007  1 UN08  007122017 2:04.80S                   2:02.42S     1 6     6 18.                                            6N   0500X    N05
F01OH          OHMH  AHarper, Ava                             USA092320088 F  1                                                                              N44
G01OH          Harper, Ava                             1 4  25C                                                                                F             N22
F01OH          OHMH  ASager, Lila                             USA101520097 F  2                                                                              N34
G01OH          Sager, Lila                             1 4  25C 1:03.17                                                                        F             N82
F01OH          OHMH  ALane, Sarah                             USA061920088 F  3                                                                              N44
G01OH          Lane, Sarah                             1 4  25C                                                                                F             N12
F01OH          OHMH  ABruce, Kristen                          USA082020088 F  4                                                                              N75
G01OH          Bruce, Kristen                          1 4  25C 2:02.42                                                                        F             N24
E01OH      AOHMH    M 1007  2 UN08  007122017 2:12.93S                   2:40.37S     1 6     4 22.                                            4N   0500X    N15
F01OH          OHMH  AUnderwood, Jack                         USA011520098 M  1                                                                              N06
G01OH          Underwood, Jack                         1 4  25C                                                                                F             N83
F01OH          OHMH  AHardoerfer, Grayson                     USA071320097 M  2                                                                              N77
G01OH          Hardoerfer, Grayson                     1 4  25C   59.55                                                                        F             N06
F01OH          OHMH  AStorer, Patrick                         USA010220098 M  3                                                                              N26
G01OH          Storer, Patrick                         1 4  25C                                                                                F             N04
F01OH          OHMH  AHirsch, Austin                          USA080720106 M  4                                                                              N75
G01OH          Hirsch, Austin                          1 4  25C 2:40.37                                                                        F             N24
E01OH      AOHMH    F 1007  3 0910  007122017 1:29.08S                   1:30.43S     1 5     6 18.                                            6N   0500X    N74
F01OH          OHMH  ALieving, Lindsey                        USA0629200610F  1                                                                              N56
G01OH          Lieving, Lindsey                        1 4  25C                                                                                F             N34
F01OH          OHMH  AMcKenney, Kelsey                        USA0724200610F  2                                                                              N46
G01OH          McKenney, Kelsey                        1 4  25C   46.53                                                                        F             N64
F01OH          OHMH  AHall, Auri                              USA031820089 F  3                                                                              N04
G01OH          Hall, Auri                              1 4  25C                                                                                F             N81
F01OH          OHMH  AMeshew, Cali                            USA0726200610F  4                                                                              N84
G01OH          Meshew, Cali                            1 4  25C 1:30.43                                                                        F             N23
E01OH      AOHMH    M 1007  4 0910  007122017 1:27.61S                        DQS     1 2     0                                                0N   05  X    N04
F01OH          OHMH  ASchulte, Matthew                        USA0426200710M  1                                                                              N66
G01OH          Schulte, Matthew                        1 4  25C                                                                                F             N34
F01OH          OHMH  AWilliams, Nathan                        USA031920089 M  2                                                                              N56
G01OH          Williams, Nathan                        1 4  25C   49.25                                                                        F             N74
F01OH          OHMH  AFentress, Emmett                        USA101420079 M  3                                                                              N66
G01OH          Fentress, Emmett                        1 4  25C                                                                                F             N34
F01OH          OHMH  AMunz, Ryder                             USA081220079 M  4                                                                              N74
G01OH          Munz, Ryder                             1 4  25C 1:27.35                                                                        F             N23
E01OH      AOHMH    F 2007  5 1112  007122017 2:35.99S                   2:31.33S     1 2     3 24.                                            3N   0500X    N74
F01OH          OHMH  ACraycraft, Molly                        USA1216200412F  1                                                                              N56
G01OH          Craycraft, Molly                        1 4  50C   40.40                                                                        F             N74
F01OH          OHMH  AHerriott, Lilly                         USA1003200412F  2                                                                              N26
G01OH          Herriott, Lilly                         1 4  50C 1:22.95                                                                        F             N74
F01OH          OHMH  AFentress, Erin                          USA0711200511F  3                                                                              N85
G01OH          Fentress, Erin                          1 4  50C 1:58.98                                                                        F             N34
F01OH          OHMH  AKing, Emily                             USA0915200412F  4                                                                              N54
G01OH          King, Emily                             1 4  50C 2:31.33                                                                        F             N92
E01OH      AOHMH    M 2007  6 1112  007122017 3:03.22S                   3:10.51S     1 5     5 20.                                            5N   0500X    N74
F01OH          OHMH  ABarclay, Carson                         USA0920200511M  1                                                                              N06
G01OH          Barclay, Carson                         1 4  50C 1:08.34                                                                        F             N54
F01OH          OHMH  ADickerson, Walt                         USA0209200512M  2                                                                              N26
G01OH          Dickerson, Walt                         1 4  50C 1:19.22                                                                        F             N64
F01OH          OHMH  AWesterkamp, Luke                        USA0331200512M  3                                                                              N66
G01OH          Westerkamp, Luke                        1 4  50C 1:52.15                                                                        F             N05
F01OH          OHMH  AMunz, Kai                               USA0720200511M  4                                                                              N83
G01OH          Munz, Kai                               1 4  50C 3:10.51                                                                        F             N22
E01OH      AOHMH    F 2007  7 1314  007122017 2:49.48S                        DQS     1 6     0                                                0N   05  X    N04
F01OH          OHMH  APattison, Olivia                        USA0823200214F  1                                                                              N66
G01OH          Pattison, Olivia                        1 4  50C   38.54                                                                        F             N94
F01OH          OHMH  AMagato, Isa                             USA1228200313F  2                                                                              N44
G01OH          Magato, Isa                             1 4  50C 1:25.16                                                                        F             N92
F01OH          OHMH  ASerger, Amanda                          USA1016200313F  3                                                                              N55
G01OH          Serger, Amanda                          1 4  50C 2:01.16                                                                        F             N93
F01OH          OHMH  ALieving, Laney                          USA0329200413F  4                                                                              N75
G01OH          Lieving, Laney                          1 4  50C 2:40.98                                                                        F             N24
E01OH      AOHMH    M 2007  8 1314  007122017 2:24.38S                   2:14.60S     1 4     2 26.                                            2N   0500X    N84
F01OH          OHMH  AFentress, Evan                          USA1227200313M  1                                                                              N85
G01OH          Fentress, Evan                          1 4  50C   36.45                                                                        F             N04
F01OH          OHMH  AMarks, Matthew                          USA0830200214M  2                                                                              N85
G01OH          Marks, Matthew                          1 4  50C 1:16.74                                                                        F             N24
F01OH          OHMH  AGreenwell, Tim                          USA0801200214M  3                                                                              N85
G01OH          Greenwell, Tim                          1 4  50C 1:28.52                                                                        F             N24
F01OH          OHMH  AMcKenney, Jack                          USA1008200214M  4                                                                              N45
G01OH          McKenney, Jack                          1 4  50C 2:14.60                                                                        F             N83
E01OH      AOHMH    F 2007  9 1518  007122017 2:36.38S                   2:31.59S     1 5     5 20.                                            5N   0500X    N84
F01OH          OHMH  AHanna, Sydney                           USA0125199918F  1                                                                              N45
G01OH          Hanna, Sydney                           1 4  50C   40.74                                                                        F             N53
F01OH          OHMH  AFrancis, Sarah                          USA0402200116F  2                                                                              N65
G01OH          Francis, Sarah                          1 4  50C 1:25.42                                                                        F             N04
F01OH          OHMH  AClark, Lauren                           USA0110200215F  3                                                                              N25
G01OH          Clark, Lauren                           1 4  50C 1:59.82                                                                        F             N73
F01OH          OHMH  ASchaeffer, Katelyn                      USA1130199818F  4                                                                              N37
G01OH          Schaeffer, Katelyn                      1 4  50C 2:31.59                                                                        F             N65
E01OH      AOHMH    M 2007 10 1518  007122017 2:18.87S                   2:27.95S     1 1     4 22.                                            4N   0500X    N94
F01OH          OHMH  ADickerson, A.J.                         USA1031200016M  1                                                                              N25
G01OH          Dickerson, A.J.                         1 4  50C   36.16                                                                        F             N43
F01OH          OHMH  AWright, Nicholas                        USA0922200016M  2                                                                              N66
G01OH          Wright, Nicholas                        1 4  50C 1:18.10                                                                        F             N94
F01OH          OHMH  ABradley, Will                           USA1105200115M  3                                                                              N35
G01OH          Bradley, Will                           1 4  50C 1:58.83                                                                        F             N83
F01OH          OHMH  AEngelman, Jared                         USA0419200116M  4                                                                              N95
G01OH          Engelman, Jared                         1 4  50C 2:27.95                                                                        F             N44
E01OH      AOHMH    F 1006 61 UN08  007122017 1:49.92S                   1:56.18S     1 6     6 18.                                            6N   0100X    N35
F01OH          OHMH  ALane, Sarah                             USA061920088 F  1                                                                              N44
G01OH          Lane, Sarah                             1 4  25C                                                                                F             N12
F01OH          OHMH  ASager, Lila                             USA101520097 F  2                                                                              N34
G01OH          Sager, Lila                             1 4  25C 1:01.26                                                                        F             N82
F01OH          OHMH  AStorer, Kaitlyn                         USA052220107 F  3                                                                              N26
G01OH          Storer, Kaitlyn                         1 4  25C                                                                                F             N04
F01OH          OHMH  ABruce, Kristen                          USA082020088 F  4                                                                              N75
G01OH          Bruce, Kristen                          1 4  25C 1:56.18                                                                        F             N24
E01OH      AOHMH    M 1006 62 UN08  007122017 1:52.56S                   2:19.03S     1 6     6 18.                                            6N   0100X    N25
F01OH          OHMH  AStorer, Patrick                         USA010220098 M  1                                                                              N26
G01OH          Storer, Patrick                         1 4  25C                                                                                F             N04
F01OH          OHMH  AHirsch, Austin                          USA080720106 M  2                                                                              N75
G01OH          Hirsch, Austin                          1 4  25C 1:32.80                                                                        F             N24
F01OH          OHMH  AHardoerfer, Grayson                     USA071320097 M  3                                                                              N77
G01OH          Hardoerfer, Grayson                     1 4  25C                                                                                F             N45
F01OH          OHMH  AUnderwood, Jack                         USA011520098 M  4                                                                              N16
G01OH          Underwood, Jack                         1 4  25C 2:19.03                                                                        F             N54
E01OH      AOHMH    F 1006 71 0910  007122017 1:18.84S                   1:24.36S     1 5     6 18.                                            6N   0100X    N94
F01OH          OHMH  ALieving, Lindsey                        USA0629200610F  1                                                                              N56
G01OH          Lieving, Lindsey                        1 4  25C                                                                                F             N34
F01OH          OHMH  AHall, Auri                              USA031820089 F  2                                                                              N04
G01OH          Hall, Auri                              1 4  25C   42.86                                                                        F             N32
F01OH          OHMH  AMeshew, Cali                            USA0726200610F  3                                                                              N84
G01OH          Meshew, Cali                            1 4  25C                                                                                F             N62
F01OH          OHMH  AMcKenney, Kelsey                        USA0724200610F  4                                                                              N46
G01OH          McKenney, Kelsey                        1 4  25C 1:24.36                                                                        F             N84
E01OH      AOHMH    M 1006 72 0910  007122017 1:19.03S                   1:15.25S     1 5     5 20.                                            5N   0100X    N84
F01OH          OHMH  AFentress, Emmett                        USA101420079 M  1                                                                              N66
G01OH          Fentress, Emmett                        1 4  25C                                                                                F             N34
F01OH          OHMH  AMunz, Ryder                             USA081220079 M  2                                                                              N74
G01OH          Munz, Ryder                             1 4  25C   36.95                                                                        F             N03
F01OH          OHMH  AWilliams, Nathan                        USA031920089 M  3                                                                              N56
G01OH          Williams, Nathan                        1 4  25C                                                                                F             N24
F01OH          OHMH  ASchulte, Matthew                        USA0426200710M  4                                                                              N66
G01OH          Schulte, Matthew                        1 4  25C 1:15.25                                                                        F             N05
E01OH      AOHMH    F 2006 73 1112  007122017 2:17.13S                   2:09.99S     1 2     4 22.                                            4N   0100X    N84
F01OH          OHMH  AFentress, Erin                          USA0711200511F  1                                                                              N85
G01OH          Fentress, Erin                          1 4  50C   31.55                                                                        F             N04
F01OH          OHMH  ACraycraft, Molly                        USA1216200412F  2                                                                              N56
G01OH          Craycraft, Molly                        1 4  50C 1:04.91                                                                        F             N05
F01OH          OHMH  AKing, Emily                             USA0915200412F  3                                                                              N54
G01OH          King, Emily                             1 4  50C 1:37.86                                                                        F             N03
F01OH          OHMH  AHerriott, Lilly                         USA1003200412F  4                                                                              N26
G01OH          Herriott, Lilly                         1 4  50C 2:09.99                                                                        F             N84
E01OH      AOHMH    M 2006 74 1112  007122017 2:46.78S                   2:40.64S     1 1     4 22.                                            4N   0100X    N94
F01OH          OHMH  ADickerson, Walt                         USA0209200512M  1                                                                              N26
G01OH          Dickerson, Walt                         1 4  50C   33.37                                                                        F             N34
F01OH          OHMH  AMunz, Kai                               USA0720200511M  2                                                                              N83
G01OH          Munz, Kai                               1 4  50C 1:21.25                                                                        F             N22
F01OH          OHMH  AHuxell, Callahan                        USA0221200611M  3                                                                              N46
G01OH          Huxell, Callahan                        1 4  50C 2:16.72                                                                        F             N84
F01OH          OHMH  AWesterkamp, Luke                        USA0331200512M  4                                                                              N66
G01OH          Westerkamp, Luke                        1 4  50C 2:40.64                                                                        F             N05
E01OH      AOHMH    F 2006 75 1314  007122017 2:29.42S                   2:20.80S     1 6     5 20.                                            5N   0100X    N84
F01OH          OHMH  APattison, Olivia                        USA0823200214F  1                                                                              N66
G01OH          Pattison, Olivia                        1 4  50C   31.81                                                                        F             N84
F01OH          OHMH  ASager, Anna                             USA1027200214F  2                                                                              N44
G01OH          Sager, Anna                             1 4  50C 1:11.64                                                                        F             N82
F01OH          OHMH  ALieving, Laney                          USA0329200413F  3                                                                              N75
G01OH          Lieving, Laney                          1 4  50C 1:50.23                                                                        F             N14
F01OH          OHMH  ASerger, Amanda                          USA1016200313F  4                                                                              N55
G01OH          Serger, Amanda                          1 4  50C 2:20.80                                                                        F             N04
E01OH      AOHMH    M 2006 76 1314  007122017 2:09.36S                   1:59.24S     1 4     2 26.                                            2N   0100X    N94
F01OH          OHMH  AMcKenney, Jack                          USA1008200214M  1                                                                              N45
G01OH          McKenney, Jack                          1 4  50C   28.31                                                                        F             N63
F01OH          OHMH  AMarks, Matthew                          USA0830200214M  2                                                                              N85
G01OH          Marks, Matthew                          1 4  50C 1:01.20                                                                        F             N24
F01OH          OHMH  AFentress, Evan                          USA1227200313M  3                                                                              N85
G01OH          Fentress, Evan                          1 4  50C 1:30.42                                                                        F             N24
F01OH          OHMH  AGreenwell, Tim                          USA0801200214M  4                                                                              N85
G01OH          Greenwell, Tim                          1 4  50C 1:59.24                                                                        F             N24
E01OH      AOHMH    F 2006 77 1518  007122017 2:22.09S                        DQS     1 6     0                                                0N   01  X    N14
F01OH          OHMH  AHanna, Sydney                           USA0125199918F  1                                                                              N45
G01OH          Hanna, Sydney                           1 4  50C   31.97                                                                        F             N63
F01OH          OHMH  AFrancis, Sarah                          USA0402200116F  2                                                                              N65
G01OH          Francis, Sarah                          1 4  50C 1:07.84                                                                        F             N14
F01OH          OHMH  AChumley, Samantha                       USA0404200215F  3                                                                              N86
G01OH          Chumley, Samantha                       1 4  50C 1:42.43                                                                        F             N35
F01OH          OHMH  AClark, Lauren                           USA0110200215F  4                                                                              N25
G01OH          Clark, Lauren                           1 4  50C 2:14.23                                                                        F             N73
E01OH      AOHMH    M 2006 78 1518  007122017 2:04.23S                   2:03.84S     1 6     3 24.                                            3N   0100X    N94
F01OH          OHMH  AWright, Nicholas                        USA0922200016M  1                                                                              N66
G01OH          Wright, Nicholas                        1 4  50C   30.09                                                                        F             N74
F01OH          OHMH  ABradley, Will                           USA1105200115M  2                                                                              N35
G01OH          Bradley, Will                           1 4  50C 1:02.20                                                                        F             N73
F01OH          OHMH  ADickerson, A.J.                         USA1031200016M  3                                                                              N25
G01OH          Dickerson, A.J.                         1 4  50C 1:34.32                                                                        F             N63
F01OH          OHMH  AEngelman, Jared                         USA0419200116M  4                                                                              N95
G01OH          Engelman, Jared                         1 4  50C 2:03.84                                                                        F             N34
C11OH      OHNOR Normandy Swim Team            Nor                                                                                         USA               N17
D01OH      Bachman, Katie                          AUSA0103200710FF  251 13 091007122017   22.93S                     20.75S 0 0 3 2  0 16       2  01      NN78
D3                                                                                                                                                           N67
D01OH      Bachman, Katie                          AUSA0103200710FF  254 23 091007122017   22.86S                     22.59S 0 0 3 1  0 12  1.   3  0400    NN19
D01OH      Bachman, Katie                          AUSA0103200710FF  501 43 091007122017   46.15S                     47.95S 0 0 3 2  0 21       4  01      NN78
D01OH      Bachman, Peter                          AUSA0103200710MM  251 14 091007122017   20.02S                     18.87S 0 0 4 4  0 14       2  01      NN88
D3                                                                                                                                                           N67
D01OH      Bachman, Peter                          AUSA0103200710MM  501 44 091007122017                              43.93S 0 0 1 4  0 11  2.   2  0100    NN48
D01OH      Bachman, Peter                          AUSA0103200710MM  253 54 091007122017   31.20S                     27.88S 0 0 3 1  0 18       4  03      NN98
D01OH      Burch, Kyle                             AUSA09232009 7MM  251 12 UN0807122017   35.21S                     29.14S 0 0 3 4  0 25       1  01      NN28
D3                                                                                                                                                           N67
D01OH      Burch, Kyle                             AUSA09232009 7MM  501 42 UN0807122017 1:07.38S                   1:14.16S 0 0 4 1  0 27       6  01      NN78
D01OH      Burch, Kyle                             AUSA09232009 7MM  253 52 UN0807122017   50.43S                     47.79S 0 0 2 1  0 14       3  03      NN38
D01OH      Burch, Nathan                           AUSA0418200611MM  251 16 111207112017   19.96S   18.96S                   1 3 0 0 14             01      NN58
D3                                                                                                                                                           N67
D01OH      Burch, Nathan                           AUSA0418200611MM  503 56 111207122017 1:00.41S                     56.42S 0 0 2 2  0 13       2  03      NN88
D01OH      Burch, Nathan                           AUSA0418200611MM 1005 66 111207122017 2:02.94S                   1:58.31S 0 0 1 3  0 11  2.   2  0500    NN59
G01OH          Burch, Nathan                           1 2  50C   57.76 1:58.31                                                                F             N24
D01OH      Cannon, Aya                             AUSA0222200017FF  501 19 151807112017   32.27S   31.32S            31.62S 5 5 1 2  9 10  3.   4  0100    NN88
D3                                                                                                                                                           N67
D01OH      Cannon, Aya                             AUSA0222200017FF  502 39 151807112017   41.70S   39.77S            40.81S 2 5 1 5 10 11  2.   5  0200    NN09
D01OH      Cannon, Aya                             AUSA0222200017FF  503 59 151807122017   45.68S                     44.42S 0 0 3 2  0 11  2.   4  0300    NN18
D01OH      Cannon, Nami                            AUSA0712200214FF  501 17 131407112017   28.93S   29.30S            28.92S 4 3 2 3  1  1 16.   1  0100    NN39
D3                                                                                                                                                           N67
D01OH      Cannon, Nami                            AUSA0712200214FF  504 27 131407112017   31.24S   32.05S            31.03S 3 3 2 3  1  1 16.   1  0400    NN19
D01OH      Cannon, Nami                            AUSA0712200214FF 1001 47 131407112017 1:06.29S 1:06.39S          1:04.55S 3 3 2 3  1  1 16.   1  0100    NN00
G01OH          Cannon, Nami                            1 2  50C   31.37 1:06.39                                                                P             N83
G01OH          Cannon, Nami                            1 2  50C   31.02 1:04.55                                                                F             N73
D01OH      Carle, Jacob                            AUSA0531200314MM  501 18 131407112017   40.11S   37.68S                   2 6 0 0 16             01      NN97
D3                                                                                                                                                           N67
D01OH      Carle, Jacob                            AUSA0531200314MM  502 38 131407112017   48.95S   51.23S                   3 1 0 0 15             02      NN97
D01OH      Carle, Leah                             AUSA09062009 7FF  251 11 UN0807122017   57.00S                     53.92S 0 0 2 2  0 54       3  01      NN97
D3                                                                                                                                                           N67
D01OH      Carle, Leah                             AUSA09062009 7FF  252 31 UN0807122017   46.37S                        DQS 0 0 2 5  0  0       0  02      NN87
G01OH          Carle, Leah                             1 3  25C                   56.57                                                        F             N62
D01OH      Carver, Max                             AUSA0811200610MM  254 24 091007122017   15.51S                     15.34S 0 0 4 3  0  1 16.   1  0400    NN28
D3                                                                                                                                                           N67
D01OH      Carver, Max                             AUSA0811200610MM  252 34 091007122017   17.08S                     16.74S 0 0 6 3  0  1 16.   1  0200    NN38
D01OH      Carver, Max                             AUSA0811200610MM  501 44 091007122017   34.09S                     33.10S 0 0 5 3  0  1 16.   1  0100    NN28
D01OH      Carver, Mia                             AUSA0511200314FF  504 27 131407112017   38.14S   38.47S            38.51S 2 4 1 4  8  8  5.   2  0400    NN98
D3                                                                                                                                                           N67
D01OH      Carver, Mia                             AUSA0511200314FF  502 37 131407112017   37.58S   38.98S            39.45S 4 4 2 5  4  6  9.   6  0200    NN09
D01OH      Carver, Mia                             AUSA0511200314FF  503 57 131407122017   45.34S                     44.22S 0 0 2 4  0  7  7.   2  0300    NN08
D01OH      DeMoss, Cora                            AUSA06132009 7FF  251 11 UN0807122017   29.34S                     33.20S 0 0 8 2  0 40       6  01      NN38
D3                                                                                                                                                           N67
G01OH          DeMoss, Cora                            1 2  25C           33.20                                                                F             N92
D01OH      DeMoss, Cora                            AUSA06132009 7FF  254 21 UN0807122017   44.82S                        DQS 0 0 1 4  0  0       0  04      NN28
D01OH      DeMoss, Cora                            AUSA06132009 7FF  501 41 UN0807122017 1:17.39S                   1:21.51S 0 0 3 4  0 27       4  01      NN88
D01OH      DeMoss, Lucy                            AUSA0823200511FF  251 15 111207112017   19.08S   19.33S                   3 5 0 0 31             01      NN08
D3                                                                                                                                                           N67
D01OH      DeMoss, Lucy                            AUSA0823200511FF  501 45 111207112017   43.37S   43.46S                   4 6 0 0 32             01      NN08
D01OH      DeMoss, Lucy                            AUSA0823200511FF  503 55 111207122017   57.53S                     58.39S 0 0 3 6  0 25       6  03      NN38
D01OH      DeMoss, Sophie                          AUSA11122007 9FF  251 13 091007122017   23.15S                     22.16S 0 0 3 6  0 20       3  01      NN78
D3                                                                                                                                                           N67
D01OH      DeMoss, Sophie                          AUSA11122007 9FF  252 33 091007122017   33.30S                     32.08S 0 0 2 5  0 23       4  02      NN88
G01OH          DeMoss, Sophie                          1 2  25C           32.08                                                                F             N83
D01OH      DeMoss, Sophie                          AUSA11122007 9FF  253 53 091007122017   32.07S                     29.91S 0 0 2 1  0 14       2  03      NN88
D01OH      Dial, Evan                              AUSA0226200116MM  501 20 151807112017   27.86S   28.24S            28.29S 3 4 1 2  9 10  3.   4  0100    NN68
D3                                                                                                                                                           N67
D01OH      Dial, Evan                              AUSA0226200116MM  504 30 151807112017   30.65S   30.81S            30.66S 1 4 2 1  5  5 10.   5  0400    NN58
D01OH      Dial, Evan                              AUSA0226200116MM 1001 50 151807122017 1:02.61S                   1:02.06S 0 0 3 5  0  5 10.   5  0100    NN28
G01OH          Dial, Evan                              1 2  50C   29.02 1:02.06                                                                F             N92
D01OH      Dickman, James                          AUSA04172008 9MM  251 14 091007122017   23.85S                     23.36S 0 0 2 3  0 26       2  01      NN98
D3                                                                                                                                                           N67
D01OH      Dickman, James                          AUSA04172008 9MM  501 44 091007122017   57.54S                     52.36S 0 0 2 2  0 23       3  01      NN98
D01OH      Dickman, James                          AUSA04172008 9MM  253 54 091007122017   28.18S                     27.32S 0 0 4 6  0 15       5  03      NN98
D01OH      Dickman, Maddie                         AUSA0710200610FF  252 33 091007122017   28.16S                     27.41S 0 0 3 2  0 18       4  02      NN19
D3                                                                                                                                                           N67
G01OH          Dickman, Maddie                         1 2  25C           27.41                                                                F             N04
D01OH      Dickman, Maddie                         AUSA0710200610FF  501 43 091007122017   53.05S                     51.33S 0 0 2 3  0 26       2  01      NN09
D01OH      Dickman, Maddie                         AUSA0710200610FF  253 53 091007122017   28.04S                     27.06S 0 0 3 6  0 12  1.   6  0300    NN49
D01OH      Dressell, Grace                         AUSA0622200610FF  251 13 091007122017   16.51S                     16.06S 0 0 6 2  0  2 13.   2  0100    NN69
D3                                                                                                                                                           N67
D01OH      Dressell, Grace                         AUSA0622200610FF  501 43 091007122017   36.46S                     36.00S 0 0 6 2  0  3 12.   3  0100    NN69
D01OH      Dressell, Grace                         AUSA0622200610FF 1005 63 091007122017 1:46.36S                   1:40.28S 0 0 3 1  0  5 10.   5  0500    NN20
G01OH          Dressell, Grace                         1 2  50C         1:40.28                                                                F             N44
D01OH      Dressell, Nick                          AUSA0712200412MM  251 16 111207112017   14.71S   14.51S            14.48S 4 3 2 3  1  1 16.   1  0100    NN10
D3                                                                                                                                                           N67
D01OH      Dressell, Nick                          AUSA0712200412MM  501 46 111207112017   33.01S   33.30S            32.76S 4 4 2 5  4  4 11.   4  0100    NN10
D01OH      Dressell, Nick                          AUSA0712200412MM  503 56 111207122017   45.44S                     43.19S 0 0 4 2  0  2 13.   2  0300    NN49
D01OH      Ensley, Elizabeth                       AUSA1024200115FF  501 19 151807112017   39.09S   39.34S                   2 2 0 0 22             01      NN00
D3                                                                                                                                                           N67
D01OH      Ensley, Elizabeth                       AUSA1024200115FF  503 59 151807122017   54.86S                     51.44S 0 0 2 6  0 16       4  03      NN30
D01OH      Eversole, Kamryn                        AUSA0117200116FF  502 39 151807112017   42.98S   43.51S                   2 1 0 0 15             02      NN89
D3                                                                                                                                                           N67
D01OH      Eversole, Kamryn                        AUSA0117200116FF  503 59 151807122017   50.32S                        NSS 0 0 2 5  0  0       0  03      NN89
D01OH      Eversole, Kamryn                        AUSA0117200116FF 1005 69 151807122017                                 NSS 0 0 1 5  0  0       0  05      NN29
G01OH          Eversole, Kamryn                        1 2  50C                                                                                F             N34
D01OH      Farrell, Jackson                        AUSA0419200512MM  504 26 111207112017   47.37S   48.45S            44.82S 1 5 1 3  7  7  7.   1  0400    NN01
D3                                                                                                                                                           N67
D01OH      Farrell, Jackson                        AUSA0419200512MM  503 56 111207122017   48.00S                     47.54S 0 0 4 5  0  4 11.   4  0300    NN20
D01OH      Farrell, Jackson                        AUSA0419200512MM 1005 66 111207122017 1:32.78S                   1:33.76S 0 0 3 6  0  7  7.   6  0500    NN70
G01OH          Farrell, Jackson                        1 2  50C         1:33.76                                                                F             N94
D01OH      Farrell, Lanie                          AUSA12102009 7FF  251 11 UN0807122017   25.03S                     25.16S 0 010 4  0 12  1.   3  0100    NN59
D3                                                                                                                                                           N67
D01OH      Farrell, Lanie                          AUSA12102009 7FF  252 31 UN0807122017   28.44S                     29.26S 0 0 8 2  0  9  4.   4  0200    NN59
D01OH      Farrell, Lanie                          AUSA12102009 7FF  501 41 UN0807122017   58.43S                     56.45S 0 0 5 1  0 11  2.   5  0100    NN59
D01OH      Fleshour, Abigail                       AUSA0626200214FF  501 17 131407112017   36.19S   35.62S            35.78S 3 5 1 1 11 11  2.   5  0100    NN31
D3                                                                                                                                                           N67
D01OH      Fleshour, Abigail                       AUSA0626200214FF  502 37 131407112017   42.23S   41.60S            40.89S 3 2 1 2  9  9  4.   3  0200    NN11
D01OH      Fleshour, Abigail                       AUSA0626200214FF 1005 67 131407122017 1:33.37S                   1:28.56S 0 0 1 3  0  7  7.   2  0500    NN90
G01OH          Fleshour, Abigail                       1 2  50C   40.62 1:28.56                                                                F             N75
D01OH      Fleshour, Audrey                        AUSA1006200610FF  251 13 091007122017   17.77S                     18.38S 0 0 6 6  0  8  5.   6  0100    NN20
D3                                                                                                                                                           N67
D01OH      Fleshour, Audrey                        AUSA1006200610FF  254 23 091007122017   20.97S                     22.54S 0 0 4 6  0 11  2.   6  0400    NN20
D01OH      Fleshour, Audrey                        AUSA1006200610FF  253 53 091007122017   22.23S                     21.32S 0 0 4 3  0  1 16.   1  0300    NN10
D01OH      Forschner, Emily                        AUSA12222008 8FF  252 31 UN0807122017   29.28S                     29.50S 0 0 8 1  0 10  3.   5  0200    NN50
D3                                                                                                                                                           N67
D01OH      Forschner, Emily                        AUSA12222008 8FF  501 41 UN0807122017   50.93S                     52.96S 0 0 5 3  0  8  5.   3  0100    NN40
D01OH      Forschner, Emily                        AUSA12222008 8FF  253 51 UN0807122017   29.59S                     30.10S 0 0 6 1  0  7  7.   5  0300    NN40
D01OH      Forschner, Mikey                        AUSA0828200511MM  251 16 111207112017   19.76S   19.50S                   2 6 0 0 16             01      NN89
D3                                                                                                                                                           N67
D01OH      Forschner, Mikey                        AUSA0828200511MM  502 36 111207112017   56.98S      DQS                   1 2 0 0  0             02      NN69
D01OH      Forschner, Mikey                        AUSA0828200511MM  501 46 111207112017   45.49S   46.01S                   4 6 0 0 18             01      NN89
D01OH      Fried, Payton                           AUSA02032009 8FF  251 11 UN0807122017   27.51S                     24.29S 0 0 9 2  0  9  4.   1  0100    NN29
D3                                                                                                                                                           N67
D01OH      Fried, Payton                           AUSA02032009 8FF  254 21 UN0807122017   37.94S                     36.59S 0 0 2 6  0 11  2.   3  0400    NN39
D01OH      Fried, Payton                           AUSA02032009 8FF  252 31 UN0807122017   33.28S                     36.19S 0 0 6 1  0 26       3  02      NN98
D01OH      Griffin, Bethany                        AUSA0622200610FF  251 13 091007122017   22.68S                     22.36S 0 0 3 4  0 22       4  01      NN79
D3                                                                                                                                                           N67
D01OH      Griffin, Bethany                        AUSA0622200610FF  501 43 091007122017   49.14S                     49.25S 0 0 3 5  0 24       6  01      NN89
D01OH      Griffin, Bethany                        AUSA0622200610FF  253 53 091007122017   29.76S                     30.03S 0 0 2 2  0 15       3  03      NN79
D01OH      Griffin, Kyle                           AUSA0714200115MM  501 20 151807112017   36.23S   34.16S                   1 3 0 0 23             01      NN48
D3                                                                                                                                                           N67
D01OH      Griffin, Kyle                           AUSA0714200115MM  502 40 151807112017   45.20S   44.29S                   3 6 0 0 16             02      NN58
D01OH      Griffin, Kyle                           AUSA0714200115MM  503 60 151807122017   43.61S                     42.20S 0 0 2 6  0 13       5  03      NN78
D01OH      Hardin, Charlotte                       AUSA06242008 8FF  251 11 UN0807122017   29.48S                     28.72S 0 0 8 5  0 24       2  01      NN50
D3                                                                                                                                                           N67
G01OH          Hardin, Charlotte                       1 2  25C           28.72                                                                F             N05
D01OH      Hardin, Charlotte                       AUSA06242008 8FF  252 31 UN0807122017   39.36S                        DQS 0 0 3 5  0  0       0  02      NN30
G01OH          Hardin, Charlotte                       1 3  25C                   39.34                                                        F             N15
D01OH      Hardin, Charlotte                       AUSA06242008 8FF  501 41 UN0807122017 1:12.53S                   1:04.32S 0 0 3 3  0 15       1  01      NN80
D01OH      Hiett, Laura                            AUSA1026200610FF  254 23 091007122017                              21.41S 0 0 1 5  0 10  3.   2  0400    NN77
D3                                                                                                                                                           N67
D01OH      Hiett, Laura                            AUSA1026200610FF  501 43 091007122017   45.08S                     41.07S 0 0 4 1  0 10  3.   1  0100    NN58
D01OH      Hiett, Laura                            AUSA1026200610FF 1005 63 091007122017 1:49.55S                   1:49.61S 0 0 3 6  0  8  5.   6  0500    NN19
G01OH          Hiett, Laura                            1 2  50C   52.34 1:49.61                                                                F             N93
D01OH      Hubbell, Keally                         AUSA08032007 9FF  253 53 091007122017   34.35S                     31.46S 0 0 1 3  0 16       1  03      NN29
D3                                                                                                                                                           N67
D01OH      Hubbell, Keally                         AUSA08032007 9FF  251 13 091007122017   20.91S                     19.95S 0 0 5 6  0 12  1.   5  0100    NN69
D01OH      Hubbell, Keally                         AUSA08032007 9FF  252 33 091007122017   28.58S                     26.33S 0 0 3 5  0 10  3.   1  0200    NN69
G01OH          Hubbell, Keally                         1 2  25C           26.33                                                                F             N24
D01OH      Hubbell, Kendall                        AUSA1020200412FF  504 25 111207112017   39.46S   40.64S            40.50S 4 2 1 1 12 11  2.   5  0400    NN70
D3                                                                                                                                                           N67
D01OH      Hubbell, Kendall                        AUSA1020200412FF  502 35 111207112017   42.69S   43.60S            44.03S 2 2 1 5 11 11  2.   5  0200    NN70
D01OH      Hubbell, Kendall                        AUSA1020200412FF  501 45 111207112017   34.94S   37.45S                   7 5 0 0 18             01      NN49
D01OH      Jenkins, Cade                           AUSA0201200215MM  501 20 151807112017   30.25S   30.36S                   5 6 0 0 16             01      NN38
D3                                                                                                                                                           N67
D01OH      Jenkins, Cade                           AUSA0201200215MM 1001 50 151807122017 1:12.85S                   1:06.79S 0 0 1 4  0 12  1.   1  0100    NN39
G01OH          Jenkins, Cade                           1 2  50C   31.51 1:06.79                                                                F             N14
D01OH      Jenkins, Cade                           AUSA0201200215MM  503 60 151807122017   42.09S                     40.17S 0 0 2 2  0  9  4.   3  0300    NN88
D01OH      Jenkins, Quinn                          AUSA0331200512MM  501 46 111207112017   44.67S   43.17S                   2 1 0 0 13             01      NN98
D3                                                                                                                                                           N67
D01OH      Jenkins, Quinn                          AUSA0331200512MM  503 56 111207122017 1:01.55S                     54.24S 0 0 2 6  0 11  2.   1  0300    NN79
D01OH      Johnston, Katie                         AUSA0913200412FF  251 15 111207112017   16.04S   16.11S            16.69S 6 5 1 6 12 12  1.   6  0100    NN70
D3                                                                                                                                                           N67
D01OH      Johnston, Katie                         AUSA0913200412FF  502 35 111207112017   47.05S   46.41S            44.85S 2 1 1 6 13 12  1.   6  0200    NN70
D01OH      Johnston, Katie                         AUSA0913200412FF  503 55 111207122017   48.61S                     49.79S 0 0 5 2  0 13       5  03      NN69
D01OH      Johnston, Sarah                         AUSA1219200511FF  251 15 111207112017   18.90S   18.64S                   3 2 0 0 28             01      NN39
D3                                                                                                                                                           N67
D01OH      Johnston, Sarah                         AUSA1219200511FF  503 55 111207122017 1:02.03S                     57.85S 0 0 2 4  0 24       2  03      NN79
D01OH      Johnston, Sarah                         AUSA1219200511FF 1005 65 111207122017 2:12.93S                   1:59.27S 0 0 1 2  0 16       4  05      NN10
G01OH          Johnston, Sarah                         1 2  50C         1:59.27                                                                F             N64
D01OH      Jones, Brennan                          AUSA07072008 8MM  251 12 UN0807122017   22.90S                     23.79S 0 0 8 4  0 14       6  01      NN49
D3                                                                                                                                                           N67
D01OH      Jones, Brennan                          AUSA07072008 8MM  254 22 UN0807122017   33.00S                     47.24S 0 0 2 4  0 13       5  04      NN39
D01OH      Jones, Brennan                          AUSA07072008 8MM  501 42 UN0807122017 1:00.53S                   1:01.77S 0 0 5 5  0 14       5  01      NN89
D01OH      Jones, Carson                           AUSA0325200611MM  251 16 111207112017   17.77S   17.54S            18.16S 3 2 1 2  9 10  3.   4  0100    NN99
D3                                                                                                                                                           N67
D01OH      Jones, Carson                           AUSA0325200611MM  502 36 111207112017   54.58S   52.82S            54.91S 2 2 1 2  9 10  3.   4  0200    NN99
D01OH      Jones, Carson                           AUSA0325200611MM  501 46 111207112017   42.72S   43.19S                   2 5 0 0 14             01      NN58
D01OH      Jones, Rowan                            AUSA07292007 9FF  251 13 091007122017   24.74S                     25.80S 0 0 2 5  0 28       4  01      NN38
D3                                                                                                                                                           N67
D01OH      Jones, Rowan                            AUSA07292007 9FF  252 33 091007122017   33.51S                     31.48S 0 0 2 1  0 22       3  02      NN38
G01OH          Jones, Rowan                            1 2  25C           31.48                                                                F             N23
D01OH      Jones, Rowan                            AUSA07292007 9FF  253 53 091007122017   34.15S                     33.98S 0 0 2 6  0 19       5  03      NN48
D01OH      Jones, Townes                           AUSA09302009 7MM  251 12 UN0807122017   29.53S                     30.41S 0 0 5 4  0 31       4  01      NN19
D3                                                                                                                                                           N67
D01OH      Jones, Townes                           AUSA09302009 7MM  252 32 UN0807122017   49.60S                     37.44S 0 0 4 6  0 19       1  02      NN29
D01OH      Jones, Townes                           AUSA09302009 7MM  501 42 UN0807122017 1:12.02S                   1:08.52S 0 0 3 2  0 22       3  01      NN59
D01OH      Kern, Ellie                             AUSA03212008 9FF  251 13 091007122017   24.26S                     24.63S 0 0 2 4  0 27       3  01      NN77
D3                                                                                                                                                           N67
D01OH      Kern, Ellie                             AUSA03212008 9FF  254 23 091007122017   29.42S                     28.78S 0 0 1 3  0 21       3  04      NN77
D01OH      Kern, Ellie                             AUSA03212008 9FF  252 33 091007122017   29.46S                     26.74S 0 0 3 1  0 16       2  02      NN77
G01OH          Kern, Ellie                             1 2  25C           26.74                                                                F             N72
D01OH      Kissick, Cole                           AUSA0617200511MM  251 16 111207112017   17.51S   17.20S            17.29S 4 2 2 6  6  6  9.   6  0100    NN79
D3                                                                                                                                                           N67
D01OH      Kissick, Cole                           AUSA0617200511MM  504 26 111207112017   45.09S   45.72S            45.47S 2 5 2 6  6  5 10.   5  0400    NN99
D01OH      Kissick, Cole                           AUSA0617200511MM  503 56 111207122017   49.43S                     50.27S 0 0 4 1  0  7  7.   6  0300    NN09
D01OH      Kissick, Erin                           AUSA03072008 9FF  251 13 091007122017   26.33S                     28.97S 0 0 2 6  0 33       6  01      NN68
D3                                                                                                                                                           N67
D01OH      Kissick, Erin                           AUSA03072008 9FF  252 33 091007122017   32.34S                     30.14S 0 0 2 2  0 21       2  02      NN58
G01OH          Kissick, Erin                           1 2  25C   27.85   30.14                                                                F             N04
D01OH      Kissick, Erin                           AUSA03072008 9FF  501 43 091007122017 1:07.47S                   1:06.64S 0 0 1 3  0 30       2  01      NN09
D01OH      Koenig, Lauren                          AUSA03122008 9FF  251 13 091007122017   28.02S                     27.52S 0 0 1 2  0 31       2  01      NN88
D3                                                                                                                                                           N67
D01OH      Koenig, Lauren                          AUSA03122008 9FF  252 33 091007122017   34.90S                     36.21S 0 0 2 6  0 26       6  02      NN98
G01OH          Koenig, Lauren                          1 2  25C           36.21                                                                F             N93
D01OH      Koenig, Lauren                          AUSA03122008 9FF  501 43 091007122017 1:04.57S                   1:03.18S 0 0 2 5  0 29       4  01      NN49
D01OH      Koenig, Maleah                          AUSA12032009 7FF  251 11 UN0807122017   35.25S                     36.10S 0 0 4 2  0 48       3  01      NN19
D3                                                                                                                                                           N67
D01OH      Koenig, Maleah                          AUSA12032009 7FF  501 41 UN0807122017   48.85S                   1:25.30S 0 0 6 5  0 29       6  01      NN49
D01OH      Koester, Tucker                         AUSA0328200314MM  502 38 131407112017   36.28S   35.13S            34.13S 2 3 2 4  2  2 13.   2  0200    NN70
D3                                                                                                                                                           N67
D01OH      Koester, Tucker                         AUSA0328200314MM 1001 48 131407112017 1:07.71S 1:07.54S          1:04.00S 1 3 2 2  3  2 13.   2  0100    NN41
G01OH          Koester, Tucker                         1 2  50C   31.64 1:07.54                                                                P             N25
G01OH          Koester, Tucker                         1 2  50C   30.19 1:04.00                                                                F             N15
D01OH      Koester, Tucker                         AUSA0328200314MM  503 58 131407122017   37.07S                     35.61S 0 0 3 4  0  1 16.   1  0300    NN00
D01OH      Lamb, Aidan                             AUSA1002200610MM  254 24 091007122017   22.22S                     21.64S 0 0 3 5  0 10  3.   4  0400    NN97
D3                                                                                                                                                           N67
D01OH      Lamb, Aidan                             AUSA1002200610MM  501 44 091007122017   46.23S                     47.52S 0 0 4 1  0 15       5  01      NN67
D01OH      Lamb, Aidan                             AUSA1002200610MM 1005 64 091007122017 2:03.88S                        DQS 0 0 2 5  0  0       0  05      NN87
G01OH          Lamb, Aidan                             1 2  50C   54.62 1:57.50                                                                F             N23
D01OH      Lamb, Gavin                             AUSA0814200412MM  504 26 111207112017   43.37S   42.86S            42.34S 1 2 2 1  5  4 11.   4  0400    NN98
D3                                                                                                                                                           N67
D01OH      Lamb, Gavin                             AUSA0814200412MM  501 46 111207112017   38.89S   37.60S            35.60S 4 2 1 3  7  7  7.   1  0100    NN98
D01OH      Lamb, Gavin                             AUSA0814200412MM 1005 66 111207122017 1:38.68S                        DQS 0 0 2 3  0  0       0  05      NN08
G01OH          Lamb, Gavin                             1 2  50C   41.81 1:34.14                                                                F             N33
D01OH      Lang, Charlotte                         AUSA05272010 7FF  252 31 UN0807122017   34.01S                     31.51S 0 0 5 5  0 13       1  02      NN59
D3                                                                                                                                                           N67
D01OH      Lang, Charlotte                         AUSA05272010 7FF  501 41 UN0807122017 1:12.41S                   1:07.04S 0 0 4 6  0 20       4  01      NN00
D01OH      Lang, Charlotte                         AUSA05272010 7FF  253 51 UN0807122017   37.92S                     34.67S 0 0 4 6  0 15       2  03      NN79
D01OH      Lang, Liam                              AUSA0508200710MM  252 34 091007122017   26.82S                     26.76S 0 0 4 2  0 17       4  02      NN47
D3                                                                                                                                                           N67
G01OH          Lang, Liam                              1 2  25C           26.76                                                                F             N22
D01OH      Lang, Liam                              AUSA0508200710MM  253 54 091007122017   27.63S                     30.00S 0 0 4 1  0 19       6  03      NN47
D01OH      Lang, Liam                              AUSA0508200710MM 1005 64 091007122017 2:10.20S                   2:17.28S 0 0 1 4  0 12  1.   2  0500    NN28
G01OH          Lang, Liam                              1 2  50C 1:08.93 2:17.28                                                                F             N23
D01OH      Lolli, Meghan                           AUSA0818200016FF  504 29 151807112017   36.45S   35.18S            35.01S 3 2 1 4  8  8  5.   2  0400    NN79
D3                                                                                                                                                           N67
D01OH      Lolli, Meghan                           AUSA0818200016FF 1001 49 151807122017 1:15.25S                   1:11.81S 0 0 2 3  0  7  7.   1  0100    NN39
G01OH          Lolli, Meghan                           1 2  50C   34.97 1:11.81                                                                F             N24
D01OH      Lolli, Meghan                           AUSA0818200016FF 1005 69 151807122017 1:25.05S                   1:24.59S 0 0 2 4  0  9  4.   2  0500    NN49
G01OH          Lolli, Meghan                           1 2  50C   39.51 1:24.59                                                                F             N24
D01OH      Lolli, Sarah                            AUSA1005200214FF  502 37 131407112017   36.91S   34.37S            34.65S 3 3 2 3  1  2 13.   2  0200    NN39
D3                                                                                                                                                           N67
D01OH      Lolli, Sarah                            AUSA1005200214FF  503 57 131407122017   43.19S                     37.06S 0 0 3 6  0  1 16.   1  0300    NN58
D01OH      Lolli, Sarah                            AUSA1005200214FF 1005 67 131407122017 1:18.88S                   1:14.12S 0 0 2 2  0  1 16.   1  0500    NN09
G01OH          Lolli, Sarah                            1 2  50C   35.14 1:14.12                                                                F             N73
D01OH      Losekamp, Elizabeth                     AUSA0903200313FF  501 17 131407112017   41.38S   39.44S                   1 4 0 0 18             01      NN70
D3                                                                                                                                                           N67
D01OH      Losekamp, Elizabeth                     AUSA0903200313FF  503 57 131407122017   46.23S                     44.31S 0 0 2 1  0  8  5.   3  0300    NN21
D01OH      Meerdink, Jackson                       AUSA09102009 7MM  251 12 UN0807122017   25.20S                     22.65S 0 0 8 6  0  9  4.   2  0100    NN70
D3                                                                                                                                                           N67
D01OH      Meerdink, Jackson                       AUSA09102009 7MM  252 32 UN0807122017   33.71S                     32.26S 0 0 6 5  0 14       4  02      NN50
D01OH      Meerdink, Jackson                       AUSA09102009 7MM  253 52 UN0807122017   35.03S                     29.45S 0 0 3 4  0  4 11.   1  0300    NN80
D01OH      Pacitti, Noah                           AUSA0418200116MM  501 20 151807112017   32.21S   31.90S                   2 3 0 0 20             01      NN48
D3                                                                                                                                                           N67
D01OH      Pacitti, Noah                           AUSA0418200116MM  504 30 151807112017   32.96S   33.23S            33.18S 1 2 1 4  8  9  4.   3  0400    NN79
D01OH      Pacitti, Noah                           AUSA0418200116MM 1005 70 151807122017 1:24.00S                   1:24.14S 0 0 2 1  0 15       4  05      NN19
G01OH          Pacitti, Noah                           1 2  50C   37.08 1:24.14                                                                F             N24
D01OH      Partin, Blythe                          AUSA12212007 9FF  251 13 091007122017   22.94S                     23.35S 0 0 3 5  0 24       6  01      NN09
D3                                                                                                                                                           N67
D01OH      Partin, Blythe                          AUSA12212007 9FF  252 33 091007122017   26.60S                     27.03S 0 0 3 3  0 17       3  02      NN09
G01OH          Partin, Blythe                          1 2  25C   13.12   27.03                                                                F             N44
D01OH      Partin, Blythe                          AUSA12212007 9FF  501 43 091007122017   56.10S                     50.95S 0 0 2 4  0 25       1  01      NN09
D01OH      Partin, Caroline                        AUSA0502200611FF  251 15 111207112017   21.03S   20.64S                   2 6 0 0 38             01      NN59
D3                                                                                                                                                           N67
D01OH      Partin, Caroline                        AUSA0502200611FF  501 45 111207112017   49.42S   50.82S                   2 3 0 0 41             01      NN59
D01OH      Partin, Caroline                        AUSA0502200611FF  503 55 111207122017 1:13.86S                   1:04.52S 0 0 2 5  0 28       4  03      NN20
D01OH      Ramage, Alison                          AUSA0528200512FF  504 25 111207112017   48.37S   48.41S                   3 1 0 0 19             04      NN88
D3                                                                                                                                                           N67
D01OH      Ramage, Alison                          AUSA0528200512FF  501 45 111207112017   41.89S   42.18S                   5 6 0 0 30             01      NN78
D01OH      Ramage, Alison                          AUSA0528200512FF  503 55 111207122017                              58.45S 0 0 1 2  0 26       2  03      NN28
D01OH      Ramage, Gwen                            AUSA05102010 7FF  251 11 UN0807122017   33.02S                     29.63S 0 0 5 5  0 28       2  01      NN38
D3                                                                                                                                                           N67
D01OH      Ramage, Gwen                            AUSA05102010 7FF  254 21 UN0807122017   42.40S                        DQS 0 0 1 3  0  0       0  04      NN18
D01OH      Ramage, Gwen                            AUSA05102010 7FF  252 31 UN0807122017   33.77S                     32.60S 0 0 5 4  0 15       2  02      NN38
D01OH      Ramage, Patrick                         AUSA06252007 9MM  251 14 091007122017   18.03S                     18.19S 0 0 6 1  0  9  4.   5  0100    NN69
D3                                                                                                                                                           N67
D01OH      Ramage, Patrick                         AUSA06252007 9MM  254 24 091007122017   21.20S                     21.57S 0 0 3 4  0  9  4.   3  0400    NN69
D01OH      Ramage, Patrick                         AUSA06252007 9MM  501 44 091007122017   41.37S                     40.94S 0 0 4 3  0  6  9.   2  0100    NN69
D01OH      Rand, Elijah                            AUSA08212007 9MM  251 14 091007122017   18.11S                     17.91S 0 0 6 6  0  7  7.   4  0100    NN48
D3                                                                                                                                                           N67
D01OH      Rand, Elijah                            AUSA08212007 9MM  252 34 091007122017   24.49S                        NSS 0 0 5 5  0  0       0  02      NN08
D01OH      Rand, Elijah                            AUSA08212007 9MM  501 44 091007122017   42.86S                        NSS 0 0 4 2  0  0       0  01      NN08
D01OH      Retzler, Ben                            AUSA03202010 7MM  251 12 UN0807122017   33.09S                     30.81S 0 0 4 2  0 33       3  01      NN58
D3                                                                                                                                                           N67
D01OH      Retzler, Ben                            AUSA03202010 7MM  252 32 UN0807122017   41.74S                     39.12S 0 0 4 2  0 20       2  02      NN58
D01OH      Rhoads, Benson                          AUSA08212008 8MM  251 12 UN0807122017   21.17S                     18.63S 0 0 9 2  0  1 16.   1  0100    NN79
D3                                                                                                                                                           N67
D01OH      Rhoads, Benson                          AUSA08212008 8MM  501 42 UN0807122017   43.13S                     41.87S 0 0 6 3  0  1 16.   1  0100    NN79
D01OH      Rhoads, Benson                          AUSA08212008 8MM  253 52 UN0807122017   23.12S                     22.38S 0 0 4 3  0  1 16.   1  0300    NN79
D01OH      Rhoads, Charlotte                       AUSA09172011 5FF  251 11 UN0807122017   34.75S                        NSS 0 0 4 3  0  0       0  01      NN30
D3                                                                                                                                                           N67
D01OH      Rogers, Alex                            AUSA03192010 7MM  251 12 UN0807122017   39.84S                     44.20S 0 0 2 5  0 47       6  01      NN68
D3                                                                                                                                                           N67
D01OH      Rogers, Alex                            AUSA03192010 7MM  252 32 UN0807122017   49.89S                     42.72S 0 0 3 3  0 27       3  02      NN78
G01OH          Rogers, Alex                            1 2  25C           42.72                                                                F             N23
D01OH      Rogers, Alex                            AUSA03192010 7MM  501 42 UN0807122017 1:35.40S                        DQS 0 0 2 4  0  0       0  01      NN68
D01OH      Rogers, Liam                            AUSA0827200313MM  501 18 131407112017   38.09S   37.95S                   1 1 0 0 18             01      NN28
D3                                                                                                                                                           N67
D01OH      Rogers, Liam                            AUSA0827200313MM  502 38 131407112017   44.90S   43.49S            42.46S 1 5 1 3  7  8  5.   2  0200    NN59
D01OH      Rogers, Liam                            AUSA0827200313MM  503 58 131407122017   53.30S                        DQS 0 0 1 3  0  0       0  03      NN18
D01OH      Shafer, Julia                           AUSA06072007 9FF  251 13 091007122017   17.28S                     16.38S 0 0 6 5  0  4 11.   4  0100    NN98
D3                                                                                                                                                           N67
D01OH      Shafer, Julia                           AUSA06072007 9FF  254 23 091007122017   18.86S                     18.43S 0 0 4 5  0  3 12.   3  0400    NN98
D01OH      Shafer, Julia                           AUSA06072007 9FF  501 43 091007122017   38.14S                     38.23S 0 0 6 5  0  5 10.   4  0100    NN88
D01OH      Speed, Stella                           AUSA12252009 7FF  251 11 UN0807122017   26.45S                     27.36S 0 010 6  0 18       5  01      NN09
D3                                                                                                                                                           N67
D01OH      Speed, Stella                           AUSA12252009 7FF  501 41 UN0807122017 1:05.95S                   1:06.39S 0 0 4 4  0 19       3  01      NN49
D01OH      Speed, Stella                           AUSA12252009 7FF  253 51 UN0807122017   37.26S                     31.43S 0 0 4 5  0  9  4.   1  0300    NN19
D01OH      Stephenson, Brady                       AUSA08012009 7MM  251 12 UN0807122017   27.11S                     26.86S 0 0 6 3  0 20       2  01      NN60
D3                                                                                                                                                           N67
D01OH      Stephenson, Brady                       AUSA08012009 7MM  254 22 UN0807122017   36.91S                     32.61S 0 0 2 1  0  7  7.   1  0400    NN90
D01OH      Stephenson, Brady                       AUSA08012009 7MM  501 42 UN0807122017 1:10.21S                   1:04.26S 0 0 4 6  0 18       4  01      NN01
D01OH      Troyer, Claire                          AUSA05042010 7FF  251 11 UN0807122017   37.81S                        NSS 0 0 3 1  0  0       0  01      NN29
D3                                                                                                                                                           N67
D01OH      Troyer, Claire                          AUSA05042010 7FF  252 31 UN0807122017   44.72S                        NSS 0 0 2 2  0  0       0  02      NN29
G01OH          Troyer, Claire                          1 3  25C                                                                                F             N53
D01OH      Troyer, Hagen                           AUSA06232008 8MM  251 12 UN0807122017   33.32S                        NSS 0 0 4 5  0  0       0  01      NN98
D3                                                                                                                                                           N67
D01OH      Troyer, Hagen                           AUSA06232008 8MM  252 32 UN0807122017   41.42S                        NSS 0 0 4 4  0  0       0  02      NN98
D01OH      Troyer, Warren                          AUSA0101200710MM  251 14 091007122017   26.60S                        NSS 0 0 2 6  0  0       0  01      NN19
D3                                                                                                                                                           N67
D01OH      Troyer, Warren                          AUSA0101200710MM  252 34 091007122017   33.85S                        NSS 0 0 2 4  0  0       0  02      NN29
G01OH          Troyer, Warren                          1 4  25C                                                                                F             N73
D01OH      Ujvari, Nolyn                           AUSA1125200511FF  251 15 111207112017   19.10S   19.45S                   3 6 0 0 33             01      NN68
D3                                                                                                                                                           N67
D01OH      Ujvari, Nolyn                           AUSA1125200511FF  503 55 111207122017   56.86S                     52.92S 0 0 3 5  0 18       1  03      NN98
D01OH      Washburn, Hannah                        AUSA04292011 6FF  251 11 UN0807122017   32.79S                        NSS 0 0 5 4  0  0       0  01      NN99
D3                                                                                                                                                           N67
D01OH      Washburn, Logan                         AUSA0308200611MM  502 36 111207112017   51.62S      NSS                   3 4 0 0  0             02      NN29
D3                                                                                                                                                           N67
D01OH      Washburn, Logan                         AUSA0308200611MM  501 46 111207112017   41.56S      NSS                   3 5 0 0  0             01      NN29
D01OH      Washburn, Logan                         AUSA0308200611MM  503 56 111207122017   54.73S                        NSS 0 0 3 5  0  0       0  03      NN49
E01OH      AOHNOR   F 1007  1 UN08  007122017 1:56.33S                   1:52.58S     1 2     4 22.                                            4N   0500X    N45
F01OH          OHNOR AFarrell, Lanie                          USA121020097 F  1                                                                              N85
G01OH          Farrell, Lanie                          1 4  25C                                                                                F             N33
F01OH          OHNOR AForschner, Emily                        USA122220088 F  2                                                                              N86
G01OH          Forschner, Emily                        1 4  25C 1:07.04                                                                        F             N05
F01OH          OHNOR APaddock, Brooke                                    8 F  3                                                                              N74
G01OH          Paddock, Brooke                         1 4  25C                                                                                F             N73
F01OH          OHNOR ASpeed, Stella                           USA122520097 F  4                                                                              N55
G01OH          Speed, Stella                           1 4  25C 1:52.58                                                                        F             N73
E01OH      AOHNOR   M 1007  2 UN08  007122017 1:55.50S                   1:55.58S     1 2     2 26.                                            2N   0500X    N45
F01OH          OHNOR AMeerdink, Jackson                       USA091020097 M  1                                                                              N17
G01OH          Meerdink, Jackson                       1 4  25C                                                                                F             N54
F01OH          OHNOR ARhoads, Benson                          USA082120088 M  2                                                                              N06
G01OH          Rhoads, Benson                          1 4  25C   54.66                                                                        F             N93
F01OH          OHNOR AJones, Brennan                          USA070720088 M  3                                                                              N06
G01OH          Jones, Brennan                          1 4  25C                                                                                F             N43
F01OH          OHNOR AStephenson, Brady                       USA080120097 M  4                                                                              N37
G01OH          Stephenson, Brady                       1 4  25C 1:55.58                                                                        F             N55
E01OH      AOHNOR   F 1007  3 0910  007122017 1:26.32S                   1:19.74S     1 3     1 32.                                            1N   0500X    N05
F01OH          OHNOR AHiett, Laura                            USA1026200610F  1                                                                              N25
G01OH          Hiett, Laura                            1 4  25C                                                                                F             N62
F01OH          OHNOR AFleshour, Audrey                        USA1006200610F  2                                                                              N96
G01OH          Fleshour, Audrey                        1 4  25C   44.49                                                                        F             N84
F01OH          OHNOR AShafer, Julia                           USA060720079 F  3                                                                              N55
G01OH          Shafer, Julia                           1 4  25C                                                                                F             N03
F01OH          OHNOR ADressell, Grace                         USA0622200610F  4                                                                              N36
G01OH          Dressell, Grace                         1 4  25C 1:19.74                                                                        F             N54
E01OH      AOHNOR   M 1007  4 0910  007122017 1:28.78S                   1:21.76S     1 1     2 26.                                            2N   0500X    N15
F01OH          OHNOR ARamage, Patrick                         USA062520079 M  1                                                                              N36
G01OH          Ramage, Patrick                         1 4  25C                                                                                F             N73
F01OH          OHNOR ACarver, Max                             USA0811200610M  2                                                                              N84
G01OH          Carver, Max                             1 4  25C   44.07                                                                        F             N72
F01OH          OHNOR ALamb, Aidan                             USA1002200610M  3                                                                              N64
G01OH          Lamb, Aidan                             1 4  25C                                                                                F             N02
F01OH          OHNOR ARand, Elijah                            USA082120079 M  4                                                                              N05
G01OH          Rand, Elijah                            1 4  25C 1:21.76                                                                        F             N23
E01OH      AOHNOR   F 2007  5 1112  007122017 3:04.35S                   3:05.70S     1 6     6 18.                                            6N   0500X    N05
F01OH          OHNOR ADeMoss, Lucy                            USA0823200511F  1                                                                              N25
G01OH          DeMoss, Lucy                            1 4  50C   49.57                                                                        F             N13
F01OH          OHNOR AJohnston, Katie                         USA0913200412F  2                                                                              N56
G01OH          Johnston, Katie                         1 4  50C 1:40.10                                                                        F             N64
F01OH          OHNOR AHubbell, Kendall                        USA1020200412F  3                                                                              N56
G01OH          Hubbell, Kendall                        1 4  50C 2:19.38                                                                        F             N84
F01OH          OHNOR AJohnston, Sarah                         USA1219200511F  4                                                                              N56
G01OH          Johnston, Sarah                         1 4  50C 3:05.70                                                                        F             N64
E01OH      AOHNOR   M 2007  6 1112  007122017 2:53.47S                   2:49.68S     1 4     2 26.                                            2N   0500X    N15
F01OH          OHNOR ADressell, Nick                          USA0712200412M  1                                                                              N06
G01OH          Dressell, Nick                          1 4  50C   39.18                                                                        F             N93
F01OH          OHNOR AFarrell, Jackson                        USA0419200512M  2                                                                              N86
G01OH          Farrell, Jackson                        1 4  50C 1:33.46                                                                        F             N94
F01OH          OHNOR ALamb, Gavin                             USA0814200412M  3                                                                              N74
G01OH          Lamb, Gavin                             1 4  50C 2:10.82                                                                        F             N82
F01OH          OHNOR AJones, Carson                           USA0325200611M  4                                                                              N75
G01OH          Jones, Carson                           1 4  50C 2:49.68                                                                        F             N93
E01OH      AOHNOR   F 2007  7 1314  007122017 2:32.42S                   2:21.26S     1 2     1 32.                                            1N   0500X    N05
F01OH          OHNOR ACarver, Mia                             USA0511200314F  1                                                                              N74
G01OH          Carver, Mia                             1 4  50C   38.69                                                                        F             N72
F01OH          OHNOR ALolli, Sarah                            USA1005200214F  2                                                                              N15
G01OH          Lolli, Sarah                            1 4  50C 1:15.83                                                                        F             N33
F01OH          OHNOR ACannon, Nami                            USA0712200214F  3                                                                              N15
G01OH          Cannon, Nami                            1 4  50C 1:46.28                                                                        F             N33
F01OH          OHNOR AFleshour, Abigail                       USA0626200214F  4                                                                              N17
G01OH          Fleshour, Abigail                       1 4  50C 2:21.26                                                                        F             N25
E01OH      AOHNOR   M 2007  8 1314  007122017 2:59.14S                   2:52.53S     1 1     5 20.                                            5N   0500X    N15
F01OH          OHNOR AJenkins, Quinn                          USA0331200512M  1                                                                              N16
G01OH          Jenkins, Quinn                          1 4  50C   34.35                                                                        F             N04
F01OH          OHNOR ARogers, Liam                            USA0827200313M  2                                                                              N35
G01OH          Rogers, Liam                            1 4  50C 1:23.15                                                                        F             N33
F01OH          OHNOR AKoester, Tucker                         USA0328200314M  3                                                                              N66
G01OH          Koester, Tucker                         1 4  50C 2:24.43                                                                        F             N74
F01OH          OHNOR ACarle, Jacob                            USA0531200314M  4                                                                              N05
G01OH          Carle, Jacob                            1 4  50C 2:52.53                                                                        F             N13
E01OH      AOHNOR   F 2007  9 1518  007122017 2:43.88S                   2:38.49S     1 1     6 18.                                            6N   0500X    N25
F01OH          OHNOR ACannon, Aya                             USA0222200017F  1                                                                              N74
G01OH          Cannon, Aya                             1 4  50C   39.20                                                                        F             N62
F01OH          OHNOR ALosekamp, Elizabeth                     USA0903200313F  2                                                                              N97
G01OH          Losekamp, Elizabeth                     1 4  50C 1:23.67                                                                        F             N16
F01OH          OHNOR ALolli, Meghan                           USA0818200016F  3                                                                              N55
G01OH          Lolli, Meghan                           1 4  50C 1:59.21                                                                        F             N73
F01OH          OHNOR AEnsley, Elizabeth                       USA1024200115F  4                                                                              N27
G01OH          Ensley, Elizabeth                       1 4  50C 2:38.49                                                                        F             N45
E01OH      AOHNOR   M 2007 10 1518  007122017 2:22.10S                   2:20.15S     1 6     3 24.                                            3N   0500X    N15
F01OH          OHNOR ADial, Evan                              USA0226200116M  1                                                                              N34
G01OH          Dial, Evan                              1 4  50C   34.72                                                                        F             N22
F01OH          OHNOR AJenkins, Cade                           USA0201200215M  2                                                                              N45
G01OH          Jenkins, Cade                           1 4  50C 1:13.59                                                                        F             N63
F01OH          OHNOR APacitti, Noah                           USA0418200116M  3                                                                              N65
G01OH          Pacitti, Noah                           1 4  50C 1:46.74                                                                        F             N73
F01OH          OHNOR AGriffin, Kyle                           USA0714200115M  4                                                                              N65
G01OH          Griffin, Kyle                           1 4  50C 2:20.15                                                                        F             N73
E01OH      AOHNOR   F 1006 61 UN08  007122017 1:46.22S                   1:41.59S     1 5     4 22.                                            4N   0100X    N45
F01OH          OHNOR AForschner, Emily                        USA122220088 F  1                                                                              N86
G01OH          Forschner, Emily                        1 4  25C                                                                                F             N34
F01OH          OHNOR AFarrell, Lanie                          USA121020097 F  2                                                                              N85
G01OH          Farrell, Lanie                          1 4  25C   55.79                                                                        F             N93
F01OH          OHNOR ASpeed, Stella                           USA122520097 F  3                                                                              N55
G01OH          Speed, Stella                           1 4  25C                                                                                F             N03
F01OH          OHNOR APaddock, Brooke                                    8 F  4                                                                              N74
G01OH          Paddock, Brooke                         1 4  25C 1:41.59                                                                        F             N44
E01OH      AOHNOR   M 1006 62 UN08  007122017 1:45.08S                   1:36.81S     1 2     4 22.                                            4N   0100X    N55
F01OH          OHNOR AJones, Brennan                          USA070720088 M  1                                                                              N06
G01OH          Jones, Brennan                          1 4  25C                                                                                F             N43
F01OH          OHNOR AMeerdink, Jackson                       USA091020097 M  2                                                                              N17
G01OH          Meerdink, Jackson                       1 4  25C   50.82                                                                        F             N05
F01OH          OHNOR AStephenson, Brady                       USA080120097 M  3                                                                              N37
G01OH          Stephenson, Brady                       1 4  25C                                                                                F             N74
F01OH          OHNOR ARhoads, Benson                          USA082120088 M  4                                                                              N06
G01OH          Rhoads, Benson                          1 4  25C 1:36.81                                                                        F             N24
E01OH      AOHNOR   F 1006 71 0910  007122017 1:15.48S                   1:11.59S     1 4     1 32.                                            1N   0100X    N15
F01OH          OHNOR AShafer, Julia                           USA060720079 F  1                                                                              N55
G01OH          Shafer, Julia                           1 4  25C                                                                                F             N03
F01OH          OHNOR AFleshour, Audrey                        USA1006200610F  2                                                                              N96
G01OH          Fleshour, Audrey                        1 4  25C   35.44                                                                        F             N84
F01OH          OHNOR AHiett, Laura                            USA1026200610F  3                                                                              N25
G01OH          Hiett, Laura                            1 4  25C                                                                                F             N62
F01OH          OHNOR ADressell, Grace                         USA0622200610F  4                                                                              N36
G01OH          Dressell, Grace                         1 4  25C 1:11.59                                                                        F             N44
E01OH      AOHNOR   M 1006 72 0910  007122017 1:13.25S                   1:12.90S     1 4     2 26.                                            2N   0100X    N15
F01OH          OHNOR ARamage, Patrick                         USA062520079 M  1                                                                              N36
G01OH          Ramage, Patrick                         1 4  25C                                                                                F             N73
F01OH          OHNOR ALamb, Aidan                             USA1002200610M  2                                                                              N64
G01OH          Lamb, Aidan                             1 4  25C   47.64                                                                        F             N52
F01OH          OHNOR ABachman, Peter                          USA0103200710M  3                                                                              N85
G01OH          Bachman, Peter                          1 4  25C                                                                                F             N33
F01OH          OHNOR ACarver, Max                             USA0811200610M  4                                                                              N94
G01OH          Carver, Max                             1 4  25C 1:12.90                                                                        F             N03
E01OH      AOHNOR   F 2006 73 1112  007122017 2:46.20S                   2:45.36S     1 6     6 18.                                            6N   0100X    N15
F01OH          OHNOR AJohnston, Katie                         USA0913200412F  1                                                                              N56
G01OH          Johnston, Katie                         1 4  50C   40.37                                                                        F             N44
F01OH          OHNOR ARamage, Alison                          USA0528200512F  2                                                                              N95
G01OH          Ramage, Alison                          1 4  50C 1:29.02                                                                        F             N04
F01OH          OHNOR ADeMoss, Lucy                            USA0823200511F  3                                                                              N25
G01OH          DeMoss, Lucy                            1 4  50C 2:10.18                                                                        F             N33
F01OH          OHNOR AHubbell, Kendall                        USA1020200412F  4                                                                              N66
G01OH          Hubbell, Kendall                        1 4  50C 2:45.36                                                                        F             N84
E01OH      AOHNOR   M 2006 74 1112  007122017 2:35.00S                   2:25.89S     1 4     1 32.                                            1N   0100X    N15
F01OH          OHNOR ALamb, Gavin                             USA0814200412M  1                                                                              N74
G01OH          Lamb, Gavin                             1 4  50C   37.72                                                                        F             N62
F01OH          OHNOR AFarrell, Jackson                        USA0419200512M  2                                                                              N86
G01OH          Farrell, Jackson                        1 4  50C 1:14.16                                                                        F             N84
F01OH          OHNOR AJones, Carson                           USA0325200611M  3                                                                              N75
G01OH          Jones, Carson                           1 4  50C 2:04.85                                                                        F             N83
F01OH          OHNOR ADressell, Nick                          USA0712200412M  4                                                                              N06
G01OH          Dressell, Nick                          1 4  50C 2:25.89                                                                        F             N24
E01OH      AOHNOR   F 2006 75 1314  007122017 2:10.78S                   2:07.90S     1 4     1 32.                                            1N   0100X    N15
F01OH          OHNOR ALolli, Sarah                            USA1005200214F  1                                                                              N15
G01OH          Lolli, Sarah                            1 4  50C   30.42                                                                        F             N13
F01OH          OHNOR ACarver, Mia                             USA0511200314F  2                                                                              N74
G01OH          Carver, Mia                             1 4  50C 1:04.41                                                                        F             N92
F01OH          OHNOR AFleshour, Abigail                       USA0626200214F  3                                                                              N17
G01OH          Fleshour, Abigail                       1 4  50C 1:40.35                                                                        F             N25
F01OH          OHNOR ACannon, Nami                            USA0712200214F  4                                                                              N15
G01OH          Cannon, Nami                            1 4  50C 2:07.90                                                                        F             N33
E01OH      AOHNOR   M 2006 76 1314  007122017 2:15.00S                   2:28.43S     1 5     5 20.                                            5N   0100X    N15
F01OH          OHNOR ARogers, Liam                            USA0827200313M  1                                                                              N35
G01OH          Rogers, Liam                            1 4  50C   51.48                                                                        F             N23
F01OH          OHNOR ACarle, Jacob                            USA0531200314M  2                                                                              N05
G01OH          Carle, Jacob                            1 4  50C 1:15.65                                                                        F             N13
F01OH          OHNOR AJenkins, Quinn                          USA0331200512M  3                                                                              N16
G01OH          Jenkins, Quinn                          1 4  50C 1:27.21                                                                        F             N24
F01OH          OHNOR AKoester, Tucker                         USA0328200314M  4                                                                              N66
G01OH          Koester, Tucker                         1 4  50C 2:28.43                                                                        F             N74
E01OH      AOHNOR   F 2006 77 1518  007122017 2:21.86S                   2:28.39S     1 1     5 20.                                            5N   0100X    N25
F01OH          OHNOR ALolli, Meghan                           USA0818200016F  1                                                                              N55
G01OH          Lolli, Meghan                           1 4  50C   32.72                                                                        F             N43
F01OH          OHNOR ALosekamp, Elizabeth                     USA0903200313F  2                                                                              N97
G01OH          Losekamp, Elizabeth                     1 4  50C 1:15.81                                                                        F             N16
F01OH          OHNOR AEnsley, Elizabeth                       USA1024200115F  3                                                                              N27
G01OH          Ensley, Elizabeth                       1 4  50C 1:55.80                                                                        F             N35
F01OH          OHNOR ACannon, Aya                             USA0222200017F  4                                                                              N74
G01OH          Cannon, Aya                             1 4  50C 2:28.39                                                                        F             N92
E01OH      AOHNOR   M 2006 78 1518  007122017 1:57.00S                   2:07.09S     1 2     4 22.                                            4N   0100X    N25
F01OH          OHNOR AJenkins, Cade                           USA0201200215M  1                                                                              N45
G01OH          Jenkins, Cade                           1 4  50C   30.96                                                                        F             N43
F01OH          OHNOR APacitti, Noah                           USA0418200116M  2                                                                              N65
G01OH          Pacitti, Noah                           1 4  50C 1:04.48                                                                        F             N73
F01OH          OHNOR AGriffin, Kyle                           USA0714200115M  3                                                                              N65
G01OH          Griffin, Kyle                           1 4  50C 1:38.23                                                                        F             N73
F01OH          OHNOR ADial, Evan                              USA0226200116M  4                                                                              N34
G01OH          Dial, Evan                              1 4  50C 2:07.09                                                                        F             N42
C11OH      OHTP  Terrace Park                  TP                                                                                          USA               N44
D01OH      Albrecht, Avy                           AUSA09242007 9FF  254 23 091007122017   26.67S                     26.33S 0 0 2 6  0 20       6  04      NN68
D3                                                                                                                                                           N67
D01OH      Albrecht, Avy                           AUSA09242007 9FF  501 43 091007122017   49.16S                     47.77S 0 0 3 1  0 20       3  01      NN68
D01OH      Albrecht, Avy                           AUSA09242007 9FF 1005 63 091007122017 2:05.45S                   2:01.40S 0 0 1 4  0 12  1.   3  0500    NN49
G01OH          Albrecht, Avy                           1 2  50C   59.45 2:01.40                                                                F             N24
D01OH      Albrecht, Mika                          AUSA10082009 7FF  251 11 UN0807122017   34.10S                     28.06S 0 0 5 1  0 21       1  01      NN09
D3                                                                                                                                                           N67
D01OH      Albrecht, Mika                          AUSA10082009 7FF  252 31 UN0807122017   33.19S                     28.68S 0 0 6 2  0  6  9.   1  0200    NN49
D01OH      Albrecht, Mika                          AUSA10082009 7FF  253 51 UN0807122017   42.77S                     39.70S 0 0 3 1  0 18       3  03      NN29
D01OH      Albrecht, William                       AUSA10082009 7MM  251 12 UN0807122017   21.77S                     22.00S 0 0 9 1  0  6  9.   5  0100    NN70
D3                                                                                                                                                           N67
D01OH      Albrecht, William                       AUSA10082009 7MM  252 32 UN0807122017   26.18S                     25.82S 0 0 8 5  0  5 10.   5  0200    NN80
D01OH      Albrecht, William                       AUSA10082009 7MM  253 52 UN0807122017   30.09S                     28.12S 0 0 4 2  0  3 12.   3  0300    NN70
D01OH      Barclay, Kendall                        AUSA01012009 8FF  251 11 UN0807122017   24.47S                     24.86S 0 011 6  0 11  2.   6  0100    NN30
D3                                                                                                                                                           N67
D01OH      Barclay, Kendall                        AUSA01012009 8FF  252 31 UN0807122017   32.27S                     36.31S 0 0 7 5  0 27       5  02      NN99
G01OH          Barclay, Kendall                        1 3  25C                   36.31                                                        F             N54
D01OH      Barclay, Kendall                        AUSA01012009 8FF  501 41 UN0807122017   56.75S                     58.91S 0 0 5 5  0 13       6  01      NN99
D01OH      Bauer, Hailey                           AUSA0819200313FF  501 17 131407112017   36.03S   34.91S            35.74S 4 5 1 2  9 10  3.   4  0100    NN79
D3                                                                                                                                                           N67
D01OH      Bauer, Hailey                           AUSA0819200313FF  504 27 131407112017   43.36S   40.44S            40.30S 2 5 1 2  9  9  4.   3  0400    NN69
D01OH      Bauer, Hailey                           AUSA0819200313FF 1001 47 131407112017 1:18.57S 1:19.94S          1:19.55S 2 4 2 6  6  6  9.   6  0100    NN50
G01OH          Bauer, Hailey                           1 2  50C         1:19.94                                                                P             N73
G01OH          Bauer, Hailey                           1 2  50C   37.51 1:19.55                                                                F             N24
D01OH      Beck, Jade                              AUSA0310200611FF  251 15 111207112017   16.41S   16.95S                   5 5 0 0 18             01      NN96
D3                                                                                                                                                           N67
D01OH      Beck, Jade                              AUSA0310200611FF  501 45 111207112017   37.86S   38.37S                   6 6 0 0 22             01      NN07
D01OH      Beck, Jade                              AUSA0310200611FF  503 55 111207122017   58.13S                     48.67S 0 0 2 3  0 11  2.   1  0300    NN57
D01OH      Beck, Lily                              AUSA03052010 7FF  251 11 UN0807122017   34.83S                     34.50S 0 0 4 4  0 44       1  01      NN67
D3                                                                                                                                                           N67
D01OH      Beck, Lily                              AUSA03052010 7FF  252 31 UN0807122017   34.47S                        DQS 0 0 4 3  0  0       0  02      NN47
D01OH      Beck, Lily                              AUSA03052010 7FF  501 41 UN0807122017 1:28.46S                   1:32.14S 0 0 3 1  0 30       5  01      NN08
D01OH      Bonnell, Elizabeth                      AUSA0819200115FF  501 19 151807112017   31.45S   30.88S            31.38S 3 4 2 2  3  4 11.   4  0100    NN71
D3                                                                                                                                                           N67
D01OH      Bonnell, Elizabeth                      AUSA0819200115FF  502 39 151807112017   35.44S   34.80S            35.12S 2 3 2 4  2  3 12.   3  0200    NN71
D01OH      Bonnell, Elizabeth                      AUSA0819200115FF 1001 49 151807122017 1:09.02S                   1:10.19S 0 0 3 2  0  5 10.   4  0100    NN41
G01OH          Bonnell, Elizabeth                      1 2  50C   33.92 1:10.19                                                                F             N16
D01OH      Bonnell, Samantha                       AUSA1213200214FF  501 17 131407112017   38.39S   38.81S                   3 1 0 0 15             01      NN99
D3                                                                                                                                                           N67
D01OH      Bonnell, Samantha                       AUSA1213200214FF  502 37 131407112017   48.78S   46.88S                   2 1 0 0 14             02      NN00
D01OH      Bonnell, Samantha                       AUSA1213200214FF  503 57 131407122017   55.16S                     53.84S 0 0 1 4  0 12  1.   2  0300    NN40
D01OH      Carlier, Ava                            AUSA04142008 9FF  251 13 091007122017   18.66S                     18.14S 0 0 5 3  0  7  7.   1  0100    NN38
D3                                                                                                                                                           N67
D01OH      Carlier, Ava                            AUSA04142008 9FF  253 53 091007122017   22.64S                     21.51S 0 0 4 4  0  2 13.   2  0300    NN38
D01OH      Carlier, Ava                            AUSA04142008 9FF 1005 63 091007122017 1:49.93S                   1:48.19S 0 0 2 3  0  7  7.   2  0500    NN98
G01OH          Carlier, Ava                            1 2  50C   57.12 1:48.19                                                                F             N73
D01OH      Carlier, Lucy                           AUSA09022010 6FF  254 21 UN0807122017   31.85S                        DQS 0 0 2 3  0  0       0  04      NN78
D3                                                                                                                                                           N67
D01OH      Carlier, Lucy                           AUSA09022010 6FF  252 31 UN0807122017   39.51S                     29.01S 0 0 3 1  0  7  7.   1  0200    NN19
G01OH          Carlier, Lucy                           1 3  25C   23.75   36.35   29.01                                                        F             N54
D01OH      Carlier, Lucy                           AUSA09022010 6FF  251 11 UN0807122017   30.35S                     27.06S 0 0 7 2  0 16       1  01      NN88
G01OH          Carlier, Lucy                           1 4  25C                           27.06                                                F             N53
D01OH      Cooper, Anna                            AUSA08272008 8FF  251 11 UN0807122017   22.41S                     23.92S 0 011 5  0  8  5.   5  0100    NN88
D3                                                                                                                                                           N67
D01OH      Cooper, Anna                            AUSA08272008 8FF  254 21 UN0807122017   26.92S                     23.92S 0 0 3 1  0  5 10.   5  0400    NN88
D01OH      Cooper, Anna                            AUSA08272008 8FF  501 41 UN0807122017   50.32S                     51.44S 0 0 6 6  0  6  9.   5  0100    NN78
D01OH      Cooper, Daniel                          AUSA1230200412MM  251 16 111207112017   17.10S   16.01S            16.22S 4 4 2 2  3  3 12.   3  0100    NN99
D3                                                                                                                                                           N67
D01OH      Cooper, Daniel                          AUSA1230200412MM  501 46 111207112017   41.49S   39.17S            39.00S 4 5 1 4  8  9  4.   3  0100    NN10
D01OH      Cooper, Daniel                          AUSA1230200412MM  503 56 111207122017   56.48S                     51.19S 0 0 3 6  0 10  3.   4  0300    NN39
D01OH      Cooper, Sarah                           AUSA0912200610FF  251 13 091007122017   20.50S                     18.64S 0 0 5 1  0  9  4.   2  0100    NN88
D3                                                                                                                                                           N67
D01OH      Cooper, Sarah                           AUSA0912200610FF  252 33 091007122017   26.08S                     24.03S 0 0 4 1  0  6  9.   1  0200    NN88
G01OH          Cooper, Sarah                           1 2  25C           24.03                                                                F             N53
D01OH      Cooper, Sarah                           AUSA0912200610FF  501 43 091007122017   43.04S                     41.66S 0 0 5 6  0 12  1.   5  0100    NN98
D01OH      Cumming, Cameron                        AUSA0902200016FF  501 19 151807112017   35.21S   36.28S                   3 6 0 0 19             01      NN69
D3                                                                                                                                                           N67
D01OH      Cumming, Cameron                        AUSA0902200016FF  502 39 151807112017   44.13S   46.59S                   3 6 0 0 18             02      NN79
D01OH      Cumming, Cameron                        AUSA0902200016FF  503 59 151807122017   47.36S                     47.53S 0 0 3 1  0 13       5  03      NN89
D01OH      Cumming, Erin                           AUSA09302008 8FF  251 11 UN0807122017   35.83S                     36.49S 0 0 4 1  0 49       4  01      NN09
D3                                                                                                                                                           N67
D01OH      Cumming, Erin                           AUSA09302008 8FF  252 31 UN0807122017   39.09S                        DQS 0 0 3 4  0  0       0  02      NN88
G01OH          Cumming, Erin                           1 3  25C   29.36           37.20                                                        F             N04
D01OH      Cumming, Erin                           AUSA09302008 8FF  253 51 UN0807122017   38.81S                     32.25S 0 0 3 3  0 12  1.   1  0300    NN29
D01OH      Cumming, Jack                           AUSA0712200313MM  501 18 131407112017   35.77S   34.44S                   2 1 0 0 14             01      NN48
D3                                                                                                                                                           N67
D01OH      Cumming, Jack                           AUSA0712200313MM  502 38 131407112017   44.16S   43.63S            42.97S 3 5 1 4  8  9  4.   3  0200    NN79
D01OH      Cumming, Jack                           AUSA0712200313MM  503 58 131407122017   49.00S                     43.03S 0 0 2 2  0  6  9.   1  0300    NN88
D01OH      Dunlap, Kiley                           AUSA0315200512FF  251 15 111207112017   15.26S   15.01S            15.39S 7 2 2 6  6  6  9.   6  0100    NN79
D3                                                                                                                                                           N67
D01OH      Dunlap, Kiley                           AUSA0315200512FF  504 25 111207112017   38.02S   36.96S            37.25S 3 4 2 1  6  5 10.   5  0400    NN89
D01OH      Dunlap, Kiley                           AUSA0315200512FF  502 35 111207112017   41.93S   41.14S            42.36S 2 4 2 6  7  6  9.   6  0200    NN79
D01OH      Dunlap, McKenzie                        AUSA0115200710FF  251 13 091007122017   16.06S                     15.94S 0 0 6 3  0  1 16.   1  0100    NN99
D3                                                                                                                                                           N67
D01OH      Dunlap, McKenzie                        AUSA0115200710FF  254 23 091007122017   17.80S                     17.59S 0 0 4 4  0  1 16.   1  0400    NN00
D01OH      Dunlap, McKenzie                        AUSA0115200710FF 1005 63 091007122017 1:32.43S                   1:35.06S 0 0 3 4  0  2 13.   2  0500    NN40
G01OH          Dunlap, McKenzie                        1 2  50C   42.22 1:35.06                                                                F             N25
D01OH      Dupre, Cora                             AUSA0507200116FF  501 19 151807112017   27.72S   26.52S            26.10S 5 3 2 3  1  1 16.   1  0100    NN98
D3                                                                                                                                                           N67
D01OH      Dupre, Cora                             AUSA0507200116FF  504 29 151807112017   29.74S   29.93S            28.83S 3 3 2 3  1  1 16.   1  0400    NN19
D01OH      Dupre, Cora                             AUSA0507200116FF 1005 69 151807122017 1:08.25S                   1:05.23S 0 0 3 3  0  1 16.   1  0500    NN78
G01OH          Dupre, Cora                             1 2  50C   30.59 1:05.23                                                                F             N43
D01OH      Flerage, Samantha                       AUSA0816200313FF  501 17 131407112017   34.34S   39.52S                   3 2 0 0 19             01      NN89
D3                                                                                                                                                           N67
D01OH      Flerage, Samantha                       AUSA0816200313FF  504 27 131407112017   43.52S      NSS                   1 5 0 0  0             04      NN79
D01OH      Flerage, Samantha                       AUSA0816200313FF  502 37 131407112017   42.01S   41.04S            40.18S 4 2 1 3  7  7  7.   1  0200    NN01
D01OH      Heis, Corbin                            AUSA07272010 6MM  251 12 UN0807122017   26.56S                     25.19S 0 0 7 6  0 17       4  01      NN68
D3                                                                                                                                                           N67
D01OH      Heis, Corbin                            AUSA07272010 6MM  252 32 UN0807122017   28.50S                     32.79S 0 0 7 3  0 16       4  02      NN68
D01OH      Heis, Corbin                            AUSA07272010 6MM  501 42 UN0807122017 1:04.84S                   1:01.85S 0 0 4 5  0 15       2  01      NN98
D01OH      Heis, Quinn                             AUSA10082007 9MM  251 14 091007122017   22.25S                     21.07S 0 0 3 3  0 20       1  01      NN87
D3                                                                                                                                                           N67
D01OH      Heis, Quinn                             AUSA10082007 9MM  252 34 091007122017   28.03S                     27.08S 0 0 3 3  0 19       2  02      NN97
D01OH      Heis, Quinn                             AUSA10082007 9MM  253 54 091007122017   30.16S                     35.41S 0 0 3 5  0 21       5  03      NN97
D01OH      Henkel, Maggy                           AUSA0825200511FF  501 45 111207112017   39.27S   38.27S                   5 4 0 0 21             01      NN48
D3                                                                                                                                                           N67
D01OH      Henkel, Maggy                           AUSA0825200511FF  503 55 111207122017   47.78S                     47.34S 0 0 5 3  0  8  5.   2  0300    NN98
D01OH      Henkel, Maggy                           AUSA0825200511FF 1005 65 111207122017 1:47.05S                   1:39.28S 0 0 1 3  0 14       2  05      NN19
G01OH          Henkel, Maggy                           1 2  50C   49.61 1:39.28                                                                F             N24
D01OH      Kaplan, Lily                            AUSA06262008 8FF  251 11 UN0807122017   21.93S                     21.06S 0 011 4  0  1 16.   1  0100    NN98
D3                                                                                                                                                           N67
D01OH      Kaplan, Lily                            AUSA06262008 8FF  501 41 UN0807122017   58.87S                     50.67S 0 0 5 6  0  5 10.   1  0100    NN98
D01OH      Kaplan, Lily                            AUSA06262008 8FF  253 51 UN0807122017   27.53S                     26.63S 0 0 6 2  0  2 13.   2  0300    NN98
D01OH      Kaplan, Perry                           AUSA12142009 7MM  254 22 UN0807122017   25.76S                     23.46S 0 0 3 3  0  1 16.   1  0400    NN49
D3                                                                                                                                                           N67
D01OH      Kaplan, Perry                           AUSA12142009 7MM  501 42 UN0807122017   45.31S                     42.55S 0 0 6 2  0  2 13.   2  0100    NN39
D01OH      Kaplan, Perry                           AUSA12142009 7MM  253 52 UN0807122017   29.53S                     26.40S 0 0 4 4  0  2 13.   2  0300    NN49
D01OH      Kessel, Elizabeth                       AUSA04022008 9FF  251 13 091007122017   26.41S                     25.84S 0 0 1 3  0 29       1  01      NN10
D3                                                                                                                                                           N67
D01OH      Kessel, Elizabeth                       AUSA04022008 9FF  254 23 091007122017   36.23S                     31.83S 0 0 1 4  0 22       4  04      NN10
D01OH      Kessel, Elizabeth                       AUSA04022008 9FF  252 33 091007122017   31.15S                     35.67S 0 0 3 6  0 25       6  02      NN10
G01OH          Kessel, Elizabeth                       1 2  25C           35.67                                                                F             N15
D01OH      Kessel, Nick                            AUSA0622200610MM 1005 64 091007122017 1:56.65S                   2:02.94S 0 0 2 3  0 10  3.   3  0500    NN19
D3                                                                                                                                                           N67
G01OH          Kessel, Nick                            1 2  50C 1:00.83 2:02.94                                                                F             N04
D01OH      Kessel, Nick                            AUSA0622200610MM  251 14 091007122017   21.57S                     22.52S 0 0 4 6  0 25       6  01      NN28
D01OH      Kessel, Nick                            AUSA0622200610MM  501 44 091007122017   48.29S                     48.85S 0 0 3 2  0 18       3  01      NN38
D01OH      Kessler, Claire                         AUSA09012007 9FF  251 13 091007122017   20.19S                     20.39S 0 0 5 5  0 13       6  01      NN39
D3                                                                                                                                                           N67
D01OH      Kessler, Claire                         AUSA09012007 9FF  253 53 091007122017   25.56S                     25.87S 0 0 3 4  0 11  2.   5  0300    NN79
D01OH      Kessler, Claire                         AUSA09012007 9FF 1005 63 091007122017 1:59.09S                   1:54.65S 0 0 1 3  0  9  4.   1  0500    NN20
G01OH          Kessler, Claire                         1 2  50C   54.70 1:54.65                                                                F             N05
D01OH      Kohlman, Sadie                          AUSA1029200511FF  251 15 111207112017   19.09S   18.51S                   3 1 0 0 27             01      NN78
D3                                                                                                                                                           N67
D01OH      Kohlman, Sadie                          AUSA1029200511FF  501 45 111207112017   44.54S   44.46S                   3 4 0 0 34             01      NN78
D01OH      Kohlman, Sadie                          AUSA1029200511FF  503 55 111207122017   48.57S                     47.44S 0 0 5 4  0  9  4.   3  0300    NN29
D01OH      Letton, Tanner                          AUSA1219200511MM  251 16 111207112017   17.47S   16.40S            16.63S 2 4 2 5  4  4 11.   4  0100    NN30
D3                                                                                                                                                           N67
D01OH      Letton, Tanner                          AUSA1219200511MM  501 46 111207112017   39.23S   39.29S            38.79S 3 2 1 2  9  8  5.   2  0100    NN40
D01OH      Letton, Tanner                          AUSA1219200511MM  503 56 111207122017   51.59S                     50.89S 0 0 3 4  0  9  4.   3  0300    NN59
D01OH      McManus, Julia                          AUSA0420200512FF  502 35 111207112017 1:01.83S   59.63S                   1 3 0 0 22             02      NN88
D3                                                                                                                                                           N67
D01OH      McManus, Julia                          AUSA0420200512FF  501 45 111207112017   50.18S   55.12S                   2 4 0 0 42             01      NN68
D01OH      McManus, Julia                          AUSA0420200512FF  503 55 111207122017   55.90S                     55.27S 0 0 3 2  0 23       5  03      NN88
D01OH      McManus, Walker                         AUSA1102199818MM  501 20 151807112017   29.27S   28.73S                   4 1 0 0 14             01      NN39
D3                                                                                                                                                           N67
D01OH      McManus, Walker                         AUSA1102199818MM  502 40 151807112017   38.21S   40.38S                   1 5 0 0 14             02      NN29
D01OH      McManus, Walker                         AUSA1102199818MM  503 60 151807122017                              36.63S 0 0 1 6  0  4 11.   1  0300    NN09
D01OH      Nerl, Sophie                            AUSA1208200412FF  251 15 111207112017   16.48S   16.12S                   6 1 0 0 13             01      NN08
D3                                                                                                                                                           N67
D01OH      Nerl, Sophie                            AUSA1208200412FF  504 25 111207112017   44.57S      DQS                   2 5 0 0  0             04      NN97
D01OH      Nerl, Sophie                            AUSA1208200412FF  501 45 111207112017   37.38S   37.49S                   8 6 0 0 19             01      NN28
D01OH      O'Brien, Cullen                         AUSA09022008 8MM  251 12 UN0807122017   20.19S                     19.94S 0 0 9 4  0  4 11.   4  0100    NN69
D3                                                                                                                                                           N67
D01OH      O'Brien, Cullen                         AUSA09022008 8MM  252 32 UN0807122017   29.21S                     30.91S 0 0 7 4  0 11  2.   3  0200    NN59
D01OH      O'Brien, Cullen                         AUSA09022008 8MM  501 42 UN0807122017   44.96S                     44.28S 0 0 6 4  0  4 11.   3  0100    NN69
D01OH      Peirol, Jack                            AUSA1030200412MM  251 16 111207112017   17.38S      NSS                   3 4 0 0  0             01      NN97
D3                                                                                                                                                           N67
D01OH      Peirol, Jack                            AUSA1030200412MM  502 36 111207112017   54.47S      NSS                   3 2 0 0  0             02      NN97
D01OH      Peirol, Jack                            AUSA1030200412MM  501 46 111207112017   43.43S      NSS                   4 1 0 0  0             01      NN87
D01OH      Peirol, Lauren                          AUSA07282007 9FF  251 13 091007122017   24.63S                     23.60S 0 0 2 2  0 26       2  01      NN09
D3                                                                                                                                                           N67
D01OH      Peirol, Lauren                          AUSA07282007 9FF  252 33 091007122017   27.81S                     27.45S 0 0 3 4  0 19       5  02      NN19
G01OH          Peirol, Lauren                          1 2  25C           27.45                                                                F             N04
D01OH      Peirol, Lauren                          AUSA07282007 9FF  501 43 091007122017   59.29S                     56.39S 0 0 2 2  0 28       3  01      NN19
D01OH      Peirol, Will                            AUSA07212009 7MM  251 12 UN0807122017   36.41S                     29.71S 0 0 2 3  0 29       1  01      NN78
D3                                                                                                                                                           N67
D01OH      Peirol, Will                            AUSA07212009 7MM  252 32 UN0807122017   30.29S                        DQS 0 0 7 1  0  0       0  02      NN58
D01OH      Peirol, Will                            AUSA07212009 7MM  501 42 UN0807122017 1:48.77S                   1:14.86S 0 0 2 2  0 28       3  01      NN29
D01OH      Perry, Kent                             AUSA0101200116MM  504 30 151807112017   29.84S      NSS                   3 4 0 0  0             04      NN87
D3                                                                                                                                                           N67
D01OH      Perry, Kent                             AUSA0101200116MM 1005 70 151807122017 1:10.73S                        NSS 0 0 4 1  0  0       0  05      NN28
G01OH          Perry, Kent                             1 2  50C                                                                                F             N42
D01OH      Potter, Jack                            AUSA10162009 7MM  251 12 UN0807122017   25.11S                     22.82S 0 0 8 1  0 10  3.   3  0100    NN98
D3                                                                                                                                                           N67
D01OH      Potter, Jack                            AUSA10162009 7MM  252 32 UN0807122017   29.37S                     29.82S 0 0 7 2  0  8  5.   1  0200    NN98
D01OH      Potter, Jack                            AUSA10162009 7MM  501 42 UN0807122017 1:01.38S                     57.39S 0 0 4 3  0 13       1  01      NN88
D01OH      Rooney, Claire                          A            5FF  251 11 UN0807122017                              31.59S 0 0 1 2  0 34       3  01      NN07
D3                                                                                                                                                           N67
D01OH      Rooney, Lucy                            AUSA09172007 9FF  251 13 091007122017   16.49S                     16.29S 0 0 6 4  0  3 12.   3  0100    NN88
D3                                                                                                                                                           N67
D01OH      Rooney, Lucy                            AUSA09172007 9FF  252 33 091007122017   20.06S                     20.27S 0 0 5 3  0  2 13.   2  0200    NN78
D01OH      Rooney, Lucy                            AUSA09172007 9FF  501 43 091007122017   36.06S                     35.65S 0 0 6 4  0  2 13.   2  0100    NN78
D01OH      Rucker, Nick                            AUSA07102007 9MM  251 14 091007122017   17.04S                     17.38S 0 0 6 4  0  4 11.   3  0100    NN68
D3                                                                                                                                                           N67
D01OH      Rucker, Nick                            AUSA07102007 9MM  252 34 091007122017   22.49S                     21.58S 0 0 6 1  0  3 12.   3  0200    NN68
D01OH      Rucker, Nick                            AUSA07102007 9MM  253 54 091007122017   22.79S                     22.29S 0 0 5 4  0  2 13.   2  0300    NN68
D01OH      Sabath, Grace                           AUSA1006200511FF  251 15 111207112017   19.42S   19.09S                   2 5 0 0 29             01      NN28
D3                                                                                                                                                           N67
D01OH      Sabath, Grace                           AUSA1006200511FF  502 35 111207112017   49.83S   52.57S                   4 6 0 0 18             02      NN38
D01OH      Sabath, Grace                           AUSA1006200511FF  503 55 111207122017   54.70S                     54.51S 0 0 3 4  0 20       3  03      NN38
D01OH      Senes, Skylar                           AUSA0314200512FF  251 15 111207112017   17.69S   17.36S                   4 1 0 0 22             01      NN58
D3                                                                                                                                                           N67
D01OH      Senes, Skylar                           AUSA0314200512FF  504 25 111207112017   51.16S   46.63S                   3 6 0 0 15             04      NN58
D01OH      Senes, Skylar                           AUSA0314200512FF  501 45 111207112017   40.40S   41.34S                   5 5 0 0 28             01      NN58
D01OH      Shelton, Beckley                        AUSA07202007 9FF  251 13 091007122017   21.38S                     22.24S 0 0 4 2  0 21       6  01      NN79
D3                                                                                                                                                           N67
D01OH      Shelton, Beckley                        AUSA07202007 9FF  252 33 091007122017   24.88S                     26.34S 0 0 5 6  0 11  2.   6  0200    NN10
D01OH      Shelton, Beckley                        AUSA07202007 9FF  253 53 091007122017   25.91S                     24.86S 0 0 3 2  0  8  5.   2  0300    NN00
D01OH      Stimson, Beckett                        AUSA01012008 9MM  251 14 091007122017   22.74S                     21.72S 0 0 3 2  0 22       2  01      NN89
D3                                                                                                                                                           N67
D01OH      Stimson, Beckett                        AUSA01012008 9MM  501 44 091007122017   55.71S                     47.54S 0 0 2 4  0 16       1  01      NN99
D01OH      Stimson, Beckett                        AUSA01012008 9MM  253 54 091007122017   25.53S                     23.97S 0 0 4 3  0  4 11.   1  0300    NN20
D01OH      Stimson, Henry                          AUSA0626200610MM  251 14 091007122017   19.66S                     14.26S 0 0 4 3  0  1 16.   1  0100    NN69
D3                                                                                                                                                           N67
D01OH      Stimson, Henry                          AUSA0626200610MM  254 24 091007122017   16.66S                     15.57S 0 0 4 4  0  2 13.   2  0400    NN79
D01OH      Stimson, Henry                          AUSA0626200610MM 1005 64 091007122017 1:22.64S                   1:20.49S 0 0 3 3  0  1 16.   1  0500    NN20
G01OH          Stimson, Henry                          1 2  50C   37.21 1:20.49                                                                F             N84
D01OH      Walter, AJ                              AUSA0101200017MM  501 20 151807112017   28.41S   28.12S            27.88S 5 5 1 4  8  8  5.   2  0100    NN48
D3                                                                                                                                                           N67
D01OH      Walter, AJ                              AUSA0101200017MM  502 40 151807112017   33.10S   32.51S            31.17S 3 4 2 5  4  4 11.   4  0200    NN38
D01OH      Walter, AJ                              AUSA0101200017MM 1005 70 151807122017 1:14.29S                   1:10.44S 0 0 3 3  0  7  7.   2  0500    NN18
G01OH          Walter, AJ                              1 2  50C   32.09 1:10.44                                                                F             N82
D01OH      Werner, Rex                             AUSA07122010 6MM  251 12 UN0807122017   29.11S                     27.14S 0 0 6 5  0 21       3  01      NN28
D3                                                                                                                                                           N67
D01OH      Werner, Rex                             AUSA07122010 6MM  253 52 UN0807122017   34.40S                     32.09S 0 0 4 6  0  6  9.   4  0300    NN68
D01OH      Winders, Grace                          AUSA02082008 9FF  251 13 091007122017   21.51S                     21.16S 0 0 4 1  0 19       5  01      NN88
D3                                                                                                                                                           N67
D01OH      Winders, Grace                          AUSA02082008 9FF  252 33 091007122017   25.49S                     26.38S 0 0 4 3  0 13       5  02      NN09
G01OH          Winders, Grace                          1 2  25C    7.80   26.38                                                                F             N34
D01OH      Winders, Grace                          AUSA02082008 9FF  501 43 091007122017   39.20S                     51.58S 0 0 6 6  0 27       6  01      NN09
D01OH      Winders, Maddy                          AUSA08242009 7FF  251 11 UN0807122017   29.54S                     29.66S 0 0 8 1  0 29       4  01      NN49
D3                                                                                                                                                           N67
G01OH          Winders, Maddy                          1 2  25C           29.66                                                                F             N04
D01OH      Winders, Maddy                          AUSA08242009 7FF  252 31 UN0807122017   32.54S                     33.79S 0 0 7 6  0 19       4  02      NN49
G01OH          Winders, Maddy                          1 3  25C                   33.79                                                        F             N04
D01OH      Winders, Maddy                          AUSA08242009 7FF  253 51 UN0807122017   39.08S                        DQS 0 0 3 4  0  0       0  03      NN29
D01OH      Young, Jacqueline                       AUSA0504200413FF  501 17 131407112017   32.13S      NSS                   2 3 0 0  0             01      NN99
D3                                                                                                                                                           N67
D01OH      Young, Jacqueline                       AUSA0504200413FF  503 57 131407122017   39.21S                        NSS 0 0 3 3  0  0       0  03      NN10
D01OH      Young, Jacqueline                       AUSA0504200413FF 1005 67 131407122017 1:16.33S                        NSS 0 0 2 3  0  0       0  05      NN40
G01OH          Young, Jacqueline                       1 2  50C                                                                                F             N64
E01OH      AOHTP    F 1007  1 UN08  007122017 2:00.70S                   1:59.45S     1 5     5 20.                                            5N   0500X    N15
F01OH          OHTP  AAlbrecht, Mika                          USA100820097 F  1                                                                              N55
G01OH          Albrecht, Mika                          1 4  25C                                                                                F             N33
F01OH          OHTP  AKaplan, Lily                            USA062620088 F  2                                                                              N94
G01OH          Kaplan, Lily                            1 4  25C 1:03.21                                                                        F             N33
F01OH          OHTP  ACooper, Anna                            USA082720088 F  3                                                                              N94
G01OH          Cooper, Anna                            1 4  25C                                                                                F             N62
F01OH          OHTP  ABarclay, Kendall                        USA010120098 F  4                                                                              N36
G01OH          Barclay, Kendall                        1 4  25C 1:59.45                                                                        F             N84
E01OH      AOHTP    M 1007  2 UN08  007122017 1:42.78S                   1:42.49S     1 3     1 32.                                            1N   0500X    N25
F01OH          OHTP  APeirol, Will                            USA072120097 M  1                                                                              N15
G01OH          Peirol, Will                            1 4  25C                                                                                F             N72
F01OH          OHTP  AAlbrecht, William                       USA100820097 M  2                                                                              N86
G01OH          Albrecht, William                       1 4  25C   58.48                                                                        F             N05
F01OH          OHTP  AKaplan, Perry                           USA121420097 M  3                                                                              N45
G01OH          Kaplan, Perry                           1 4  25C                                                                                F             N13
F01OH          OHTP  AO'Brien, Cullen                         USA090220088 M  4                                                                              N65
G01OH          O'Brien, Cullen                         1 4  25C 1:42.49                                                                        F             N04
E01OH      AOHTP    F 1007  3 0910  007122017 1:26.91S                   1:22.49S     1 4     2 26.                                            2N   0500X    N84
F01OH          OHTP  AShelton, Beckley                        USA072020079 F  1                                                                              N56
G01OH          Shelton, Beckley                        1 4  25C                                                                                F             N24
F01OH          OHTP  ACarlier, Ava                            USA041420089 F  2                                                                              N84
G01OH          Carlier, Ava                            1 4  25C   47.81                                                                        F             N03
F01OH          OHTP  ADunlap, McKenzie                        USA0115200710F  3                                                                              N46
G01OH          Dunlap, McKenzie                        1 4  25C                                                                                F             N04
F01OH          OHTP  ARooney, Lucy                            USA091720079 F  4                                                                              N25
G01OH          Rooney, Lucy                            1 4  25C 1:22.49                                                                        F             N63
E01OH      AOHTP    M 1007  4 0910  007122017 1:27.82S                   1:23.97S     1 5     3 24.                                            3N   0500X    N94
F01OH          OHTP  ARucker, Nick                            USA071020079 M  1                                                                              N94
G01OH          Rucker, Nick                            1 4  25C                                                                                F             N62
F01OH          OHTP  AStimson, Beckett                        USA010120089 M  2                                                                              N66
G01OH          Stimson, Beckett                        1 4  25C   46.62                                                                        F             N84
F01OH          OHTP  AStimson, Henry                          USA0626200610M  3                                                                              N06
G01OH          Stimson, Henry                          1 4  25C                                                                                F             N73
F01OH          OHTP  AKessel, Nick                            USA0622200610M  4                                                                              N05
G01OH          Kessel, Nick                            1 4  25C 1:23.97                                                                        F             N43
E01OH      AOHTP    F 2007  5 1112  007122017 2:52.19S                   2:49.23S     1 1     5 20.                                            5N   0500X    N84
F01OH          OHTP  ANerl, Sophie                            USA1208200412F  1                                                                              N05
G01OH          Nerl, Sophie                            1 4  50C   46.41                                                                        F             N23
F01OH          OHTP  AHenkel, Maggy                           USA0825200511F  2                                                                              N35
G01OH          Henkel, Maggy                           1 4  50C 1:34.28                                                                        F             N73
F01OH          OHTP  ADunlap, Kiley                           USA0315200512F  3                                                                              N45
G01OH          Dunlap, Kiley                           1 4  50C 2:11.33                                                                        F             N73
F01OH          OHTP  ABeck, Jade                              USA0310200611F  4                                                                              N93
G01OH          Beck, Jade                              1 4  50C 2:49.23                                                                        F             N32
E01OH      AOHTP    M 2007  6 1112  007122017 4:17.48S                   3:29.88S     1 6     6 18.                                            6N   0500X    N05
F01OH          OHTP  APeirol, Jack                            USA1030200412M  1                                                                              N94
G01OH          Peirol, Jack                            1 4  50C   53.94                                                                        F             N13
F01OH          OHTP  ACooper, Daniel                          USA1230200412M  2                                                                              N75
G01OH          Cooper, Daniel                          1 4  50C 1:44.98                                                                        F             N14
F01OH          OHTP  ALetton, Tanner                          USA1219200511M  3                                                                              N95
G01OH          Letton, Tanner                          1 4  50C 2:31.80                                                                        F             N24
F01OH          OHTP  AHeis, Quinn                             USA100820079 M  4                                                                              N64
G01OH          Heis, Quinn                             1 4  50C 3:29.88                                                                        F             N13
E01OH      AOHTP    F 2007  7 1314  007122017 2:22.55S                   2:43.35S     1 3     5 20.                                            5N   0500X    N84
F01OH          OHTP  AFlerage, Samantha                       USA0816200313F  1                                                                              N86
G01OH          Flerage, Samantha                       1 4  50C   40.79                                                                        F             N94
F01OH          OHTP  AKohlman, Sadie                          USA1029200511F  2                                                                              N65
G01OH          Kohlman, Sadie                          1 4  50C 1:26.90                                                                        F             N04
F01OH          OHTP  ABauer, Hailey                           USA0819200313F  3                                                                              N35
G01OH          Bauer, Hailey                           1 4  50C 2:05.55                                                                        F             N73
F01OH          OHTP  ABonnell, Samantha                       USA1213200214F  4                                                                              N86
G01OH          Bonnell, Samantha                       1 4  50C 2:43.35                                                                        F             N25
E01OH      AOHTP    F 2007  9 1518  007122017 2:19.58S                   2:19.10S     1 4     2 26.                                            2N   0500X    N94
F01OH          OHTP  ABonnell, Elizabeth                      USA0819200115F  1                                                                              N37
G01OH          Bonnell, Elizabeth                      1 4  50C   34.98                                                                        F             N45
F01OH          OHTP  ADupre, Cora                             USA0507200116F  2                                                                              N64
G01OH          Dupre, Cora                             1 4  50C 1:08.57                                                                        F             N03
F01OH          OHTP  AForbes, Sarah                           USA1109200016F  3                                                                              N35
G01OH          Forbes, Sarah                           1 4  50C 1:44.37                                                                        F             N73
F01OH          OHTP  ACumming, Cameron                        USA0902200016F  4                                                                              N56
G01OH          Cumming, Cameron                        1 4  50C 2:19.10                                                                        F             N94
E01OH      AOHTP    M 2007 10 1518  007122017 2:14.24S                   2:48.20S     1 5     5 20.                                            5N   0500X    N94
F01OH          OHTP  ACumming, Jack                           USA0712200313M  1                                                                              N35
G01OH          Cumming, Jack                           1 4  50C   46.90                                                                        F             N43
F01OH          OHTP  AMcManus, Walker                         USA1102199818M  2                                                                              N26
G01OH          McManus, Walker                         1 4  50C 1:19.92                                                                        F             N44
F01OH          OHTP  APerry, Kent                             USA0101200116M  3                                                                              N74
G01OH          Perry, Kent                             1 4  50C 1:52.11                                                                        F             N13
F01OH          OHTP  AWalter, AJ                              USA0101200017M  4                                                                              N04
G01OH          Walter, AJ                              1 4  50C 2:48.20                                                                        F             N42
E01OH      AOHTP    F 1006 61 UN08  007122017 1:33.44S                   1:36.67S     1 3     3 24.                                            3N   0100X    N25
F01OH          OHTP  AKaplan, Lily                            USA062620088 F  1                                                                              N94
G01OH          Kaplan, Lily                            1 4  25C                                                                                F             N62
F01OH          OHTP  ABarclay, Kendall                        USA010120098 F  2                                                                              N36
G01OH          Barclay, Kendall                        1 4  25C   21.10                                                                        F             N54
F01OH          OHTP  AWinders, Maddy                          USA082420097 F  3                                                                              N85
G01OH          Winders, Maddy                          1 4  25C                                                                                F             N43
F01OH          OHTP  ACooper, Anna                            USA082720088 F  4                                                                              N94
G01OH          Cooper, Anna                            1 4  25C 1:36.67                                                                        F             N33
E01OH      AOHTP    M 1006 62 UN08  007122017 1:34.15S                   1:27.56S     1 4     2 26.                                            2N   0100X    N35
F01OH          OHTP  AKaplan, Perry                           USA121420097 M  1                                                                              N45
G01OH          Kaplan, Perry                           1 4  25C                                                                                F             N13
F01OH          OHTP  AAlbrecht, William                       USA100820097 M  2                                                                              N86
G01OH          Albrecht, William                       1 4  25C   42.03                                                                        F             N05
F01OH          OHTP  APotter, Jack                            USA101620097 M  3                                                                              N05
G01OH          Potter, Jack                            1 4  25C                                                                                F             N72
F01OH          OHTP  AO'Brien, Cullen                         USA090220088 M  4                                                                              N65
G01OH          O'Brien, Cullen                         1 4  25C 1:27.56                                                                        F             N04
E01OH      AOHTP    F 1006 71 0910  007122017 1:14.49S                   1:14.92S     1 3     3 24.                                            3N   0100X    N94
F01OH          OHTP  ARooney, Lucy                            USA091720079 F  1                                                                              N25
G01OH          Rooney, Lucy                            1 4  25C                                                                                F             N82
F01OH          OHTP  AKessler, Claire                         USA090120079 F  2                                                                              N16
G01OH          Kessler, Claire                         1 4  25C   39.59                                                                        F             N34
F01OH          OHTP  ACarlier, Ava                            USA041420089 F  3                                                                              N84
G01OH          Carlier, Ava                            1 4  25C                                                                                F             N52
F01OH          OHTP  ADunlap, McKenzie                        USA0115200710F  4                                                                              N46
G01OH          Dunlap, McKenzie                        1 4  25C 1:14.92                                                                        F             N84
E01OH      AOHTP    M 1006 72 0910  007122017 1:19.54S                   1:15.22S     1 1     4 22.                                            4N   0100X    N94
F01OH          OHTP  ARucker, Nick                            USA071020079 M  1                                                                              N94
G01OH          Rucker, Nick                            1 4  25C                                                                                F             N62
F01OH          OHTP  AKessel, Nick                            USA0622200610M  2                                                                              N05
G01OH          Kessel, Nick                            1 4  25C   39.75                                                                        F             N13
F01OH          OHTP  AStimson, Beckett                        USA010120089 M  3                                                                              N66
G01OH          Stimson, Beckett                        1 4  25C                                                                                F             N34
F01OH          OHTP  AStimson, Henry                          USA0626200610M  4                                                                              N16
G01OH          Stimson, Henry                          1 4  25C 1:15.22                                                                        F             N34
E01OH      AOHTP    F 2006 73 1112  007122017 2:37.26S                   2:32.56S     1 1     5 20.                                            5N   0100X    N94
F01OH          OHTP  ANerl, Sophie                            USA1208200412F  1                                                                              N05
G01OH          Nerl, Sophie                            1 4  50C   37.25                                                                        F             N23
F01OH          OHTP  AHenkel, Maggy                           USA0825200511F  2                                                                              N35
G01OH          Henkel, Maggy                           1 4  50C 1:17.21                                                                        F             N63
F01OH          OHTP  ABeck, Jade                              USA0310200611F  3                                                                              N93
G01OH          Beck, Jade                              1 4  50C 1:58.43                                                                        F             N32
F01OH          OHTP  ADunlap, Kiley                           USA0315200512F  4                                                                              N45
G01OH          Dunlap, Kiley                           1 4  50C 2:32.56                                                                        F             N83
E01OH      AOHTP    M 2006 74 1112  007122017 2:36.70S                        DQS     1 2     0                                                0N   01  X    N24
F01OH          OHTP  ALetton, Tanner                          USA1219200511M  1                                                                              N95
G01OH          Letton, Tanner                          1 4  50C   56.91                                                                        F             N14
F01OH          OHTP  APeirol, Jack                            USA1030200412M  2                                                                              N94
G01OH          Peirol, Jack                            1 4  50C 1:51.55                                                                        F             N33
F01OH          OHTP  AHeis, Quinn                             USA100820079 M  3                                                                              N64
G01OH          Heis, Quinn                             1 4  50C 2:16.46                                                                        F             N03
F01OH          OHTP  ACooper, Daniel                          USA1230200412M  4                                                                              N75
G01OH          Cooper, Daniel                          1 4  50C 3:09.44                                                                        F             N14
E01OH      AOHTP    F 2006 75 1314  007122017 2:08.74S                   2:42.84S     1 3     6 18.                                            6N   0100X    N05
F01OH          OHTP  ABonnell, Samantha                       USA1213200214F  1                                                                              N86
G01OH          Bonnell, Samantha                       1 4  50C   39.53                                                                        F             N05
F01OH          OHTP  ASabath, Grace                           USA1006200511F  2                                                                              N15
G01OH          Sabath, Grace                           1 4  50C 1:25.53                                                                        F             N53
F01OH          OHTP  AKohlman, Sadie                          USA1029200511F  3                                                                              N65
G01OH          Kohlman, Sadie                          1 4  50C 1:36.58                                                                        F             N04
F01OH          OHTP  ABauer, Hailey                           USA0819200313F  4                                                                              N35
G01OH          Bauer, Hailey                           1 4  50C 2:42.84                                                                        F             N73
E01OH      AOHTP    F 2006 77 1518  007122017 2:06.87S                   2:05.48S     1 3     3 24.                                            3N   0100X    N05
F01OH          OHTP  ACumming, Cameron                        USA0902200016F  1                                                                              N56
G01OH          Cumming, Cameron                        1 4  50C   31.79                                                                        F             N74
F01OH          OHTP  ABonnell, Elizabeth                      USA0819200115F  2                                                                              N37
G01OH          Bonnell, Elizabeth                      1 4  50C 1:07.92                                                                        F             N65
F01OH          OHTP  AForbes, Sarah                           USA1109200016F  3                                                                              N35
G01OH          Forbes, Sarah                           1 4  50C 1:39.65                                                                        F             N73
F01OH          OHTP  ADupre, Cora                             USA0507200116F  4                                                                              N64
G01OH          Dupre, Cora                             1 4  50C 2:05.48                                                                        F             N03
E01OH      AOHTP    M 2006 78 1518  007122017 1:58.90S                   2:36.92S     1 5     6 18.                                            6N   0100X    N15
F01OH          OHTP  APerry, Kent                             USA0101200116M  1                                                                              N74
G01OH          Perry, Kent                             1 4  50C   28.02                                                                        F             N92
F01OH          OHTP  AWalter, AJ                              USA0101200017M  2                                                                              N04
G01OH          Walter, AJ                              1 4  50C   58.04                                                                        F             N22
F01OH          OHTP  AMcManus, Walker                         USA1102199818M  3                                                                              N26
G01OH          McManus, Walker                         1 4  50C 1:59.50                                                                        F             N44
F01OH          OHTP  ACumming, Jack                           USA0712200313M  4                                                                              N35
G01OH          Cumming, Jack                           1 4  50C 2:36.92                                                                        F             N73
C11AD      ADTHSCTurpin Hills                  Thsc                                                                                        USA               N75
D01AD      Abernethy, Kylie                        AUSA0915200016FF  504 29 151807112017            33.19S            33.03S 3 6 2 1  5  5 10.   5  0400    NN10
D3                                                                                                                                                           N67
D01AD      Abernethy, Kylie                        AUSA0915200016FF  502 39 151807112017   33.57S   33.63S            33.45S 4 3 2 3  1  1 16.   1  0200    NN90
D01AD      Abernethy, Kylie                        AUSA0915200016FF 1005 69 151807122017                            1:13.99S 0 0 1 3  0  4 11.   1  0500    NN79
G01AD          Abernethy, Kylie                        1 2  50C   33.71 1:13.99                                                                F             N45
D01AD      Aman, Grace                             AUSA1026200511FF  504 25 111207112017   48.75S   47.19S                   2 1 0 0 17             04      NN47
D3                                                                                                                                                           N67
D01AD      Aman, Grace                             AUSA1026200511FF  503 55 111207122017   51.72S                     51.04S 0 0 4 2  0 16       4  03      NN57
D01AD      Aman, Grace                             AUSA1026200511FF 1005 65 111207122017                            1:39.07S 0 0 1 5  0 13       1  05      NN07
G01AD          Aman, Grace                             1 2  50C   46.15 1:39.07                                                                F             N23
D01AD      Bien, Liam                              AUSA0310200710MM  251 14 091007122017                              18.53S 0 0 1 1  0 12  0.   2  0150    NN86
D3                                                                                                                                                           N67
D01AD      Bien, Liam                              AUSA0310200710MM  252 34 091007122017   25.24S                     24.71S 0 0 5 1  0 11  2.   3  0200    NN57
D01AD      Bien, Liam                              AUSA0310200710MM  253 54 091007122017   21.71S                     21.89S 0 0 5 3  0  1 16.   1  0300    NN67
D01AD      Bien, Lillian                           AUSA1024200313FF  504 27 131407112017   45.43S   42.60S            43.04S 1 1 1 1 11 11  2.   5  0400    NN59
D3                                                                                                                                                           N67
D01AD      Bien, Lillian                           AUSA1024200313FF  503 57 131407122017   45.71S                     47.25S 0 0 2 2  0 10  3.   5  0300    NN78
D01AD      Bien, Lillian                           AUSA1024200313FF 1005 67 131407122017 1:37.32S                   1:36.83S 0 0 1 4  0  8  5.   3  0500    NN29
G01AD          Bien, Lillian                           1 2  50C   46.29 1:36.83                                                                F             N04
D01AD      Blazer, Beckett                         AUSA06132007 9MM  251 14 091007122017   19.12S                     18.48S 0 0 5 5  0 11  2.   4  0100    NN69
D3                                                                                                                                                           N67
D01AD      Blazer, Beckett                         AUSA06132007 9MM  252 34 091007122017                              26.52S 0 0 1 3  0 16       2  02      NN58
D01AD      Blazer, Beckett                         AUSA06132007 9MM  501 44 091007122017   46.26S                     42.32S 0 0 4 6  0  9  4.   3  0100    NN59
D01AD      Blazer, Brody                           AUSA0214200611MM  251 16 111207112017   19.01S   18.41S            18.89S 3 1 1 6 12 12  1.   6  0100    NN89
D3                                                                                                                                                           N67
D01AD      Blazer, Brody                           AUSA0214200611MM  501 46 111207112017   43.62S   45.80S                   3 1 0 0 17             01      NN48
D01AD      Blazer, Brody                           AUSA0214200611MM  503 56 111207122017                              58.32S 0 0 1 2  0 16       1  03      NN87
D01AD      Boberschmidt, Allie                     AUSA1108200412FF  502 35 111207112017   45.95S   46.98S                   4 1 0 0 14             02      NN60
D3                                                                                                                                                           N67
D01AD      Boberschmidt, Allie                     AUSA1108200412FF  501 45 111207112017            39.70S                   1 3 0 0 24             01      NN79
D01AD      Boberschmidt, Allie                     AUSA1108200412FF  503 55 111207122017   49.69S                     50.51S 0 0 4 3  0 14       2  03      NN70
D01AD      Combs, Aidan                            AUSA0909200313MM  504 28 131407112017   33.96S   37.66S            35.89S 2 4 2 6  6  6  9.   6  0400    NN39
D3                                                                                                                                                           N67
D01AD      Combs, Aidan                            AUSA0909200313MM  503 58 131407122017                              40.61S 0 0 1 2  0  4 11.   1  0300    NN67
D01AD      Combs, Aidan                            AUSA0909200313MM 1005 68 131407122017 1:18.22S                   1:17.18S 0 0 2 2  0  3 12.   3  0500    NN98
G01AD          Combs, Aidan                            1 2  50C   36.19 1:17.18                                                                F             N63
D01AD      Concannon, Annie                        AUSA0227200017FF  501 19 151807112017   31.30S   31.06S            31.24S 4 4 1 3  7  8  5.   2  0100    NN60
D3                                                                                                                                                           N67
D01AD      Concannon, Annie                        AUSA0227200017FF 1001 49 151807122017                            1:10.11S 0 0 1 2  0  4 11.   1  0100    NN49
G01AD          Concannon, Annie                        1 2  50C   33.48 1:10.11                                                                F             N15
D01AD      Concannon, Annie                        AUSA0227200017FF 1005 69 151807122017 1:23.98S                   1:21.94S 0 0 2 3  0  8  5.   1  0500    NN50
G01AD          Concannon, Annie                        1 2  50C   36.74 1:21.94                                                                F             N25
D01AD      Concannon, John                         AUSA0602200610MM  251 14 091007122017   19.02S                     19.96S 0 0 5 2  0 17       6  01      NN49
D3                                                                                                                                                           N67
D01AD      Concannon, John                         AUSA0602200610MM  254 24 091007122017   25.21S                     25.77S 0 0 2 2  0 17       4  04      NN39
D01AD      Concannon, John                         AUSA0602200610MM  501 44 091007122017                              44.51S 0 0 1 3  0 12  1.   3  0100    NN88
D01AD      Cosgrove, Jack                          AUSA08112008 8MM  254 22 UN0807122017   26.94S                     27.74S 0 0 3 4  0  4 11.   4  0400    NN69
D3                                                                                                                                                           N67
D01AD      Cosgrove, Jack                          AUSA08112008 8MM  501 42 UN0807122017   47.76S                     44.33S 0 0 6 1  0  5 10.   4  0100    NN69
D01AD      Cosgrove, Jack                          AUSA08112008 8MM  253 52 UN0807122017                                 DQS 0 0 1 2  0  0       0  03      NN38
D01AD      Cosgrove, Meg                           AUSA0603200511FF  504 25 111207112017            38.39S            38.84S 1 2 1 4  9  8  5.   2  0400    NN98
D3                                                                                                                                                           N67
D01AD      Cosgrove, Meg                           AUSA0603200511FF  502 35 111207112017   44.07S   42.04S            42.55S 4 5 1 4  9  8  5.   2  0200    NN69
D01AD      Cosgrove, Meg                           AUSA0603200511FF  501 45 111207112017            33.51S            32.61S 1 2 1 4  9  7  7.   1  0100    NN88
D01AD      Covington, Corinne                      AUSA04032010 7FF  251 11 UN0807122017   31.51S                     27.57S 0 0 6 4  0 19       1  01      NN80
D3                                                                                                                                                           N67
D01AD      Covington, Corinne                      AUSA04032010 7FF  252 31 UN0807122017   34.37S                     35.53S 0 0 5 6  0 24       6  02      NN90
D01AD      Covington, Corinne                      AUSA04032010 7FF  253 51 UN0807122017   41.35S                     33.77S 0 0 3 5  0 14       2  03      NN80
D01AD      Covington, Drew                         AUSA1211200313MM  501 18 131407112017   32.94S   33.54S            32.87S 1 2 1 6 12 10  3.   4  0100    NN70
D3                                                                                                                                                           N67
D01AD      Covington, Drew                         AUSA1211200313MM  504 28 131407112017   39.09S   38.56S            36.91S 2 5 1 3  7  7  7.   1  0400    NN70
D01AD      Covington, Drew                         AUSA1211200313MM  503 58 131407122017   40.75S                     40.24S 0 0 3 5  0  3 12.   3  0300    NN89
D01AD      Covington, Ty                           AUSA1102200511MM  504 26 111207112017 1:05.62S   58.63S            58.04S 1 1 1 5 10  9  4.   3  0400    NN10
D3                                                                                                                                                           N67
D01AD      Covington, Ty                           AUSA1102200511MM  502 36 111207112017            47.96S            46.26S 2 1 2 5  4  4 11.   4  0200    NN29
D01AD      Covington, Ty                           AUSA1102200511MM  501 46 111207112017            40.59S            40.14S 1 6 1 5 10 11  2.   5  0100    NN19
D01AD      Dehlinger, Clay                         AUSA0615200412MM  251 16 111207112017   14.87S   15.25S            14.57S 3 3 2 4  2  2 13.   2  0100    NN40
D3                                                                                                                                                           N67
D01AD      Dehlinger, Clay                         AUSA0615200412MM  501 46 111207112017   33.66S   34.21S            33.49S 3 4 2 1  5  5 10.   5  0100    NN40
D01AD      Dehlinger, Clay                         AUSA0615200412MM  503 56 111207122017   45.41S                     43.28S 0 0 4 4  0  3 12.   3  0300    NN69
D01AD      Dehlinger, Cole                         AUSA0720199818MM  501 20 151807112017   27.34S   25.99S               NSS 4 4 2 3  1  0       0  01      NN10
D3                                                                                                                                                           N67
D01AD      Dehlinger, Cole                         AUSA0720199818MM  504 30 151807112017   29.19S   28.83S            27.95S 2 3 2 2  3  2 13.   2  0400    NN60
D01AD      Dehlinger, Cole                         AUSA0720199818MM 1001 50 151807122017 1:01.52S                     58.16S 0 0 3 4  0  2 13.   2  0100    NN99
G01AD          Dehlinger, Cole                         1 2  50C   27.03   58.16                                                                F             N54
D01AD      Dehlinger, Evan                         AUSA0910199917MM  501 20 151807112017   27.34S   26.40S            26.23S 5 4 2 4  2  3 12.   3  0100    NN50
D3                                                                                                                                                           N67
D01AD      Dehlinger, Evan                         AUSA0910199917MM  502 40 151807112017   31.59S   30.19S            30.80S 2 3 2 4  2  3 12.   3  0200    NN50
D01AD      Dehlinger, Evan                         AUSA0910199917MM 1005 70 151807122017 1:10.62S                   1:06.98S 0 0 4 5  0  3 12.   3  0500    NN30
G01AD          Dehlinger, Evan                         1 2  50C   30.73 1:06.98                                                                F             N84
D01AD      Detwiler, Owen                          AUSA02022008 9MM  251 14 091007122017   23.38S                     24.46S 0 0 3 6  0 29       5  01      NN09
D3                                                                                                                                                           N67
D01AD      Detwiler, Owen                          AUSA02022008 9MM  252 34 091007122017   28.43S                     32.82S 0 0 3 4  0 28       6  02      NN09
D01AD      Detwiler, Owen                          AUSA02022008 9MM  501 44 091007122017 1:00.02S                     59.86S 0 0 2 5  0 26       5  01      NN29
D01AD      Detwiler, Sara                          AUSA0227200413FF  504 27 131407112017   44.10S   44.54S                   2 1 0 0 14             04      NN78
D3                                                                                                                                                           N67
D01AD      Detwiler, Sara                          AUSA0227200413FF  502 37 131407112017   45.56S   43.44S            44.68S 2 5 1 5 10 11  2.   5  0200    NN20
D01AD      Detwiler, Sara                          AUSA0227200413FF 1001 47 131407112017 1:26.31S 1:31.59S          1:23.69S 1 2 1 1 11  9  4.   3  0100    NN80
G01AD          Detwiler, Sara                          1 2  50C   41.20 1:31.59                                                                P             N54
G01AD          Detwiler, Sara                          1 2  50C   39.31 1:23.69                                                                F             N54
D01AD      Dornhaggen, Molly                       AUSA10222008 8FF  254 21 UN0807122017   23.65S                     23.39S 0 0 3 2  0  4 11.   4  0400    NN70
D3                                                                                                                                                           N67
D01AD      Dornhaggen, Molly                       AUSA10222008 8FF  501 41 UN0807122017   46.19S                     45.73S 0 0 6 2  0  3 12.   3  0100    NN70
D01AD      Dornhaggen, Molly                       AUSA10222008 8FF  253 51 UN0807122017   28.06S                     26.71S 0 0 6 5  0  4 11.   4  0300    NN70
D01AD      Dotzauer, Chase                         AUSA09232010 6MM  251 12 UN0807122017   25.83S                     23.04S 0 0 7 1  0 12  1.   2  0100    NN99
D3                                                                                                                                                           N67
D01AD      Dotzauer, Chase                         AUSA09232010 6MM  252 32 UN0807122017                              32.03S 0 0 1 4  0 13       1  02      NN88
D01AD      Dotzauer, Chase                         AUSA09232010 6MM  253 52 UN0807122017                                 DQS 0 0 1 6  0  0       0  03      NN78
D01AD      Dotzauer, Jackson                       AUSA0711200610MM  251 14 091007122017   16.97S                     16.13S 0 0 6 3  0  2 13.   1  0100    NN60
D3                                                                                                                                                           N67
D01AD      Dotzauer, Jackson                       AUSA0711200610MM  254 24 091007122017   20.54S                     21.20S 0 0 3 3  0  7  7.   1  0400    NN40
D01AD      Dotzauer, Jackson                       AUSA0711200610MM  253 54 091007122017                              26.98S 0 0 2 1  0 13       1  03      NN59
D01AD      Dotzauer, Joey                          AUSA12292008 8MM  254 22 UN0807122017   28.95S                     26.77S 0 0 3 1  0  3 12.   3  0400    NN99
D3                                                                                                                                                           N67
D01AD      Dotzauer, Joey                          AUSA12292008 8MM  252 32 UN0807122017   24.21S                     25.26S 0 0 8 4  0  3 12.   3  0200    NN89
D01AD      Dotzauer, Joey                          AUSA12292008 8MM  501 42 UN0807122017                              43.86S 0 0 2 1  0  3 12.   1  0100    NN09
D01AD      Dryer, Elli                             AUSA01012008 9FF  251 13 091007122017   22.45S                     18.07S 0 0 3 3  0  6  9.   1  0100    NN97
D3                                                                                                                                                           N67
D01AD      Dryer, Elli                             AUSA01012008 9FF  254 23 091007122017   26.03S                     20.85S 0 0 2 5  0  8  5.   1  0400    NN97
D01AD      Dryer, Elli                             AUSA01012008 9FF  252 33 091007122017   31.84S                     23.76S 0 0 2 4  0  5 10.   1  0200    NN08
G01AD          Dryer, Elli                             1 2  25C           23.76                                                                F             N72
D01AD      Dunn, Jack                              AUSA10062008 8MM  251 12 UN0807122017                                 NSS 0 0 1 2  0  0       0  01      NN76
D3                                                                                                                                                           N67
D01AD      Dunn, Jack                              AUSA10062008 8MM  254 22 UN0807122017   28.61S                        NSS 0 0 3 5  0  0       0  04      NN57
D01AD      Dunn, Jack                              AUSA10062008 8MM  252 32 UN0807122017                                 NSS 0 0 2 5  0  0       0  02      NN76
D01AD      Dunn, Patrick                           AUSA0425200710MM  251 14 091007122017   26.21S                        NSS 0 0 2 5  0  0       0  01      NN58
D3                                                                                                                                                           N67
D01AD      Dunn, Patrick                           AUSA0425200710MM  254 24 091007122017                                 NSS 0 0 1 2  0  0       0  04      NN77
D01AD      Dunn, Patrick                           AUSA0425200710MM  252 34 091007122017                                 NSS 0 0 1 4  0  0       0  02      NN77
D01AD      Frantz, Liliana                         AUSA08182010 6FF  251 11 UN0807122017   34.17S                     34.72S 0 0 5 6  0 45       5  01      NN69
D3                                                                                                                                                           N67
D01AD      Frantz, Logan                           AUSA08032007 9MM  251 14 091007122017   25.99S                     23.96S 0 0 2 2  0 27       3  01      NN78
D3                                                                                                                                                           N67
D01AD      Frantz, Logan                           AUSA08032007 9MM  252 34 091007122017                              31.60S 0 0 2 6  0 26       3  02      NN87
G01AD          Frantz, Logan                           1 4  25C   29.19                   31.60                                                F             N04
D01AD      Frantz, Logan                           AUSA08032007 9MM  501 44 091007122017 1:01.39S                     55.59S 0 0 2 1  0 25       4  01      NN88
D01AD      Frye, Ben                               AUSA09112008 8MM  251 12 UN0807122017   25.79S                     20.78S 0 0 7 2  0  5 10.   1  0100    NN67
D3                                                                                                                                                           N67
D01AD      Frye, Ben                               AUSA09112008 8MM  252 32 UN0807122017   26.61S                     25.70S 0 0 8 1  0  4 11.   4  0200    NN67
D01AD      Frye, Ben                               AUSA09112008 8MM  501 42 UN0807122017   48.48S                     47.28S 0 0 5 3  0  8  5.   1  0100    NN67
D01AD      Gibson, Stefan                          AUSA0421200710MM  254 24 091007122017   19.16S                     19.95S 0 0 4 6  0  6  9.   6  0400    NN39
D3                                                                                                                                                           N67
D01AD      Gibson, Stefan                          AUSA0421200710MM  501 44 091007122017   39.38S                     38.44S 0 0 5 2  0  3 12.   3  0100    NN39
D01AD      Gibson, Stefan                          AUSA0421200710MM 1005 64 091007122017 1:40.68S                   1:39.00S 0 0 3 1  0  5 10.   5  0500    NN89
G01AD          Gibson, Stefan                          1 2  50C   44.90 1:39.00                                                                F             N54
D01AD      Gleason, Owen                           AUSA07272008 8MM  251 12 UN0807122017   22.49S                     22.07S 0 0 9 6  0  7  7.   6  0100    NN29
D3                                                                                                                                                           N67
D01AD      Gleason, Owen                           AUSA07272008 8MM  252 32 UN0807122017                              29.62S 0 0 3 6  0  7  7.   1  0200    NN58
G01AD          Gleason, Owen                           1 2  25C           29.62                                                                F             N53
D01AD      Gleason, Owen                           AUSA07272008 8MM  501 42 UN0807122017   52.06S                     48.53S 0 0 5 2  0  9  4.   2  0100    NN29
D01AD      Grisi, Cecilia                          AUSA05172010 7FF  251 11 UN0807122017   30.50S                     33.12S 0 0 7 5  0 39       5  01      NN09
D3                                                                                                                                                           N67
G01AD          Grisi, Cecilia                          1 4  25C                           33.12                                                F             N63
D01AD      Grisi, Cecilia                          AUSA05172010 7FF  254 21 UN0807122017                              34.34S 0 0 1 5  0 10  3.   2  0400    NN68
D01AD      Grisi, Cecilia                          AUSA05172010 7FF  252 31 UN0807122017                              37.06S 0 0 1 6  0 28       3  02      NN38
D01AD      Grisi, Chase                            AUSA08052007 9MM  251 14 091007122017   17.66S                     16.62S 0 0 6 2  0  3 12.   2  0100    NN48
D3                                                                                                                                                           N67
D01AD      Grisi, Chase                            AUSA08052007 9MM  254 24 091007122017   18.13S                     17.49S 0 0 4 5  0  3 12.   3  0400    NN58
D01AD      Grisi, Chase                            AUSA08052007 9MM 1005 64 091007122017 1:36.01S                   1:35.25S 0 0 3 5  0  2 13.   2  0500    NN98
G01AD          Grisi, Chase                            1 2  50C   44.13 1:35.25                                                                F             N63
D01AD      Grove, Harper                           AUSA03242009 8FF  251 11 UN0807122017   27.61S                        NSS 0 0 9 1  0  0       0  01      NN78
D3                                                                                                                                                           N67
D01AD      Grove, Harper                           AUSA03242009 8FF  252 31 UN0807122017   30.52S                        NSS 0 0 7 3  0  0       0  02      NN78
G01AD          Grove, Harper                           1 3  25C                                                                                F             N03
D01AD      Grove, Hollyn                           AUSA09192010 6FF  251 11 UN0807122017   26.22S                        NSS 0 010 5  0  0       0  01      NN98
D3                                                                                                                                                           N67
D01AD      Grove, Hollyn                           AUSA09192010 6FF  252 31 UN0807122017   33.66S                        NSS 0 0 6 6  0  0       0  02      NN98
D01AD      Guy, Tori                               AUSA0625200511FF  251 15 111207112017            17.84S                   1 2 0 0 24             01      NN16
D3                                                                                                                                                           N67
D01AD      Guy, Tori                               AUSA0625200511FF  502 35 111207112017   44.56S   47.81S                   3 5 0 0 16             02      NN07
D01AD      Guy, Tori                               AUSA0625200511FF  503 55 111207122017   53.24S                     51.93S 0 0 4 1  0 17       5  03      NN17
D01AD      Hartley, Ella                           AUSA0924200610FF  251 13 091007122017   25.65S                     27.00S 0 0 2 1  0 30       5  01      NN48
D3                                                                                                                                                           N67
D01AD      Hartley, Ella                           AUSA0924200610FF  252 33 091007122017   26.20S                     26.63S 0 0 4 6  0 14       6  02      NN58
G01AD          Hartley, Ella                           1 2  25C   22.30   26.63                                                                F             N83
D01AD      Hartley, Ella                           AUSA0924200610FF  253 53 091007122017                              32.12S 0 0 1 1  0 18       2  03      NN77
D01AD      Jones, Griffin                          AUSA0123200611MM  251 16 111207112017   19.05S   19.08S                   4 6 0 0 15             01      NN78
D3                                                                                                                                                           N67
D01AD      Jones, Griffin                          AUSA0123200611MM  502 36 111207112017          1:03.67S          1:00.64S 2 5 1 6 12 12  1.   6  0200    NN89
D01AD      Jones, Griffin                          AUSA0123200611MM  501 46 111207112017   45.75S   44.73S                   3 6 0 0 16             01      NN88
D01AD      Jones, Lucas                            AUSA0710200412MM  502 36 111207112017            44.95S            42.31S 3 1 2 4  2  2 13.   2  0200    NN58
D3                                                                                                                                                           N67
D01AD      Jones, Lucas                            AUSA0710200412MM  501 46 111207112017   35.91S   36.12S            36.72S 2 4 2 6  6  6  9.   6  0100    NN39
D01AD      Jones, Lucas                            AUSA0710200412MM 1005 66 111207122017 1:42.95S                   1:30.97S 0 0 2 4  0  6  9.   1  0500    NN19
G01AD          Jones, Lucas                            1 2  50C   43.13 1:30.97                                                                F             N83
D01AD      Jones, Parker                           AUSA08042007 9MM  251 14 091007122017   22.42S                     24.85S 0 0 3 4  0 30       6  01      NN68
D3                                                                                                                                                           N67
D01AD      Jones, Parker                           AUSA08042007 9MM  252 34 091007122017   30.00S                     31.25S 0 0 3 6  0 23       4  02      NN58
D01AD      Jones, Parker                           AUSA08042007 9MM  501 44 091007122017   54.02S                     52.15S 0 0 2 3  0 22       2  01      NN58
D01AD      Kiggins, Bella                          AUSA06182008 8FF  251 11 UN0807122017   26.23S                     23.34S 0 010 1  0  6  9.   1  0100    NN49
D3                                                                                                                                                           N67
D01AD      Kiggins, Bella                          AUSA06182008 8FF  252 31 UN0807122017   24.93S                     25.53S 0 0 8 3  0  2 13.   1  0200    NN49
D01AD      Kiggins, Bella                          AUSA06182008 8FF  253 51 UN0807122017                              37.55S 0 0 2 3  0 17       1  03      NN38
D01AD      Kiggins, Sam                            AUSA0707200610MM  252 34 091007122017   28.93S                     28.32S 0 0 3 5  0 20       3  02      NN28
D3                                                                                                                                                           N67
D01AD      Kiggins, Sam                            AUSA0707200610MM  501 44 091007122017   49.93S                     48.80S 0 0 3 1  0 17       2  01      NN28
D01AD      Kiggins, Sam                            AUSA0707200610MM  253 54 091007122017   26.25S                     25.03S 0 0 4 5  0  9  4.   4  0300    NN58
D01AD      Klimkowski, Andrew                      AUSA0708200610MM  251 14 091007122017   18.34S                     17.94S 0 0 5 4  0  8  5.   3  0100    NN01
D3                                                                                                                                                           N67
D01AD      Klimkowski, Andrew                      AUSA0708200610MM  252 34 091007122017   23.96S                     25.04S 0 0 5 4  0 12  1.   4  0200    NN01
D01AD      Klimkowski, Andrew                      AUSA0708200610MM 1005 64 091007122017                                 DQS 0 0 1 2  0  0       0  05      NN89
G01AD          Klimkowski, Andrew                      1 2  50C   56.07 1:55.31                                                                F             N26
D01AD      Klimkowski, Caroline                    AUSA09012008 8FF  251 11 UN0807122017                              25.66S 0 0 1 4  0 13       1  01      NN80
D3                                                                                                                                                           N67
D01AD      Klimkowski, Caroline                    AUSA09012008 8FF  252 31 UN0807122017                              41.47S 0 0 1 5  0 34       5  02      NN90
D01AD      Klimkowski, Caroline                    AUSA09012008 8FF  501 41 UN0807122017 1:06.74S                   1:07.10S 0 0 4 2  0 21       5  01      NN02
D01AD      Kuebler, Kai                            AUSA08082007 9MM  254 24 091007122017                              22.47S 0 0 1 3  0 13       1  04      NN37
D3                                                                                                                                                           N67
D01AD      Kuebler, Kai                            AUSA08082007 9MM  501 44 091007122017   45.88S                     42.58S 0 0 4 5  0 10  3.   4  0100    NN58
D01AD      Kuebler, Kai                            AUSA08082007 9MM  253 54 091007122017                              26.24S 0 0 1 5  0 11  2.   1  0300    NN67
D01AD      Kuebler, Tessa                          AUSA03162009 8FF  251 11 UN0807122017   29.71S                     33.10S 0 0 8 6  0 38       5  01      NN29
D3                                                                                                                                                           N67
G01AD          Kuebler, Tessa                          1 2  25C           33.10                                                                F             N83
D01AD      Kuebler, Tessa                          AUSA03162009 8FF  252 31 UN0807122017   33.02S                        DQS 0 0 6 4  0  0       0  02      NN09
D01AD      Kuebler, Tessa                          AUSA03162009 8FF  253 51 UN0807122017                                 DQS 0 0 2 4  0  0       0  03      NN38
D01AD      Lehman, Ben                             AUSA1011200610MM  252 34 091007122017                              24.56S 0 0 1 2  0  9  4.   1  0200    NN17
D3                                                                                                                                                           N67
D01AD      Lehman, Ben                             AUSA1011200610MM  501 44 091007122017                              49.51S 0 0 1 1  0 21       5  01      NN86
D01AD      Lehman, Ben                             AUSA1011200610MM  253 54 091007122017                                 DQS 0 0 2 6  0  0       0  03      NN76
D01AD      Lyons, Camryn                           AUSA09012008 8FF  251 11 UN0807122017   27.93S                     21.23S 0 0 8 3  0  2 13.   1  0100    NN39
D3                                                                                                                                                           N67
G01AD          Lyons, Camryn                           1 2  25C   18.91   21.23                                                                F             N14
D01AD      Lyons, Camryn                           AUSA09012008 8FF  252 31 UN0807122017                              25.61S 0 0 1 3  0  3 12.   2  0200    NN58
D01AD      Lyons, Camryn                           AUSA09012008 8FF  253 51 UN0807122017   32.39S                     27.35S 0 0 5 2  0  5 10.   1  0300    NN39
D01AD      Lyons, Gabriella                        AUSA03132009 8FF  251 11 UN0807122017   26.48S                     26.96S 0 0 9 3  0 15       3  01      NN00
D3                                                                                                                                                           N67
D01AD      Lyons, Gabriella                        AUSA03132009 8FF  252 31 UN0807122017   31.38S                     29.12S 0 0 7 2  0  8  5.   1  0200    NN20
G01AD          Lyons, Gabriella                        1 3  25C                   29.12                                                        F             N64
D01AD      Lyons, Gabriella                        AUSA03132009 8FF  253 51 UN0807122017   37.70S                     35.02S 0 0 4 1  0 16       3  03      NN99
D01AD      Lyons, Grace                            AUSA1113200412FF  502 35 111207112017   42.09S   41.38S            42.34S 4 2 1 3  8  7  7.   1  0200    NN29
D3                                                                                                                                                           N67
D01AD      Lyons, Grace                            AUSA1113200412FF  503 55 111207122017   46.27S                     46.47S 0 0 6 6  0  6  9.   5  0300    NN58
D01AD      Lyons, Grace                            AUSA1113200412FF 1005 65 111207122017 1:27.79S                   1:27.10S 0 0 2 4  0  9  4.   3  0500    NN98
G01AD          Lyons, Grace                            1 2  50C   41.03 1:27.10                                                                F             N73
D01AD      Lyons, Olivia                           AUSA0108200710FF  252 33 091007122017   22.66S                     23.02S 0 0 5 2  0  3 12.   3  0200    NN98
D3                                                                                                                                                           N67
D01AD      Lyons, Olivia                           AUSA0108200710FF  501 43 091007122017   43.79S                     41.20S 0 0 4 2  0 11  2.   2  0100    NN98
D01AD      Lyons, Olivia                           AUSA0108200710FF  253 53 091007122017   27.10S                     24.48S 0 0 3 1  0  7  7.   1  0300    NN98
D01AD      Lyons, Preston                          AUSA0504200611MM  251 16 111207112017   20.02S   21.08S                   1 4 0 0 19             01      NN09
D3                                                                                                                                                           N67
D01AD      Lyons, Preston                          AUSA0504200611MM  503 56 111207122017 1:08.29S                   1:01.02S 0 0 1 4  0 19       3  03      NN79
D01AD      Lyons, Preston                          AUSA0504200611MM 1005 66 111207122017 2:12.47S                   2:14.78S 0 0 1 4  0 12  1.   3  0500    NN10
G01AD          Lyons, Preston                          1 2  50C 1:06.53 2:14.78                                                                F             N15
D01AD      Massa, Noah                             AUSA04222009 8MM  251 12 UN0807122017   32.65S                     29.47S 0 0 4 4  0 27       1  01      NN08
D3                                                                                                                                                           N67
D01AD      Massa, Noah                             AUSA04222009 8MM  252 32 UN0807122017   39.62S                     41.00S 0 0 5 1  0 23       2  02      NN08
G01AD          Massa, Noah                             1 3  25C   16.59           41.00                                                        F             N03
D01AD      Massa, Noah                             AUSA04222009 8MM  501 42 UN0807122017 1:12.20S                   1:10.43S 0 0 3 5  0 26       5  01      NN48
D01AD      McCosh, Keely                           AUSA0809200412FF  504 25 111207112017   48.78S   43.94S                   4 6 0 0 14             04      NN28
D3                                                                                                                                                           N67
D01AD      McCosh, Keely                           AUSA0809200412FF  501 45 111207112017   35.90S   36.65S                   7 1 0 0 17             01      NN28
D01AD      McCosh, Keely                           AUSA0809200412FF  503 55 111207122017                              46.58S 0 0 1 4  0  7  7.   1  0300    NN97
D01AD      Meyer, Marc                             AUSA09292008 8MM  251 12 UN0807122017   32.64S                     29.58S 0 0 4 3  0 28       2  01      NN28
D3                                                                                                                                                           N67
D01AD      Meyer, Marc                             AUSA09292008 8MM  252 32 UN0807122017                              51.49S 0 0 2 2  0 36       4  02      NN47
D01AD      Meyer, Marc                             AUSA09292008 8MM  253 52 UN0807122017                              31.43S 0 0 1 4  0  5 10.   1  0300    NN77
D01AD      Meyers, Hayden                          AUSA0222200710MM  254 24 091007122017   18.71S                     18.49S 0 0 4 1  0  4 11.   4  0400    NN49
D3                                                                                                                                                           N67
D01AD      Meyers, Hayden                          AUSA0222200710MM  252 34 091007122017   21.93S                     21.65S 0 0 6 5  0  4 11.   4  0200    NN39
D01AD      Meyers, Hayden                          AUSA0222200710MM 1005 64 091007122017 1:34.92S                   1:35.97S 0 0 3 2  0  3 12.   3  0500    NN99
G01AD          Meyers, Hayden                          1 2  50C   46.09 1:35.97                                                                F             N64
D01AD      Meyers, Jack                            AUSA08262012 4MM  251 12 UN0807122017   36.05S                     33.72S 0 0 3 5  0 38       3  01      NN48
D3                                                                                                                                                           N67
D01AD      Meyers, Jack                            AUSA08262012 4MM  252 32 UN0807122017   39.55S                     44.37S 0 0 5 5  0 32       4  02      NN58
G01AD          Meyers, Jack                            1 3  25C                   44.37                                                        F             N03
D01AD      Meyers, Will                            AUSA1222200412MM  504 26 111207112017   37.59S      DQS                   1 4 0 0  0             04      NN08
D3                                                                                                                                                           N67
D01AD      Meyers, Will                            AUSA1222200412MM  502 36 111207112017   36.07S   35.95S            33.23S 3 3 2 3  1  1 16.   1  0200    NN49
D01AD      Meyers, Will                            AUSA1222200412MM 1005 66 111207122017 1:22.15S                   1:25.42S 0 0 3 2  0  4 11.   4  0500    NN19
G01AD          Meyers, Will                            1 2  50C   39.34 1:25.42                                                                F             N93
D01AD      Miller, Parker                          AUSA12302010 6MM  251 12 UN0807122017   35.21S                     33.80S 0 0 3 3  0 39       4  01      NN29
D3                                                                                                                                                           N67
D01AD      Miller, Parker                          AUSA12302010 6MM  252 32 UN0807122017 1:01.41S                        DQS 0 0 3 5  0  0       0  02      NN29
G01AD          Miller, Parker                          1 2  25C           57.14                                                                F             N83
D01AD      Miller, Reese                           AUSA09142009 7FF  251 11 UN0807122017   27.24S                     25.92S 0 0 9 4  0 14       2  01      NN88
D3                                                                                                                                                           N67
D01AD      Miller, Reese                           AUSA09142009 7FF  501 41 UN0807122017                              59.92S 0 0 2 4  0 14       2  01      NN18
D01AD      Miller, Reese                           AUSA09142009 7FF  253 51 UN0807122017   31.07S                     31.76S 0 0 5 3  0 10  3.   3  0300    NN19
D01AD      Norris, Lucia                           AUSA06192008 8FF  251 11 UN0807122017   27.53S                     30.09S 0 0 9 5  0 30       5  01      NN98
D3                                                                                                                                                           N67
D01AD      Norris, Lucia                           AUSA06192008 8FF  252 31 UN0807122017   29.18S                     27.92S 0 0 8 5  0  5 10.   3  0200    NN39
D01AD      Norris, Lucia                           AUSA06192008 8FF  501 41 UN0807122017 1:07.84S                   1:06.37S 0 0 4 5  0 18       2  01      NN49
D01AD      Norris, Rocco                           AUSA09262011 5MM  251 12 UN0807122017   40.65S                     34.22S 0 0 2 1  0 40       3  01      NN98
D3                                                                                                                                                           N67
D01AD      Norris, Rocco                           AUSA09262011 5MM  252 32 UN0807122017                              42.62S 0 0 1 2  0 26       2  02      NN28
D01AD      Phillips, Kaya                          AUSA1219200511FF  251 15 111207112017   16.72S   17.18S                   5 1 0 0 21             01      NN78
D3                                                                                                                                                           N67
D01AD      Phillips, Kaya                          AUSA1219200511FF  502 35 111207112017            52.24S                   1 5 0 0 17             02      NN97
D01AD      Phillips, Kaya                          AUSA1219200511FF  503 55 111207122017   50.80S                     50.60S 0 0 4 4  0 15       3  03      NN98
D01AD      Phillips, Kira                          AUSA0429200710FF  501 43 091007122017   44.46S                     42.52S 0 0 4 5  0 14       4  01      NN98
D3                                                                                                                                                           N67
D01AD      Phillips, Kira                          AUSA0429200710FF  253 53 091007122017   23.63S                     23.92S 0 0 4 5  0  5 10.   5  0300    NN39
D01AD      Phillips, Kira                          AUSA0429200710FF 1005 63 091007122017 1:53.47S                   1:47.96S 0 0 2 2  0  6  9.   1  0500    NN89
G01AD          Phillips, Kira                          1 2  50C   54.97 1:47.96                                                                F             N74
D01AD      Ragase, Truman                          AUSA08252008 8MM  254 22 UN0807122017                              33.89S 0 0 1 4  0  9  4.   2  0400    NN98
D3                                                                                                                                                           N67
D01AD      Ragase, Truman                          AUSA08252008 8MM  501 42 UN0807122017 1:01.28S                   1:03.57S 0 0 5 6  0 16       6  01      NN89
D01AD      Ragase, Truman                          AUSA08252008 8MM  253 52 UN0807122017   33.45S                     34.36S 0 0 4 1  0  9  4.   6  0300    NN69
D01AD      Reed, Mae                               AUSA1130200313FF  501 17 131407112017   39.06S   39.82S                   2 6 0 0 20             01      NN66
D3                                                                                                                                                           N67
D01AD      Reed, Mae                               AUSA1130200313FF  502 37 131407112017   48.01S      DQS                   3 1 0 0  0             02      NN46
D01AD      Reed, Mae                               AUSA1130200313FF 1001 47 131407112017          1:32.60S                   3 6 0 0 13             01      NN16
G01AD          Reed, Mae                               1 2  50C   43.49 1:32.60                                                                P             N42
D01AD      Reidy, Grace                            AUSA0930200115FF  501 19 151807112017   54.11S   57.72S                   1 4 0 0 24             01      NN97
D3                                                                                                                                                           N67
D01AD      Reidy, Grace                            AUSA0930200115FF  502 39 151807112017 1:11.56S 1:09.54S                   1 4 0 0 21             02      NN38
D01AD      Reidy, Grace                            AUSA0930200115FF  503 59 151807122017                                 NSS 0 0 1 3  0  0       0  03      NN27
D01AD      Riley, Sydney                           AUSA0918200511FF  251 15 111207112017   20.37S   19.18S                   2 1 0 0 30             01      NN58
D3                                                                                                                                                           N67
D01AD      Riley, Sydney                           AUSA0918200511FF  502 35 111207112017            57.96S                   1 6 0 0 21             02      NN87
D01AD      Riley, Sydney                           AUSA0918200511FF  501 45 111207112017   47.05S   47.34S                   3 6 0 0 37             01      NN68
D01AD      Rupp, Caroline                          AUSA0414200314FF  504 27 131407112017   33.29S   33.85S            32.97S 2 3 2 4  2  2 13.   2  0400    NN10
D3                                                                                                                                                           N67
D01AD      Rupp, Caroline                          AUSA0414200314FF  502 37 131407112017            36.03S            35.63S 1 5 2 2  3  3 12.   3  0200    NN39
D01AD      Rupp, Caroline                          AUSA0414200314FF 1005 67 131407122017 1:18.93S                   1:16.04S 0 0 2 5  0  2 13.   2  0500    NN99
G01AD          Rupp, Caroline                          1 2  50C   34.86 1:16.04                                                                F             N64
D01AD      Rupp, Hayley                            AUSA1113200016FF  504 29 151807112017   33.32S   32.29S            32.52S 3 4 2 5  4  4 11.   4  0400    NN49
D3                                                                                                                                                           N67
D01AD      Rupp, Hayley                            AUSA1113200016FF  502 39 151807112017            35.73S            35.66S 1 2 2 5  4  5 10.   5  0200    NN78
D01AD      Rupp, Hayley                            AUSA1113200016FF  503 59 151807122017   38.04S                     38.23S 0 0 4 4  0  2 13.   2  0300    NN78
D01AD      Rupp, Jessica                           AUSA0714199917FF  504 29 151807112017   31.09S   30.94S            30.31S 2 3 2 4  2  2 13.   2  0400    NN89
D3                                                                                                                                                           N67
D01AD      Rupp, Jessica                           AUSA0714199917FF 1001 49 151807122017 1:05.59S                   1:03.31S 0 0 3 3  0  1 16.   1  0100    NN69
G01AD          Rupp, Jessica                           1 2  50C   30.36 1:03.31                                                                F             N14
D01AD      Rupp, Jessica                           AUSA0714199917FF 1005 69 151807122017 1:13.07S                   1:11.39S 0 0 3 4  0  2 13.   2  0500    NN69
G01AD          Rupp, Jessica                           1 2  50C   32.62 1:11.39                                                                F             N24
D01AD      Schmittauer, Greta                      AUSA0418200512FF  251 15 111207112017   16.93S   16.91S                   5 6 0 0 17             01      NN40
D3                                                                                                                                                           N67
D01AD      Schmittauer, Greta                      AUSA0418200512FF  504 25 111207112017   44.19S   42.69S               DQS 3 5 1 6 13  0       0  04      NN31
D01AD      Schmittauer, Greta                      AUSA0418200512FF  501 45 111207112017   39.79S   38.97S                   5 2 0 0 23             01      NN40
D01AD      Schmittauer, Henry                      AUSA1213200610MM  251 14 091007122017   19.22S                     18.53S 0 0 5 6  0 12  0.   5  0150    NN01
D3                                                                                                                                                           N67
D01AD      Schmittauer, Henry                      AUSA1213200610MM  501 44 091007122017   40.83S                     41.41S 0 0 5 1  0  7  7.   4  0100    NN90
D01AD      Schmittauer, Henry                      AUSA1213200610MM  253 54 091007122017   25.53S                     24.69S 0 0 4 4  0  7  7.   2  0300    NN01
D01AD      Schmittauer, Will                       AUSA04032009 8MM  251 12 UN0807122017   22.52S                     22.83S 0 0 8 3  0 11  2.   4  0100    NN90
D3                                                                                                                                                           N67
D01AD      Schmittauer, Will                       AUSA04032009 8MM  254 22 UN0807122017   31.12S                     30.37S 0 0 3 6  0  5 10.   5  0400    NN90
D01AD      Schmittauer, Will                       AUSA04032009 8MM  253 52 UN0807122017   36.06S                     33.21S 0 0 3 5  0  7  7.   2  0300    NN80
D01AD      Shaw, Brennan                           AUSA1107200313MM  501 18 131407112017   32.99S   33.77S                   2 5 0 0 13             01      NN48
D3                                                                                                                                                           N67
D01AD      Shaw, Brennan                           AUSA1107200313MM  502 38 131407112017   44.59S   43.81S            42.15S 2 5 1 2  9  7  7.   1  0200    NN79
D01AD      Shaw, Brennan                           AUSA1107200313MM 1001 48 131407112017 1:18.28S 1:19.42S                   1 2 0 0 13             01      NN98
G01AD          Shaw, Brennan                           1 2  50C   36.29 1:19.42                                                                P             N24
D01AD      Shaw, Delaney                           AUSA12052007 9FF  251 13 091007122017   22.33S                     20.90S 0 0 4 6  0 17       3  01      NN48
D3                                                                                                                                                           N67
D01AD      Shaw, Delaney                           AUSA12052007 9FF  501 43 091007122017                              48.11S 0 0 1 2  0 22       1  01      NN67
D01AD      Shaw, Delaney                           AUSA12052007 9FF  253 53 091007122017   28.67S                        DQS 0 0 2 4  0  0       0  03      NN38
D01AD      Smith, Andrew                           AUSA0518200215MM  501 20 151807112017   52.97S   55.91S                   1 4 0 0 24             01      NN58
D3                                                                                                                                                           N67
D01AD      Smith, Andrew                           AUSA0518200215MM  502 40 151807112017 1:11.61S 1:13.16S                   1 6 0 0 18             02      NN98
D01AD      Smith, Nolan                            AUSA1225200412MM  251 16 111207112017   18.18S   18.12S            17.81S 4 5 1 1 11  9  4.   3  0100    NN49
D3                                                                                                                                                           N67
D01AD      Smith, Nolan                            AUSA1225200412MM  502 36 111207112017   50.66S   50.17S            46.77S 2 3 2 6  6  5 10.   5  0200    NN49
D01AD      Smith, Nolan                            AUSA1225200412MM  503 56 111207122017                              58.91S 0 0 1 1  0 17       2  03      NN57
D01AD      Smith, Sam                              AUSA0919200016MM  501 20 151807112017            30.37S                   1 2 0 0 17             01      NN56
D3                                                                                                                                                           N67
D01AD      Smith, Sam                              AUSA0919200016MM  504 30 151807112017            35.44S            34.76S 2 1 1 6 12 12  1.   6  0400    NN08
D01AD      Smith, Sam                              AUSA0919200016MM  503 60 151807122017                              41.55S 0 0 1 1  0 12  1.   3  0300    NN07
D01AD      Sutphin, Audrina                        AUSA09302008 8FF  254 21 UN0807122017   21.18S                     21.23S 0 0 3 3  0  1 16.   1  0400    NN40
D3                                                                                                                                                           N67
D01AD      Sutphin, Audrina                        AUSA09302008 8FF  252 31 UN0807122017                              22.19S 0 0 1 1  0  1 16.   1  0200    NN69
D01AD      Sutphin, Audrina                        AUSA09302008 8FF  501 41 UN0807122017   41.83S                     41.42S 0 0 6 3  0  2 13.   2  0100    NN40
D01AD      Sutphin, Ava                            AUSA0420200512FF  504 25 111207112017   35.26S   35.53S            34.82S 2 3 2 2  3  3 12.   3  0400    NN39
D3                                                                                                                                                           N67
D01AD      Sutphin, Ava                            AUSA0420200512FF  501 45 111207112017            31.06S            30.47S 2 5 2 1  5  3 12.   3  0100    NN58
D01AD      Sutphin, Ava                            AUSA0420200512FF 1005 65 111207122017 1:20.08S                   1:18.55S 0 0 3 4  0  2 13.   2  0500    NN09
G01AD          Sutphin, Ava                            1 2  50C   36.77 1:18.55                                                                F             N93
D01AD      Sutphin, Bella                          AUSA0717200610FF  254 23 091007122017   17.74S                     17.70S 0 0 4 3  0  2 13.   2  0400    NN39
D3                                                                                                                                                           N67
D01AD      Sutphin, Bella                          AUSA0717200610FF  501 43 091007122017   35.34S                     34.42S 0 0 6 3  0  1 16.   1  0100    NN29
D01AD      Sutphin, Bella                          AUSA0717200610FF 1005 63 091007122017 1:30.37S                   1:28.76S 0 0 3 3  0  1 16.   1  0500    NN89
G01AD          Sutphin, Bella                          1 2  50C   41.57 1:28.76                                                                F             N64
D01AD      Sutphin, Isla                           AUSA07042011 5FF  251 11 UN0807122017   37.74S                        NSS 0 0 3 5  0  0       0  01      NN88
D3                                                                                                                                                           N67
D01AD      Thiemann, Willow                        AUSA02182009 8FF  254 21 UN0807122017                              26.97S 0 0 1 2  0  6  9.   1  0400    NN79
D3                                                                                                                                                           N67
D01AD      Thiemann, Willow                        AUSA02182009 8FF  501 41 UN0807122017                              56.41S 0 0 2 3  0 10  3.   1  0100    NN69
D01AD      Thiemann, Willow                        AUSA02182009 8FF  253 51 UN0807122017   32.12S                     32.17S 0 0 5 4  0 11  2.   4  0300    NN40
D01AD      Turner, Anna                            AUSA0323200611FF  251 15 111207112017   23.02S   19.38S                   1 3 0 0 32             01      NN97
D3                                                                                                                                                           N67
D01AD      Turner, Anna                            AUSA0323200611FF  501 45 111207112017            46.36S                   1 4 0 0 36             01      NN27
D01AD      Turner, Anna                            AUSA0323200611FF  503 55 111207122017 1:14.38S                   1:07.45S 0 0 1 3  0 29       3  03      NN68
D01AD      Turner, Jake                            AUSA02122011 6MM  251 12 UN0807122017                              56.41S 0 0 1 6  0 51       4  01      NN77
D3                                                                                                                                                           N67
D01AD      Turner, Jake                            AUSA02122011 6MM  252 32 UN0807122017                            1:01.99S 0 0 1 3  0 40       3  02      NN97
D01AD      Turner, Luke                            AUSA07042008 8MM  254 22 UN0807122017                                 DQS 0 0 1 2  0  0       0  04      NN77
D3                                                                                                                                                           N67
D01AD      Turner, Luke                            AUSA07042008 8MM  252 32 UN0807122017   38.58S                     39.28S 0 0 5 4  0 21       1  02      NN78
G01AD          Turner, Luke                            1 3  25C                   39.28                                                        F             N23
D01AD      Turner, Luke                            AUSA07042008 8MM  501 42 UN0807122017                            1:05.71S 0 0 2 5  0 19       2  01      NN18
D01AD      Ward, Bridget                           AUSA0101200017FF  504 29 151807112017            37.34S            38.10S 1 1 1 5 10 12  1.   6  0400    NN98
D3                                                                                                                                                           N67
D01AD      Ward, Bridget                           AUSA0101200017FF 1001 49 151807122017                            1:13.55S 0 0 1 4  0  9  4.   2  0100    NN28
G01AD          Ward, Bridget                           1 2  50C   34.95 1:13.55                                                                F             N14
D01AD      Ward, Bridget                           AUSA0101200017FF  503 59 151807122017                              45.71S 0 0 1 2  0 12  1.   1  0300    NN08
D01AD      Ward, Gretchen                          AUSA02242009 8FF  251 11 UN0807122017   30.87S                     28.84S 0 0 7 1  0 25       2  01      NN29
D3                                                                                                                                                           N67
G01AD          Ward, Gretchen                          1 4  25C   13.11   24.18   36.20   28.84                                                F             N25
D01AD      Ward, Gretchen                          AUSA02242009 8FF  252 31 UN0807122017                                 DQS 0 0 1 4  0  0       0  02      NN28
D01AD      Ward, Trey                              AUSA0821200610MM  251 14 091007122017   33.14S                     29.47S 0 0 1 2  0 32       4  01      NN57
D3                                                                                                                                                           N67
D01AD      Ward, Trey                              AUSA0821200610MM  252 34 091007122017   32.48S                     31.57S 0 0 2 3  0 25       2  02      NN57
G01AD          Ward, Trey                              1 4  25C   12.47                   31.57                                                F             N92
D01AD      Ward, Trey                              AUSA0821200610MM  501 44 091007122017                            1:13.77S 0 0 1 6  0 27       6  01      NN07
D01AD      Wells, Cannon                           AUSA0602200511MM  251 16 111207112017   18.23S   17.42S            17.67S 3 5 1 4  8  8  5.   2  0100    NN79
D3                                                                                                                                                           N67
D01AD      Wells, Cannon                           AUSA0602200511MM  504 26 111207112017   57.43S   50.41S            49.07S 2 1 1 4  8  8  5.   2  0400    NN79
D01AD      Wells, Cannon                           AUSA0602200511MM  503 56 111207122017   57.08S                     56.87S 0 0 2 3  0 14       3  03      NN78
D01AD      Wells, Caroline                         AUSA0923199917FF  501 19 151807112017   32.43S   32.11S            32.29S 4 5 1 1 11 11  2.   5  0100    NN60
D3                                                                                                                                                           N67
D01AD      Wells, Caroline                         AUSA0923199917FF  504 29 151807112017            37.19S            35.91S 2 1 1 2  9  9  4.   3  0400    NN99
D01AD      Wells, Caroline                         AUSA0923199917FF  502 39 151807112017   39.39S   39.04S            38.04S 2 4 1 4  8  8  5.   2  0200    NN60
D01AD      Wells, Whitney                          AUSA1021200115FF  501 19 151807112017   35.09S   34.06S                   5 6 0 0 14             01      NN98
D3                                                                                                                                                           N67
D01AD      Wells, Whitney                          AUSA1021200115FF  502 39 151807112017   42.10S   39.48S            39.80S 4 1 1 2  9 10  3.   4  0200    NN30
D01AD      Wells, Whitney                          AUSA1021200115FF  503 59 151807122017   45.57S                     43.64S 0 0 3 4  0  7  7.   1  0300    NN59
D01AD      Wilson, Olivia                          AUSA0617200511FF  251 15 111207112017   14.95S   15.33S            14.75S 5 4 1 3  7  7  7.   1  0100    NN10
D3                                                                                                                                                           N67
D01AD      Wilson, Olivia                          AUSA0617200511FF  501 45 111207112017   32.43S   32.93S            32.63S 7 4 1 3  8  8  5.   2  0100    NN10
D01AD      Wilson, Olivia                          AUSA0617200511FF 1005 65 111207122017 1:27.72S                   1:26.04S 0 0 2 3  0  8  5.   2  0500    NN89
G01AD          Wilson, Olivia                          1 2  50C   40.06 1:26.04                                                                F             N64
D01AD      Winner, Hannah                          AUSA0930199917FF  501 19 151807112017            31.10S            30.45S 1 2 1 4  8  7  7.   1  0100    NN39
D3                                                                                                                                                           N67
D01AD      Winner, Hannah                          AUSA0930199917FF  504 29 151807112017   34.72S   34.51S            34.90S 1 4 2 6  6  6  9.   6  0400    NN20
D01AD      Winner, Hannah                          AUSA0930199917FF 1005 69 151807122017 1:22.81S                   1:19.12S 0 0 3 6  0  7  7.   6  0500    NN99
G01AD          Winner, Hannah                          1 2  50C   37.02 1:19.12                                                                F             N54
D01AD      Young, Carter                           AUSA0708200511MM  504 26 111207112017            56.04S               DQS 1 6 1 2  9  0       0  04      NN68
D3                                                                                                                                                           N67
D01AD      Young, Carter                           AUSA0708200511MM  501 46 111207112017   46.63S   43.25S                   1 3 0 0 15             01      NN58
D01AD      Young, Carter                           AUSA0708200511MM  503 56 111207122017   51.01S                     50.21S 0 0 4 6  0  6  9.   5  0300    NN09
E01AD      AADTHSC  F 1007  1 UN08  007122017 1:33.93S                   1:39.71S     1 3     1 32.                                            1N   0500X    N35
F01AD          ADTHSCAKiggins, Bella                          USA061820088 F  1                                                                              N85
G01AD          Kiggins, Bella                          1 4  25C                                                                                F             N23
F01AD          ADTHSCADornhaggen, Molly                       USA102220088 F  2                                                                              N17
G01AD          Dornhaggen, Molly                       1 4  25C   54.00                                                                        F             N05
F01AD          ADTHSCASutphin, Audrina                        USA093020088 F  3                                                                              N86
G01AD          Sutphin, Audrina                        1 4  25C                                                                                F             N24
F01AD          ADTHSCAThiemann, Willow                        USA021820098 F  4                                                                              N96
G01AD          Thiemann, Willow                        1 4  25C 1:39.71                                                                        F             N05
E01AD      AADTHSC  M 1007  2 UN08  007122017 1:48.16S                        DQS     1 4     0                                                0N   05  X    N74
F01AD          ADTHSCAFrye, Ben                               USA091120088 M  1                                                                              N04
G01AD          Frye, Ben                               1 4  25C                                                                                F             N31
F01AD          ADTHSCASchmittauer, Will                       USA040320098 M  2                                                                              N37
G01AD          Schmittauer, Will                       1 4  25C 1:00.06                                                                        F             N35
F01AD          ADTHSCACosgrove, Jack                          USA081120088 M  3                                                                              N95
G01AD          Cosgrove, Jack                          1 4  25C                                                                                F             N33
F01AD          ADTHSCADotzauer, Joey                          USA122920088 M  4                                                                              N26
G01AD          Dotzauer, Joey                          1 4  25C 1:42.48                                                                        F             N24
E01AD      AADTHSC  F 1007  3 0910  007122017 1:31.32S                   1:26.62S     1 6     4 22.                                            4N   0500X    N05
F01AD          ADTHSCALyons, Olivia                           USA0108200710F  1                                                                              N75
G01AD          Lyons, Olivia                           1 4  25C                                                                                F             N13
F01AD          ADTHSCAPhillips, Kira                          USA0429200710F  2                                                                              N06
G01AD          Phillips, Kira                          1 4  25C                                                                                F             N43
F01AD          ADTHSCASutphin, Bella                          USA0717200610F  3                                                                              N06
G01AD          Sutphin, Bella                          1 4  25C                                                                                F             N43
F01AD          ADTHSCADryer, Elli                             USA010120089 F  4                                                                              N84
G01AD          Dryer, Elli                             1 4  25C 1:26.62                                                                        F             N92
E01AD      AADTHSC  M 1007  4 0910  007122017 1:21.60S                   1:18.80S     1 3     1 32.                                            1N   0500X    N05
F01AD          ADTHSCAMeyers, Hayden                          USA0222200710M  1                                                                              N06
G01AD          Meyers, Hayden                          1 4  25C                                                                                F             N43
F01AD          ADTHSCABien, Liam                              USA0310200710M  2                                                                              N34
G01AD          Bien, Liam                              1 4  25C   44.23                                                                        F             N12
F01AD          ADTHSCAGrisi, Chase                            USA080520079 M  3                                                                              N15
G01AD          Grisi, Chase                            1 4  25C                                                                                F             N52
F01AD          ADTHSCADotzauer, Jackson                       USA0711200610M  4                                                                              N37
G01AD          Dotzauer, Jackson                       1 4  25C 1:18.80                                                                        F             N35
E01AD      AADTHSC  F 2007  5 1112  007122017 2:32.02S                   2:31.98S     1 4     4 22.                                            4N   0500X    N05
F01AD          ADTHSCACosgrove, Meg                           USA0603200511F  1                                                                              N65
G01AD          Cosgrove, Meg                           1 4  50C   41.42                                                                        F             N43
F01AD          ADTHSCALyons, Grace                            USA1113200412F  2                                                                              N25
G01AD          Lyons, Grace                            1 4  50C 1:25.63                                                                        F             N33
F01AD          ADTHSCASutphin, Ava                            USA0420200512F  3                                                                              N35
G01AD          Sutphin, Ava                            1 4  50C 2:00.08                                                                        F             N33
F01AD          ADTHSCAWilson, Olivia                          USA0617200511F  4                                                                              N16
G01AD          Wilson, Olivia                          1 4  50C 2:31.98                                                                        F             N24
E01AD      AADTHSC  M 2007  6 1112  007122017 2:51.07S                   2:42.60S     1 3     1 32.                                            1N   0500X    N05
F01AD          ADTHSCAMeyers, Will                            USA1222200412M  1                                                                              N35
G01AD          Meyers, Will                            1 4  50C   35.97                                                                        F             N23
F01AD          ADTHSCADehlinger, Clay                         USA0615200412M  2                                                                              N36
G01AD          Dehlinger, Clay                         1 4  50C 1:19.19                                                                        F             N34
F01AD          ADTHSCAJones, Lucas                            USA0710200412M  3                                                                              N25
G01AD          Jones, Lucas                            1 4  50C 1:30.59                                                                        F             N33
F01AD          ADTHSCABlazer, Brody                           USA0214200611M  4                                                                              N65
G01AD          Blazer, Brody                           1 4  50C 2:42.60                                                                        F             N73
E01AD      AADTHSC  F 2007  7 1314  007122017 2:39.36S                   2:34.10S     1 1     4 22.                                            4N   0500X    N05
F01AD          ADTHSCADetwiler, Sara                          USA0227200413F  1                                                                              N06
G01AD          Detwiler, Sara                          1 4  50C   45.36                                                                        F             N83
F01AD          ADTHSCABien, Lillian                           USA1024200313F  2                                                                              N45
G01AD          Bien, Lillian                           1 4  50C 1:28.94                                                                        F             N63
F01AD          ADTHSCARupp, Caroline                          USA0414200314F  3                                                                              N06
G01AD          Rupp, Caroline                          1 4  50C 2:01.98                                                                        F             N14
F01AD          ADTHSCAAman, Hannah                            USA0315200314F  4                                                                              N05
G01AD          Aman, Hannah                            1 4  50C 2:34.10                                                                        F             N03
E01AD      AADTHSC  M 2007  8 1314  007122017 2:37.70S                   2:35.41S     1 5     4 22.                                            4N   0500X    N15
F01AD          ADTHSCACovington, Ty                           USA1102200511M  1                                                                              N85
G01AD          Covington, Ty                           1 4  50C   45.29                                                                        F             N73
F01AD          ADTHSCACovington, Drew                         USA1211200313M  2                                                                              N56
G01AD          Covington, Drew                         1 4  50C   56.13                                                                        F             N34
F01AD          ADTHSCACombs, Aidan                            USA0909200313M  3                                                                              N15
G01AD          Combs, Aidan                            1 4  50C 1:25.28                                                                        F             N13
F01AD          ADTHSCAShaw, Brennan                           USA1107200313M  4                                                                              N65
G01AD          Shaw, Brennan                           1 4  50C 2:35.41                                                                        F             N63
E01AD      AADTHSC  F 2007  9 1518  007122017 2:15.32S                   2:10.52S     1 3     1 32.                                            1N   0500X    N05
F01AD          ADTHSCAAbernethy, Kylie                        USA0915200016F  1                                                                              N86
G01AD          Abernethy, Kylie                        1 4  50C   32.94                                                                        F             N64
F01AD          ADTHSCARupp, Hayley                            USA1113200016F  2                                                                              N35
G01AD          Rupp, Hayley                            1 4  50C 1:10.11                                                                        F             N43
F01AD          ADTHSCARupp, Jessica                           USA0714199917F  3                                                                              N85
G01AD          Rupp, Jessica                           1 4  50C 1:40.03                                                                        F             N73
F01AD          ADTHSCAWinner, Hannah                          USA0930199917F  4                                                                              N16
G01AD          Winner, Hannah                          1 4  50C 2:10.52                                                                        F             N04
E01AD      AADTHSC  M 2007 10 1518  007122017 2:12.39S                        NSS     1 2     0                                                0N   05  X    N54
F01AD          ADTHSCADehlinger, Evan                         USA0910199917M  1                                                                              N46
G01AD          Dehlinger, Evan                         1 4  50C                                                                                F             N63
F01AD          ADTHSCASmith, Sam                              USA0919200016M  2                                                                              N54
G01AD          Smith, Sam                              1 4  50C                                                                                F             N81
F01AD          ADTHSCADehlinger, Cole                         USA0720199818M  3                                                                              N46
G01AD          Dehlinger, Cole                         1 4  50C                                                                                F             N63
F01AD          ADTHSCASmith, Andrew                           USA0518200215M  4                                                                              N75
G01AD          Smith, Andrew                           1 4  50C                                                                                F             N03
E01AD      AADTHSC  F 1006 61 UN08  007122017 1:39.79S                   1:30.31S     1 4     1 32.                                            1N   0100X    N45
F01AD          ADTHSCADornhaggen, Molly                       USA102220088 F  1                                                                              N17
G01AD          Dornhaggen, Molly                       1 4  25C                                                                                F             N54
F01AD          ADTHSCAKiggins, Bella                          USA061820088 F  2                                                                              N85
G01AD          Kiggins, Bella                          1 4  25C   46.74                                                                        F             N73
F01AD          ADTHSCAThiemann, Willow                        USA021820098 F  3                                                                              N96
G01AD          Thiemann, Willow                        1 4  25C                                                                                F             N24
F01AD          ADTHSCASutphin, Audrina                        USA093020088 F  4                                                                              N86
G01AD          Sutphin, Audrina                        1 4  25C 1:30.31                                                                        F             N94
E01AD      AADTHSC  M 1006 62 UN08  007122017 1:29.83S                   1:24.35S     1 3     1 32.                                            1N   0100X    N55
F01AD          ADTHSCADotzauer, Joey                          USA122920088 M  1                                                                              N16
G01AD          Dotzauer, Joey                          1 4  25C                                                                                F             N53
F01AD          ADTHSCASchmittauer, Will                       USA040320098 M  2                                                                              N37
G01AD          Schmittauer, Will                       1 4  25C   43.40                                                                        F             N15
F01AD          ADTHSCAGleason, Owen                           USA072720088 M  3                                                                              N65
G01AD          Gleason, Owen                           1 4  25C                                                                                F             N03
F01AD          ADTHSCACosgrove, Jack                          USA081120088 M  4                                                                              N06
G01AD          Cosgrove, Jack                          1 4  25C 1:24.35                                                                        F             N04
E01AD      AADTHSC  F 1006 71 0910  007122017 1:19.71S                   1:12.64S     1 6     2 26.                                            2N   0100X    N15
F01AD          ADTHSCALyons, Olivia                           USA0108200710F  1                                                                              N75
G01AD          Lyons, Olivia                           1 4  25C                                                                                F             N13
F01AD          ADTHSCADryer, Elli                             USA010120089 F  2                                                                              N74
G01AD          Dryer, Elli                             1 4  25C   38.21                                                                        F             N72
F01AD          ADTHSCAPhillips, Kira                          USA0429200710F  3                                                                              N06
G01AD          Phillips, Kira                          1 4  25C                                                                                F             N43
F01AD          ADTHSCASutphin, Bella                          USA0717200610F  4                                                                              N06
G01AD          Sutphin, Bella                          1 4  25C 1:12.64                                                                        F             N14
E01AD      AADTHSC  M 1006 72 0910  007122017 1:12.97S                   1:09.18S     1 3     1 32.                                            1N   0100X    N15
F01AD          ADTHSCADotzauer, Jackson                       USA0711200610M  1                                                                              N37
G01AD          Dotzauer, Jackson                       1 4  25C                                                                                F             N64
F01AD          ADTHSCAGibson, Stefan                          USA0421200710M  2                                                                              N06
G01AD          Gibson, Stefan                          1 4  25C   35.30                                                                        F             N83
F01AD          ADTHSCAGrisi, Chase                            USA080520079 M  3                                                                              N15
G01AD          Grisi, Chase                            1 4  25C                                                                                F             N52
F01AD          ADTHSCAMeyers, Hayden                          USA0222200710M  4                                                                              N06
G01AD          Meyers, Hayden                          1 4  25C 1:09.18                                                                        F             N14
E01AD      AADTHSC  F 2006 73 1112  007122017 2:13.90S                   2:07.94S     1 4     2 26.                                            2N   0100X    N15
F01AD          ADTHSCASutphin, Ava                            USA0420200512F  1                                                                              N35
G01AD          Sutphin, Ava                            1 4  50C   30.74                                                                        F             N13
F01AD          ADTHSCACosgrove, Meg                           USA0603200511F  2                                                                              N65
G01AD          Cosgrove, Meg                           1 4  50C 1:02.51                                                                        F             N63
F01AD          ADTHSCALyons, Grace                            USA1113200412F  3                                                                              N25
G01AD          Lyons, Grace                            1 4  50C 1:36.01                                                                        F             N33
F01AD          ADTHSCAWilson, Olivia                          USA0617200511F  4                                                                              N16
G01AD          Wilson, Olivia                          1 4  50C 2:07.94                                                                        F             N24
E01AD      AADTHSC  M 2006 74 1112  007122017 2:29.79S                   2:25.93S     1 3     2 26.                                            2N   0100X    N25
F01AD          ADTHSCADehlinger, Clay                         USA0615200412M  1                                                                              N36
G01AD          Dehlinger, Clay                         1 4  50C   33.83                                                                        F             N14
F01AD          ADTHSCABlazer, Brody                           USA0214200611M  2                                                                              N65
G01AD          Blazer, Brody                           1 4  50C 1:16.09                                                                        F             N73
F01AD          ADTHSCAJones, Lucas                            USA0710200412M  3                                                                              N25
G01AD          Jones, Lucas                            1 4  50C 1:51.88                                                                        F             N33
F01AD          ADTHSCAMeyers, Will                            USA1222200412M  4                                                                              N45
G01AD          Meyers, Will                            1 4  50C 2:25.93                                                                        F             N43
E01AD      AADTHSC  F 2006 75 1314  007122017 2:20.50S                   2:15.85S     1 1     4 22.                                            4N   0100X    N15
F01AD          ADTHSCAAman, Hannah                            USA0315200314F  1                                                                              N05
G01AD          Aman, Hannah                            1 4  50C   32.72                                                                        F             N82
F01AD          ADTHSCADetwiler, Sara                          USA0227200413F  2                                                                              N06
G01AD          Detwiler, Sara                          1 4  50C 1:09.11                                                                        F             N04
F01AD          ADTHSCABien, Lillian                           USA1024200313F  3                                                                              N45
G01AD          Bien, Lillian                           1 4  50C                                                                                F             N82
F01AD          ADTHSCARupp, Caroline                          USA0414200314F  4                                                                              N16
G01AD          Rupp, Caroline                          1 4  50C 2:15.85                                                                        F             N14
E01AD      AADTHSC  M 2006 76 1314  007122017 2:24.21S                   2:21.05S     1 1     4 22.                                            4N   0100X    N15
F01AD          ADTHSCACovington, Drew                         USA1211200313M  1                                                                              N56
G01AD          Covington, Drew                         1 4  50C   33.59                                                                        F             N44
F01AD          ADTHSCAYoung, Carter                           USA0708200511M  2                                                                              N85
G01AD          Young, Carter                           1 4  50C 1:17.59                                                                        F             N83
F01AD          ADTHSCAShaw, Brennan                           USA1107200313M  3                                                                              N65
G01AD          Shaw, Brennan                           1 4  50C 1:49.73                                                                        F             N73
F01AD          ADTHSCACombs, Aidan                            USA0909200313M  4                                                                              N15
G01AD          Combs, Aidan                            1 4  50C 2:21.05                                                                        F             N03
E01AD      AADTHSC  F 2006 77 1518  007122017 2:13.87S                   1:59.51S     1 5     1 32.                                            1N   0100X    N25
F01AD          ADTHSCAAbernethy, Kylie                        USA0915200016F  1                                                                              N86
G01AD          Abernethy, Kylie                        1 4  50C   30.53                                                                        F             N64
F01AD          ADTHSCARupp, Hayley                            USA1113200016F  2                                                                              N35
G01AD          Rupp, Hayley                            1 4  50C 1:00.18                                                                        F             N43
F01AD          ADTHSCAWinner, Hannah                          USA0930199917F  3                                                                              N16
G01AD          Winner, Hannah                          1 4  50C 1:30.17                                                                        F             N04
F01AD          ADTHSCARupp, Jessica                           USA0714199917F  4                                                                              N85
G01AD          Rupp, Jessica                           1 4  50C 1:59.51                                                                        F             N73
E01AD      AADTHSC  M 2006 78 1518  007122017 1:59.95S                   2:16.76S     1 1     5 20.                                            5N   0100X    N35
F01AD          ADTHSCADehlinger, Evan                         USA0910199917M  1                                                                              N46
G01AD          Dehlinger, Evan                         1 4  50C   26.52                                                                        F             N14
F01AD          ADTHSCASmith, Andrew                           USA0518200215M  2                                                                              N75
G01AD          Smith, Andrew                           1 4  50C 1:25.17                                                                        F             N73
F01AD          ADTHSCASmith, Sam                              USA0919200016M  3                                                                              N54
G01AD          Smith, Sam                              1 4  50C 1:36.03                                                                        F             N52
F01AD          ADTHSCADehlinger, Cole                         USA0720199818M  4                                                                              N46
G01AD          Dehlinger, Cole                         1 4  50C 2:16.76                                                                        F             N34
Z01Meet Res02Successful Build on 7/13/2017   1  1   6   6  1371   479  118   472   745                                                                       N99
